<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="s"%>
<html>
<head>
<meta charset="UTF-8">
<title>学生列表</title>
<link rel="stylesheet" type="text/css"
	href="easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="easyui/themes/icon.css">
<link rel="stylesheet" type="text/css" href="easyui/css/demo.css">
<script type="text/javascript" src="easyui/jquery.min.js"></script>
<script type="text/javascript" src="easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="easyui/js/validateExtends.js"></script>
<script type="text/javascript">
	$(function() {
		//datagrid初始化 
		$('#dataList').datagrid(
				{
					title : '学生列表',
					iconCls : 'icon-more',//图标 
					border : true,
					collapsible : false,//是否可折叠的 
					fit : true,//自动大小 
					method : "post",
					url : "StudentServlet?method=StudentList&t="
							+ new Date().getTime(),
					idField : 'id',
					singleSelect : false,//是否单选 
					pagination : true,//分页控件 
					rowu_snos : true,//行号 
					sortName : 'id',
					sortOrder : 'DESC',
					remoteSort : true,
					columns : [ [  {
						field : 'chk',
						//checkbox : true,
						width : 50
					},  {
						field : 'id',
						title : 'ID',
						width : 50,
						sortable : true
					}, {
						field : 'u_sno',
						title : '学号',
						width : 200,
						sortable : true
					}, {
						field : 'u_name',
						title : '姓名',
						width : 200
					}, {
						field : 'u_sex',
						title : '性别',
						width : 100
					}, {
						field : 'u_phone',
						title : '电话',
						width : 150
					}, {
						field : 'u_qq',
						title : 'u_qq',
						width : 150
					}, {
						field : 'u_class',
						title : '班级',
						width : 150,
						formatter : function(value, row, index) {
							if (row.clazz) {
								return row.clazz.name;
							} else {
								return value;
							}
						}
					}, {
						field : 'u_grade',
						title : '年级',
						width : 150,
						formatter : function(value, row, index) {
							if (row.grade) {
								return row.grade.name;
							} else {
								return value;
							}
						}
					}, ] ],
					toolbar : "#toolbar"

				});
		//设置分页控件 
		var p = $('#dataList').datagrid('getPager');
		$(p).pagination({
			pageSize : 10,//每页显示的记录条数，默认为10 
			pageList : [ 10, 20, 30, 50, 100 ],//可以设置每页记录条数的列表 
			beforePageText : '第',//页数文本框前显示的汉字 
			afterPageText : '页    共 {pages} 页',
			displayMsg : '当前显示 {from} - {to} 条记录   共 {total} 条记录',
		});

		//设置工具类按钮
		$("#add").click(function() {
			$("#addDialog").dialog("open");
		});
		//修改
		$("#edit").click(function() {
			var selectRows = $("#dataList").datagrid("getSelections");
			if (selectRows.length != 1) {
				$.messager.alert("消息提醒", "请选择一条数据进行操作!", "warning");
			} else {
				$("#editDialog").dialog("open");
			}
		});
		 //删除
	    $("#delete").click(function(){
	    	var selectRows = $("#dataList").datagrid("getSelections");
        	var selectLength = selectRows.length;
        	if(selectLength == 0){
            	$.messager.alert("消息提醒", "请选择数据进行删除!", "warning");
            } else{
			            	
            	var obj=document.getElementsByName('checkteahcer'); 
            	var ids = [];
            	
            	for(var i=0;i<obj.length;i++){
            		//alert(obj[i].checked)
            		if(obj[i].checked){
            			ids+=obj[i].value+",";
            		}
            	}
            	$.messager.confirm("消息提醒", "将删除与学生相关的所有数据，确认继续？", function(r){
            		if(r){
            			$.ajax({
							type: "post",
							url: "DeleteUser",
							data: {ids:ids},
							success: function(msg){
								if(msg == "success"){
									$.messager.alert("消息提醒","删除成功!","info");

									//刷新表格
									window.location.href = "StudentList";
								} else{
									$.messager.alert("消息提醒","删除失败!","warning");
									return;
								}
							}
						});
            		}
            	});
            }
	    });

		/* $("#delete")
				.click(
						function() {
							var selectRows = $("#dataList").datagrid(
									"getSelections");
							var selectLength = selectRows.length;
							if (selectLength == 0) {
								$.messager.alert("消息提醒", "请选择数据进行删除!",
										"warning");
							} else {
								var numbers = [];
								$(selectRows).each(function(i, row) {
									numbers[i] = row.u_sno;
								}); 
								var ids = [];
								$(selectRows).each(function(i, row) {
									ids[i] = row.id;
								});
								$.messager
										.confirm(
												"消息提醒",
												"将删除与学生相关的所有数据(包括成绩)，确认继续？",
												function(r) {
													if (r) {
														$.ajax({
																	type : "post",
																	url : "DeleteUser",
																	data : {
																		numbers : numbers,
																		ids : ids
																	},
																	success : function(
																			msg) {
																		if (msg == "success") {
																			$.messager
																					.alert(
																							"消息提醒",
																							"删除成功!",
																							"info");
																			//刷新表格
																			$(
																					"#dataList")
																					.datagrid(
																							"reload");
																			$(
																					"#dataList")
																					.datagrid(
																							"uncheckAll");
																		} else {
																			$.messager
																					.alert(
																							"消息提醒",
																							"删除失败!",
																							"warning");
																			return;
																		}
																	}
																});
													}
												});
							}
						}); */

		//年级下拉框
		/*$("#gradeList").combobox({
			width : "150",
			height : "25",
			valueField : "id",
			textField : "name",
			multiple : false, //可多选
			editable : false, //不可编辑
			method : "post",
			url : "GradeServlet?method=GradeList&t=" + new Date().getTime(),
			onChange : function(newValue, oldValue) {
				//加载该年级下的学生
				$('#dataList').datagrid("options").queryParams = {
					u_grade : newValue
				};
				$('#dataList').datagrid("reload");

				 //加载该年级下的班级
				$("#clazzList").combobox("clear");
				$("#clazzList").combobox("options").queryParams = {
					u_grade : newValue
				};
				$("#clazzList").combobox("reload") 
			}
		});*/
		//班级下拉框
		$("#clazzList").combobox({
			width : "150",
			height : "25",
			valueField : "id",
			textField : "name",
			multiple : false, //可多选
			editable : false, //不可编辑
			method : "post",
			url : "ClazzServlet?method=ClazzList&t=" + new Date().getTime(),
			onChange : function(newValue, oldValue) {
				//加载班级下的学生
				$('#dataList').datagrid("options").queryParams = {
					u_class : newValue
				};
				$('#dataList').datagrid("reload");
			}
		});

		//下拉框通用属性
		$("#add_gradeList, #edit_gradeList, #add_clazzList, #edit_clazzList")
				.combobox({
					width : "200",
					height : "30",
					valueField : "id",
					textField : "name",
					multiple : false, //可多选
					editable : false, //不可编辑
					method : "post",
				});

		/*$("#add_gradeList").combobox({
			url : "GradeServlet?method=GradeList&t=" + new Date().getTime(),
			 onChange : function(newValue, oldValue) {
				//加载该年级下的班级
				$("#add_clazzList").combobox("clear");
				$("#add_clazzList").combobox("options").queryParams = {
					u_grade : newValue
				};
				$("#add_clazzList").combobox("reload");
			}, 
			onLoadSuccess : function() {
				//默认选择第一条数据
				var data = $(this).combobox("getData");
				$(this).combobox("setValue", data[0].id);
			}
		});*/
		/* $("#add_clazzList").combobox({
			url : "ClazzServlet?method=ClazzList&t=" + new Date().getTime(),
			 onLoadSuccess : function() {
				//默认选择第一条数据
				var data = $(this).combobox("getData");
				;
				$(this).combobox("setValue", data[0].id);
			} 
		}); */

		/*$("#edit_gradeList").combobox({
			url : "GradeServlet?method=GradeList&t=" + new Date().getTime(),
			 onChange : function(newValue, oldValue) {
				//加载该年级下的班级
				$("#edit_clazzList").combobox("clear");
				$("#edit_clazzList").combobox("options").queryParams = {
					u_grade : newValue
				};
				$("#edit_clazzList").combobox("reload");
			}, 
			onLoadSuccess : function() {
				//默认选择第一条数据
				var data = $(this).combobox("getData");
				$(this).combobox("setValue", data[0].id);
			}
		});*/

		/* $("#edit_clazzList").combobox({
			url : "ClazzServlet?method=ClazzList&t=" + new Date().getTime(),
			onLoadSuccess : function() {
				//默认选择第一条数据
				var data = $(this).combobox("getData");
				$(this).combobox("setValue", data[0].id);
			}
		}); 
		 */
		//设置添加学生窗口
		$("#addDialog")
				.dialog(
						{
							title : "添加学生",
							width : 650,
							height : 460,
							iconCls : "icon-add",
							modal : true,
							collapsible : false,
							minimizable : false,
							maximizable : false,
							draggable : true,
							closed : true,
							buttons : [
									{
										text : '添加',
										plain : true,
										iconCls : 'icon-user_add',
										handler : function() {
											var validate = $("#addForm").form(
													"validate");
											if (!validate) {
												$.messager
														.alert("消息提醒",
																"请检查你输入的数据!",
																"warning");
												return;
											} else {
												var u_grade = $(
														"#add_gradeList")
														.combobox("getValue");
												var u_class = $(
														"#add_clazzList")
														.combobox("getValue");
												$
														.ajax({
															type : "post",
															url : "Studentadd",
															data : $("#addForm")
																	.serialize(),
															success : function(
																	msg) {
																if (msg == "success") {
																	$.messager
																			.alert(
																					"消息提醒",
																					"添加成功!",
																					"info");
																	//关闭窗口
																	$(
																			"#addDialog")
																			.dialog(
																					"close");
																	//刷新表格
																	window.location.href = "StudentList";
																	/* //清空原表格数据
																	$(
																			"#add_u_sno")
																			.textbox(
																					'setValue',
																					"");
																	$(
																			"#add_name")
																			.textbox(
																					'setValue',
																					"");
																	$(
																			"#add_u_sex")
																			.textbox(
																					'setValue',
																					"男");
																	$(
																			"#add_u_phone")
																			.textbox(
																					'setValue',
																					"");
																	$(
																			"#add_u_qq")
																			.textbox(
																					'setValue',
																					"");
																	$("#photo")
																			.textbox(
																					'setValue',
																					""); */
																	
																	/* //重新刷新页面数据
																	$(
																			'#dataList')
																			.datagrid(
																					"options").queryParams = {
																		u_class : u_class
																	};
																	$(
																			'#dataList')
																			.datagrid(
																					"reload");
																	$(
																			"#gradeList")
																			.combobox(
																					'setValue',
																					"");
																	setTimeout(
																			function() {
																				$(
																						"#clazzList")
																						.combobox(
																								'setValue',
																								u_class);
																			},
																			100); */

																} else {
																	$.messager
																			.alert(
																					"消息提醒",
																					"添加失败!",
																					"warning");
																	return;
																}
															}
														});
											}
										}
									},
									{
										text : '重置',
										plain : true,
										iconCls : 'icon-reload',
										handler : function() {
											$("#add_u_sno").textbox('setValue',
													"");
											$("#add_name").textbox('setValue',
													"");
											$("#add_u_phone").textbox(
													'setValue', "");
											$("#add_u_qq").textbox('setValue',
													"");
											//重新加载年级
											$("#add_gradeList").combobox(
													"clear");
											$("#add_gradeList").combobox(
													"reload");
										}
									}, ]
						});

		//设置编辑学生窗口
		$("#editDialog")
				.dialog(
						{
							title : "修改学生信息",
							width : 650,
							height : 460,
							iconCls : "icon-edit",
							modal : true,
							collapsible : false,
							minimizable : false,
							maximizable : false,
							draggable : true,
							closed : true,
							buttons : [
									{
										text : '提交',
										plain : true,
										iconCls : 'icon-user_add',
										handler : function() {
											var validate = $("#editForm").form(
													"validate");
											/* var u_grade = $("#edit_gradeList")
													.combobox("getValue");
											var u_class = $("#edit_clazzList")
													.combobox("getValue"); */
											if (!validate) {
												$.messager
														.alert("消息提醒",
																"请检查你输入的数据!",
																"warning");
												return;
											} else {
												$
														.ajax({
															type : "post",
															url : "UpdateStudent",
															data : $(
																	"#editForm")
																	.serialize(),
															success : function(
																	msg) {
																if (msg == "success") {
																	$.messager
																			.alert(
																					"消息提醒",
																					"更新成功!",
																					"info");
																	//关闭窗口
																	$(
																			"#editDialog")
																			.dialog(
																					"close");
																	//刷新表格
																	window.location.href = "StudentList";
																	/* //刷新表格
																	$(
																			'#dataList')
																			.datagrid(
																					"options").queryParams = {
																		u_class : u_class
																	};
																	$(
																			"#dataList")
																			.datagrid(
																					"reload");
																	$(
																			"#dataList")
																			.datagrid(
																					"uncheckAll");

																	$(
																			"#gradeList")
																			.combobox(
																					'setValue',
																					u_grade);
																	setTimeout(
																			function() {
																				$(
																						"#clazzList")
																						.combobox(
																								'setValue',
																								u_class);
																			},
																			100); */

																} else {
																	$.messager
																			.alert(
																					"消息提醒",
																					"更新失败!",
																					"warning");
																	return;
																}
															}
														});
											}
										}
									},
									{
										text : '重置',
										plain : true,
										iconCls : 'icon-reload',
										handler : function() {
											//清空表单
											$("#edit_name").textbox('setValue',
													"");
											$("#edit_u_sex").textbox(
													'setValue', "男");
											$("#edit_u_phone").textbox(
													'setValue', "");
											$("#edit_u_qq").textbox('setValue',
													"");
											$("#edit_gradeList").combobox(
													"clear");
											$("#edit_gradeList").combobox(
													"reload");
										}
									} ],
							onBeforeOpen : function() {
								var selectRow = $("#dataList").datagrid(
										"getSelected");
								//设置值
								$("#edit_u_id").textbox('setValue',
										selectRow.id);
								$("#edit_u_sno").textbox('setValue',
										selectRow.u_sno);
								$("#edit_name").textbox('setValue',
										selectRow.u_name);
								$("#edit_u_sex").textbox('setValue',
										selectRow.u_sex);
								$("#edit_u_phone").textbox('setValue',
										selectRow.u_phone);
								$("#edit_u_qq").textbox('setValue',
										selectRow.u_qq);
								$("#edit_photo").attr(
										"src",
										"PhotoServlet?method=GetPhoto&type=2&u_sno="
												+ selectRow.u_sno);
								var u_grade = selectRow.u_grade;
								var u_class = selectRow.u_class;
								$("#edit_gradeList").combobox('setValue',
										u_grade);
								setTimeout(function() {
									$("#edit_clazzList").combobox('setValue',
											u_class);
								}, 100);

							}
						});

	});
</script>
</head>
<body>
	<!-- 学生列表 -->
	<table id="dataList" cellspacing="0" cellpadding="0">
		<s:forEach var="list" items="${requestScope.list }">
			<tr align="center">
				<td><input type="checkbox" value="${list.u_id }" name="checkteahcer"></td>
				<td>${list.u_id }</td>
				<td>${list.u_sno }</td>
				<td>${list.u_name }</td>
				<td>${list.u_sex }</td>
				<td>${list.u_phone }</td>
				<td>${list.u_qq }</td>
				<td>${list.g_name }</td>
				<td>${list.c_name }</td>
			</tr>
		</s:forEach>
	</table>
	<!-- 工具栏 -->
	<div id="toolbar">
		<div style="float: left;">
			<a id="add" href="javascript:;" class="easyui-linkbutton"
				data-options="iconCls:'icon-add',plain:true">添加</a>
		</div>
		<div style="float: left;" class="datagrid-btn-separator"></div>
		<div style="float: left;">
			<a id="edit" href="javascript:;" class="easyui-linkbutton"
				data-options="iconCls:'icon-edit',plain:true">修改</a>
		</div>
		<div style="float: left;" class="datagrid-btn-separator"></div>
		<div style="float: left;">
			<a id="delete" href="javascript:;" class="easyui-linkbutton"
				data-options="iconCls:'icon-some-delete',plain:true">删除</a>
		</div>
	</div>


	<!-- 添加学生窗口 -->
	<div id="addDialog" style="padding: 10px">
		<div
			style="float: right; margin: 20px 20px 0 0; width: 200px; border: 1px solid #EBF3FF"
			id="photo">
			<img alt="照片" style="max-width: 200px; max-height: 400px;" title="照片"
				src="photo/student.jpg" name="u_photo" />
		</div>
		<form id="addForm" method="post">
			<table cellpadding="8">
				<tr>
					<td>学号:</td>
					<td><input id="add_u_sno" class="easyui-textbox"
						style="width: 200px; height: 30px;" type="text" name="u_sno"
						data-options="required:true, validType:'repeat', missingMessage:'请输入学号'" />
					</td>
				</tr>
				<tr>
					<td>姓名:</td>
					<td><input id="add_name" style="width: 200px; height: 30px;"
						class="easyui-textbox" type="text" name="u_name"
						data-options="required:true, missingMessage:'请填写姓名'" /></td>
				</tr>
				<tr>
					<td>性别:</td>
					<td><select id="add_u_sex" class="easyui-combobox"
						data-options="editable: false, panelHeight: 50, width: 60, height: 30"
						name="u_sex"><option value="男">男</option>
							<option value="女">女</option></select></td>
				</tr>
				<tr>
					<td>电话:</td>
					<td><input id="add_u_phone"
						style="width: 200px; height: 30px;" class="easyui-textbox"
						type="text" name="u_phone" validType="mobile" /></td>
				</tr>
				<tr>
					<td>u_qq:</td>
					<td><input id="add_u_qq" style="width: 200px; height: 30px;"
						class="easyui-textbox" type="text" name="u_qq" validType="u_sno" /></td>
				</tr>
				<tr>
					<td>年级:</td>
					<td><select name="u_grade" id="add_gradeList"
						class="easyui-textbox"
						data-options="editable: false, panelHeight: 200, width: 200, height: 30">
							<option value="">--请选择--</option>
							<s:forEach items="${requestScope.list1 }" var="list1">
								<option value="${list1.g_id }">${list1.g_name }</option>
							</s:forEach>
					</select></td>
				</tr>
				<tr>
					<td>班级:</td>
					<td><select name="u_class" id="add_clazzList"
						class="easyui-textbox"
						data-options="editable: false, panelHeight: 100, width: 200, height: 30">
							<option value="">--请选择--</option>
							<s:forEach items="${requestScope.list2 }" var="list2">
								<option value="${list2.c_id }">${list2.c_name }</option>
							</s:forEach>
					</select></td>
				</tr>
			</table>
		</form>
	</div>

	<!-- 修改学生窗口 -->
	<div id="editDialog" style="padding: 10px">
		<div
			style="float: right; margin: 20px 20px 0 0; width: 200px; border: 1px solid #EBF3FF"
			id="photo">
			<img alt="照片" style="max-width: 200px; max-height: 400px;" title="照片"
				src="photo/student.jpg" name="u_photo" />
		</div>
		<form id="editForm" method="post">
			<table cellpadding="8">
				<tr>
					<td>ID</td>
					<td><input id="edit_u_id" class="easyui-textbox"
						style="width: 200px; height: 30px;" data-options="readonly: true"
						type="text" name="u_id" /></td>
				</tr>
				<tr>
					<td>学号:</td>
					<td><input id="edit_u_sno" data-options="readonly: true"
						class="easyui-textbox" style="width: 200px; height: 30px;"
						type="text" name="u_sno"
						data-options="required:true, validType:'repeat', missingMessage:'请输入学号'" />
					</td>
				</tr>
				<tr>
					<td>姓名:</td>
					<td><input id="edit_name" style="width: 200px; height: 30px;"
						class="easyui-textbox" type="text" name="u_name"
						data-options="required:true, missingMessage:'请填写姓名'" /></td>
				</tr>
				<tr>
					<td>性别:</td>
					<td><select id="edit_u_sex" class="easyui-combobox"
						data-options="editable: false, panelHeight: 50, width: 60, height: 30"
						name="u_sex"><option value="男">男</option>
							<option value="女">女</option></select></td>
				</tr>
				<tr>
					<td>电话:</td>
					<td><input id="edit_u_phone"
						style="width: 200px; height: 30px;" class="easyui-textbox"
						type="text" name="u_phone" validType="mobile" /></td>
				</tr>
				<tr>
					<td>u_qq:</td>
					<td><input id="edit_u_qq" style="width: 200px; height: 30px;"
						class="easyui-textbox" type="text" name="u_qq" validType="u_sno" /></td>
				</tr>
				<tr>
					<td>年级:</td>
					<td><select name="u_grade" id="add_gradeList"
						class="easyui-textbox"
						data-options="editable: false, panelHeight: 200, width: 200, height: 30">
							<option value="">--请选择--</option>
							<s:forEach items="${requestScope.list1 }" var="list1">
								<option value="${list1.g_id }">${list1.g_name }</option>
							</s:forEach>
					</select></td>
				</tr>
				<tr>
					<td>班级:</td>
					<td><select name="u_class" id="add_clazzList"
						class="easyui-textbox"
						data-options="editable: false, panelHeight: 100, width: 200, height: 30">
							<option value="">--请选择--</option>
							<s:forEach items="${requestScope.list2 }" var="list2">
								<option value="${list2.c_id }">${list2.c_name }</option>
							</s:forEach>
					</select></td>
				</tr>
			</table>
		</form>
	</div>

</body>
</html>