<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="UTF-8">
	<title>学生列表</title>
	<link rel="stylesheet" type="text/css" href="easyui/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="easyui/themes/icon.css">
	<link rel="stylesheet" type="text/css" href="easyui/css/demo.css">
	<script type="text/javascript" src="easyui/jquery.min.js"charset="UTF-8"></script>
	<script type="text/javascript" src="easyui/jquery.easyui.min.js"charset="UTF-8"></script>
	<script type="text/javascript" src="easyui/js/validateExtends.js"charset="UTF-8"></script>
	<script type="text/javascript">
	$(function() {	
		//datagrid初始化 
	    $('#dataList').datagrid({ 
	        title:'学生列表', 
	        iconCls:'icon-more',//图标 
	        border: true, 
	        collapsible:false,//是否可折叠的 
	        fit: true,//自动大小 
	        method: "post",//请求远程数据的方法（method）类型。
	        url:"findAllStudent",//从远程站点请求数据的 URL。
	        idField:'u_Id', //指示哪个字段是标识字段。
	        singleSelect: true,//是否单选 
	        pagination: true,//分页控件 
	        rownumbers: true,//行号 
	        sortName:'u_Id',//定义可以排序的列
	        sortOrder:'DESC', //定义列的排序顺序，只能用 'asc' 或 'desc'。
	        remoteSort: true,//定义是否从服务器排序数据。
	        columns: [[  
					{field:'chk',checkbox: true,width:50},
	 		        {field:'u_Id',title:'ID',width:50, sortable: true},    
	 		        {field:'u_Sno',title:'学号',width:200, sortable: true},    
	 		        {field:'u_Name',title:'姓名',width:200},
	 		        {field:'u_Sex',title:'性别',width:100},
	 		        {field:'u_Phone',title:'电话',width:150},
	 		        {field:'u_Qq',title:'QQ',width:150},
	 		        {field:'c_Name',title:'班级',width:150},
	 		        {field:'g_Name',title:'年级',width:150}
	 			]], 
	 		toolbar: [
	        	{
	        		text: '查看',
	        		iconCls: 'icon-zoom-in',
	        		handler: function(){
	        			var selectRow = $("#dataList").datagrid("getSelected");//返回第一个选中的行或者 null。
	                	if(selectRow == null){
	                    	$.messager.alert("消息提醒", "请选择一个学生", "warning");
	                    } else{
		        			$("#editDialog").dialog("open");//打开一个会话
	                    }
	        		}
	        	}          
	        ]
	    }); 
		
	    //设置分页控件 
	    var p = $('#dataList').datagrid('getPager'); 
	    $(p).pagination({ 
	        pageSize: 10,//每页显示的记录条数，默认为10 
	        pageList: [10,20,30,50,100],//可以设置每页记录条数的列表 
	        beforePageText: '第',//页数文本框前显示的汉字 
	        afterPageText: '页    共 {pages} 页', 
	        displayMsg: '当前显示 {from} - {to} 条记录   共 {total} 条记录', 
	    }); 
	    
	  	//设置显示学生窗口
	    $("#editDialog").dialog({
	    	title: "学生信息",//对话框的标题文本。
	    	width: 650,
	    	height: 460,
	    	iconCls: "icon-man",
	    	modal: true,//模态框
	    	draggable: true,//可以拖动的
	    	closed: true,//默认弹出框关闭
			onBeforeOpen: function(){//打开前触发
				var selectRow = $("#dataList").datagrid("getSelected");//返回第一个选中的行或者 null。
				//设置值
				$("#edit_number").textbox('setValue', selectRow.u_Sno);
				$("#edit_name").textbox('setValue', selectRow.u_Name);
				$("#edit_sex").textbox('setValue', selectRow.u_Sex);
				$("#edit_phone").textbox('setValue', selectRow.u_Phone);
				$("#edit_qq").textbox('setValue', selectRow.u_Qq);
				$("#edit_grade").textbox('setValue', selectRow.g_Name);
				$("#edit_clazz").textbox('setValue', selectRow.c_Name);
				//$("#edit_photo").attr("src", "\photo\student.jpg");
			}
	    });
	   
	});
	</script>
</head>
<body>
	<!-- 学生列表 -->
	<table id="dataList" cellspacing="0" cellpadding="0"> 
	    
	</table> 
	
	<!-- 显示学生信息窗口 -->
	<div id="editDialog" style="padding: 10px">
		<div style="float: right; margin: 20px 20px 0 0; width: 200px; border: 1px solid #EBF3FF">
	    	<!--  <img id="edit_photo" alt="照片" style="max-width: 200px; max-height: 400px;" title="照片" src=""/>-->
	    </div>   
    	<form id="editForm" method="post">
	    	<table cellpadding="8" >
	    		<tr>
	    			<td>学号:</td>
	    			<td>
	    				<input id="edit_number" data-options="readonly: true" class="easyui-textbox" style="width: 200px; height: 30px;" type="text" name="number"/>
	    			</td>
	    		</tr>
	    		<tr>
	    			<td>姓名:</td>
	    			<td><input id="edit_name" style="width: 200px; height: 30px;" class="easyui-textbox" type="text" name="name" data-options="readonly:true" /></td>
	    		</tr>
	    		<tr>
	    			<td>性别:</td>
	    			<td><select id="edit_sex" class="easyui-combobox" data-options="editable: false, readonly:true, panelHeight: 50, width: 60, height: 30" name="sex"><option value="男">男</option><option value="女">女</option></select></td>
	    		</tr>
	    		<tr>
	    			<td>电话:</td>
	    			<td><input id="edit_phone" style="width: 200px; height: 30px;" class="easyui-textbox" type="text" name="phone" data-options="readonly:true" /></td>
	    		</tr>
	    		<tr>
	    			<td>QQ:</td>
	    			<td><input id="edit_qq" style="width: 200px; height: 30px;" class="easyui-textbox" type="text" name="qq" data-options="readonly:true" /></td>
	    		</tr>
	    		<tr>
	    			<td>年级:</td>
	    			<td><input id="edit_grade" style="width: 200px; height: 30px;" class="easyui-textbox" data-options="readonly:true" name="gradeid" /></td>
	    		</tr>
	    		<tr>
	    			<td>班级:</td>
	    			<td><input id="edit_clazz" style="width: 200px; height: 30px;" class="easyui-textbox" data-options="readonly:true" name="clazzid" /></td>
	    		</tr>
	    	</table>
	    </form>
	</div>
	
</body>
</html>