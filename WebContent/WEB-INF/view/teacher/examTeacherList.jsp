<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
<title>考试列表</title>
<link rel="stylesheet" type="text/css"
	href="easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="easyui/themes/icon.css">
<link rel="stylesheet" type="text/css" href="easyui/css/demo.css">
<script type="text/javascript" src="easyui/jquery.min.js"></script>
<script type="text/javascript" src="easyui/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="easyui/themes/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript">
	//验证只能为数字
	function scoreBlur(score) {
		if (!/^[1-9]\d*$/.test($(score).val())) {
			$(score).val("");
		}
	}
	$(function() {

		var table;

		//datagrid初始化 
		$('#dataList').datagrid(
				{
					title : '考试列表',
					iconCls : 'icon-more',//图标 
					border : true,
					collapsible : false,//是否可折叠的 
					fit : true,//自动大小 
					method : "post",
					url : "ExamServlet?method=TeacherExamList&t="
							+ new Date().getTime(),
					idField : 'id',
					singleSelect : true,//是否单选 
					pagination : false,//分页控件 
					rownumbers : true,//行号 
					sortName : 'id',
					sortOrder : 'DESC',
					remoteSort : true,
					columns : [ [ {
						field : 'chk',
						checkbox : true,
						width : 50
					}, {
						field : 'id',
						title : 'ID',
						width : 50,
						sortable : true
					}, {
						field : 'name',
						title : '考试名称',
						width : 200,
						sortable : true
					}, {
						field : 'etime',
						title : '考试时间',
						width : 150
					}, {
						field : 'type',
						title : '考试类型',
						width : 100,
						formatter : function(value, row, index) {
							if (value == 1) {
								return "年级统考"
							} else {
								return "平时考试";
							}
						}
					}, {
						field : 'gname',
						title : '考试年级',
						width : 100,
						formatter : function(value, row, index) {
							if (row.grade) {
								return row.grade.name;
							} else {
								return value;
							}
						}
					}, {
						field : 'cname',
						title : '考试班级',
						width : 100,
						formatter : function(value, row, index) {
							if (row.clazz) {
								return row.clazz.name;
							} else {
								return value;
							}
						}
					},  {
						field : 'remark',
						title : '备注',
						width : 250
					} ] ],
					toolbar : "#toolbar"
				});

		//设置工具类按钮
		$("#add").click(function() {
			$("#addDialog").dialog("open");
		});
		$("#register").click(function() {
			$("#regEscoreToolbar").dialog("open");
		});
		//成绩统计
		$("#escore")
				.click(
						function() {

							var exam = $("#dataList").datagrid("getSelected");

							if (exam == null) {
								$.messager.alert("消息提醒", "请选择考试进行统计!",
										"warning");
							} else {

								var data = {
									id : exam.id,
									gradeid : exam.gradeid,
									clazzid : exam.clazzid,
									courseid : exam.courseid,
									type : exam.type
								};
								//动态显示该次考试的科目
								$.ajax({
									type : "post",
									url : "selectExamByClass",
									data : data,
									dataType : "json",
									async : false,
									success : function(result) {
										var columns = [];
										$.each(result, function(i, course) {
											//	var column = {};
											//	column["field"] = "course"
											//			+ course.id;
											//	column["title"] = course.name;
											//	column["width"] = 80;
											//	column["align"] = "center";
											//	column["resizable"] = false;
											//	column["sortable"] = true;

											//	columns.push(column);//当需要formatter的时候自己添加就可以了,原理就是拼接字符串.  
										});

										if (exam.type == 1) {
											columns.push({
												field : 'total',
												title : '总分',
												width : 70,
												sortable : true
											});

											$("#escoreClazzList").combobox(
													"readonly", false);

											$("#escoreClazzList").combobox(
													"options").queryParams = {
												gradeid : exam.gradeid
											};
											$("#escoreClazzList").combobox(
													"reload");
										} else {
											$("#escoreClazzList").combobox(
													"readonly", true);
										}

										$('#escoreList').datagrid({
											columns : [ columns ]
										});

									}
								});
								setTimeout(
										function() {
											$("#escoreList")
													.datagrid("options").url = "selectExamByClass";
											$("#escoreList")
													.datagrid("options").queryParams = data;
											$("#escoreList").datagrid("reload");
											$("#escoreListDialog").dialog(
													"open");
										}, 100)

							}
						});
		//设置添加窗口
		$("#addDialog")
				.dialog(
						{
							title : "添加考试",
							width : 850,
							height : 550,
							iconCls : "icon-add",
							modal : true,
							collapsible : false,
							minimizable : false,
							maximizable : false,
							draggable : true,
							closed : true,
							buttons : [
									{
										text : '添加',
										plain : true,
										iconCls : 'icon-add',
										handler : function() {

											var validate = $("#addForm").form(
													"validate");
											if (!validate) {
												$.messager
														.alert("消息提醒",
																"请检查你输入的数据!",
																"warning");
												return;
											} else {

												var course = $(table).find(
														"input:checked");
												var gradeid = $(course).attr(
														"gradeid");
												var clazzid = $(course).attr(
														"clazzid");
												var courseid = $(course).attr(
														"courseid");
												var name = $("#add_name")
														.textbox("getValue");
												var etime = $("#add_time")
														.textbox("getValue");
												var remark = $("#add_remark")
														.textbox("getValue");
												var type = $("#add_type")
														.textbox("getValue");

												var data = {
													gradeid : gradeid,
													clazzid : clazzid,
													courseid : courseid,
													name : name,
													etime : etime,
													remark : remark,
													type : type
												};
												$
														.ajax({
															type : "post",
															url : "AddExam",
															data : data,
															success : function(
																	msg) {
																if (msg == "success") {
																	$.messager
																			.alert(
																					"消息提醒",
																					"添加成功!",
																					"info");
																	setTimeout(
																			function() {
																				window.location.href="ExaminationList";
																			},
																			1000);
																	//关闭窗口
																	$(
																			"#addDialog")
																			.dialog(
																					"close");
																	//清空原表格数据
																	$(
																			"#add_name")
																			.textbox(
																					'setValue',
																					"");
																	$(
																			"#add_time")
																			.datebox(
																					'setValue',
																					"");
																	$(
																			"#add_remark")
																			.textbox(
																					'setValue',
																					"");

																	//重新刷新页面数据
																	$(
																			'#dataList')
																			.datagrid(
																					"reload");

																} else {
																	$.messager
																			.alert(
																					"消息提醒",
																					"添加失败!",
																					"warning");

																	return;
																}
															}
														});
											}
										}
									},
									{
										text : '重置',
										plain : true,
										iconCls : 'icon-reload',
										handler : function() {
											$("#add_name").textbox('setValue',
													"");
											$("#add_time").datebox('setValue',
													"");
											$("#add_remark").textbox(
													'setValue', "");
										}
									}, ],
							onBeforeOpen : function() {
								$.ajax({
									type : "post",
									url : "TeacherServlet?method=GetTeacher&t="
											+ new Date().getTime(),
									dataType : "json",
									success : function(result) {
										$(table).find(".course :first").attr(
												"checked", "checked");
									}
								});
							},
							onClose : function() {
								$(table).find(".chooseTr").remove();
							}
						});

		//考试成绩窗口
		$("#escoreListDialog").dialog({
			title : "成绩统计",
			width : 850,
			height : 550,
			iconCls : "icon-chart_bar",
			modal : true,
			collapsible : false,
			minimizable : false,
			maximizable : false,
			draggable : true,
			closed : true,
			onClose : function() {
				$("#escoreClazzList").combobox("clear");
			}
		});
		//成绩列表
		$("#escoreList").datagrid({
			border : true,
			collapsible : false,//是否可折叠的 
			fit : true,//自动大小 
			method : "post",
			noheader : true,
			singleSelect : true,//是否单选 
			rownumbers : true,//行号 
			sortOrder : 'DESC',
			remoteSort : false,
			columns : [ [ {
				field : 'a_sno',
				title : '学号',
				width : 120,
				resizable : false,
				sortable : false
			}, {
				field : 'a_name',
				title : '姓名',
				width : 120,
				resizable : false
			}, {
				field : 's_name',
				title : '课程',
				width : 120,
				resizable : false,
				sortable : false
			}, {
				field : 'a_score',
				title : '成绩',
				width : 120,
				resizable : false
			}, ] ],
		});

		//考试成绩窗口
		$("#escoreDialog").dialog({
			title : "考试成绩",
			width : 900,
			height : 550,
			iconCls : "icon-chart_bar",
			modal : true,
			collapsible : false,
			minimizable : false,
			maximizable : false,
			draggable : true,
			closed : true,
		});

	});
</script>
</head>
<body>
	<!-- 数据列表 -->
	<table id="dataList" cellspacing="0" cellpadding="0">
		<c:forEach items="${requestScope.list}" var="Examination">
			<tr>
				<td></td>
				<td>${Examination.e_id }</td>
				<td>${Examination.e_name }</td>
				<td>${Examination.e_time }</td>
				<td>${Examination.e_type }</td>
				<td>${Examination.g_name }</td>
				<td>${Examination.c_name }</td>
				<td>${Examination.e_beizhu }</td>
			</tr>
		</c:forEach>
	</table>

	<!-- 工具栏 -->
	<div id="toolbar">
		<div style="float: left;">
			<a id="add" href="javascript:;" class="easyui-linkbutton"
				data-options="iconCls:'icon-add',plain:true">添加</a>
		</div>
		<div style="float: left;" class="datagrid-btn-separator"></div>
		<div style="float: left;" class="datagrid-btn-separator"></div>
	</div>

	<!-- 考试成绩表 -->
	<div id="escoreListDialog">
		<table id="escoreList" cellspacing="0" cellpadding="0">
			<c:forEach items="${requestScope.list2}" var="achievement">
				<tr>
					<td></td>
					<td>${achievement.a_id }</td>
					<td>${achievement.a_name }</td>
					<td>${achievement.s_name }</td>
					<td>${achievement.a_score }</td>
				</tr>
			</c:forEach>
		</table>
	</div>
	<div id="escoreToolbar">
		<a id="redo" href="javascript:;" class="easyui-linkbutton"
			data-options="iconCls:'icon-redo',plain:true">导出</a> <span
			id="escoreClazzSpan" style="margin-left: 10px;">班级：<input
			id="escoreClazzList" class="easyui-textbox" name="clazz" /></span>
	</div>

	<!-- 登记考试成绩 -->
	<div id="regEscoreDialog"></div>
	<div id="regEscoreToolbar">
		<a id="regiser" href="javascript:;" class="easyui-linkbutton"
			data-options="iconCls:'icon-folder-up',plain:true">提交</a> <a
			id="clear" href="javascript:;" class="easyui-linkbutton"
			data-options="iconCls:'icon-clear',plain:true">清空</a> <span
			id="regClazzSpan" style="margin-left: 10px;">班级：<input
			id="regClazzList" class="easyui-textbox" name="clazz" /></span> <span
			id="regCourseSpan" style="margin-left: 10px;">课程：<input
			id="regCourseList" class="easyui-textbox" name="course" /></span>
	</div>

	<!-- 添加窗口 -->
	<div id="addDialog" style="padding: 10px">
		<form id="addForm" method="post" action="AddExam">
			<table id="addTable" border=0
				style="width: 800px; table-layout: fixed;" cellpadding="6">
				<tr>
					<td style="width: 40px">名称:</td>
					<td colspan="3"><input id="add_name" class="easyui-textbox"
						style="width: 200px; height: 30px;" type="text"
						name="examination.e_name"
						data-options="required:true, missingMessage:'请输入名称'" /></td>
					<td style="width: 80px"></td>
				</tr>
				<tr>
					<td>考试<br />时间:
					</td>
					<td><input id="add_time" style="width: 200px; height: 30px;"
						class="easyui-datebox" type="text" name="examination.e_time"
						data-options="required:true, missingMessage:'请选择日期', editable:false" /></td>
				</tr>
				<tr>
					<td>考试<br />类型:
					</td>
					<td><input id="add_type" style="width: 200px; height: 30px;"
						class="easyui-textbox" data-options="readonly: true" type="text"
						value="平时考试" /></td>
				</tr>
				<tr>
					<td>备注:</td>
					<td><input id="add_remark" style="width: 200px; height: 70px;"
						class="easyui-textbox" data-options="multiline: true,"
						name="examination.e_beizhu" /></td>
				</tr>
				<tr>
					<td><input type="hidden" name="examination.e_grade"></td>
				</tr>
				<tr>
					<td><input type="hidden" name="examination.e_class"></td>
				</tr>
			</table>
		</form>
	</div>




</body>
</html>