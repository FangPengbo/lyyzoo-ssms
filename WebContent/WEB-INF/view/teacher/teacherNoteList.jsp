<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
  <%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="UTF-8">
	<title>教师通讯录</title>
	<link rel="stylesheet" type="text/css" href="easyui/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="easyui/themes/icon.css">
	<link rel="stylesheet" type="text/css" href="easyui/css/demo.css">
	<script type="text/javascript" src="easyui/jquery.min.js"></script>
	<script type="text/javascript" src="easyui/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="easyui/js/validateExtends.js"></script>
	<script type="text/javascript">
	$(function() {	
		
		var table;
		
		//datagrid初始化 
	    $('#dataList').datagrid({ 
	        title:'教师列表', 
	        iconCls:'icon-more',//图标 
	        border: true, 
	        collapsible: false,//是否可折叠的 
	        fit: true,//自动大小 
	        method: "post",
	        url:"selectTeacher",
	        idField:'tid', 
	        singleSelect: true,//是否单选 
	        pagination: true,//分页控件 
	        rownumbers: true,//行号 
	        sortName:'tid',
	        sortOrder:'DESC', 
	        remoteSort: true,
	        columns: [[  
				{field:'chk',checkbox: true,width:50},
 		        {field:'tid',title:'ID',width:50, sortable: true},    
 		        {field:'tgonghao',title:'工号',width:150, sortable: true},    
 		        {field:'tname',title:'姓名',width:150},
 		        {field:'tsex',title:'性别',width:100},
 		        {field:'tphone',title:'电话',width:150},
 		        {field:'tqq',title:'QQ',width:150},
 		        {field:'courseList',title:'课程',width:500, 
 		        	
 		        }
	 		]], 
	        toolbar: [
	        	{
	        		text: '查看',
	        		iconCls: 'icon-zoom-in',
	        		handler: function(){
	        			table = $("#editTable");
	        			var selectRow = $("#dataList").datagrid("getSelected");
	                	if(selectRow == null){
	                    	$.messager.alert("消息提醒", "请选择一个老师", "warning");
	                    } else{
		        			$("#editDialog").dialog("open");
	                    }
	        		}
	        	}          
	        ]
	    }); 
	    //设置分页控件 
	    var p = $('#dataList').datagrid('getPager'); 
	    $(p).pagination({ 
	        pageSize: 10,//每页显示的记录条数，默认为10 
	        pageList: [10,20,30,50,100],//可以设置每页记录条数的列表 
	        beforePageText: '第',//页数文本框前显示的汉字 
	        afterPageText: '页    共 {pages} 页', 
	        displayMsg: '当前显示 {from} - {to} 条记录   共 {total} 条记录', 
	    }); 
	  	
	  	//教师详细信息窗口
	    $("#editDialog").dialog({
	  		title: "教师信息",
	    	width: 850,
	    	height: 550,
	    	iconCls: "icon-man",
	    	modal: true,
	    	collapsible: false,
	    	minimizable: false,
	    	maximizable: false,
	    	draggable: true,
	    	closed: true,
			onBeforeOpen: function(){
				var selectRow = $("#dataList").datagrid("getSelected");
				//设置值
				$("#edit_number").textbox('setValue', selectRow.tgonghao);
				$("#edit_name").textbox('setValue', selectRow.tname);
				$("#edit_sex").textbox('setValue', selectRow.tsex);
				$("#edit_phone").textbox('setValue', selectRow.tphone);
				$("#edit_qq").textbox('setValue', selectRow.tqq);
				$("#edit_photo").attr("src", selectRow.t_photo);
			},
			onClose: function(){
				$("#edit_name").textbox('setValue', "");
				$("#edit_phone").textbox('setValue', "");
				$("#edit_qq").textbox('setValue', "");
				$(table).find(".chooseTr").remove();
			}
	    });
	   	
	});
	</script>
</head>
<body>
	<!-- 数据列表 -->
	<table id="dataList" cellspacing="0" cellpadding="0"> 
	    <c:forEach items="${requestScope.list}" var="teacherNote">
			<tr>
				<td>${teacherNote.t_id }</td>
				<td>${teacherNote.t_id }</td>
				<td>${teacherNote.t_gonghao }</td>
				<td>${teacherNote.t_name }</td>
				<td>${teacherNote.t_sex }</td>
				<td>${teacherNote.t_phone }</td>
				<td>${teacherNote.t_qq }</td>
				<td>
					${teacherNote.g_name }&nbsp;&nbsp;
					${teacherNote.c_name }&nbsp;&nbsp;
					${teacherNote.s_name }
					<input type="hidden"  value="${teacherNote.t_photo }">
								</td>
			</tr>	    
	    </c:forEach>
	</table> 
	
	<!-- 查看窗口 -->
	<div id="editDialog" style="padding: 10px">
    	<form id="editForm" method="post" action="selectTeacherOne">
	    	<table id="editTable" border=0 style="width:800px; table-layout:fixed;" cellpadding="6" >
	    		<input type="hidden" value="${teacher.t_id }" name="teacher.t_id">
	    		<tr>
	    			<td style="width:40px">工号:</td>
	    			<td colspan="3"><input id="edit_number" data-options="readonly: true" class="easyui-textbox" style="width: 200px; height: 30px;" type="text" name="teacher.t_gonghao" /></td>
	    			<td style="width:80px"></td>
	    		</tr>
	    		<tr>
	    			<td>姓名:</td>
	    			<td><input id="edit_name" style="width: 200px; height: 30px;" class="easyui-textbox" type="text" name="teacher.t_name" data-options="readonly: true" /></td>
	    		</tr>
	    		<tr>
	    			<td>性别:</td>
	    			<td><select id="edit_sex" class="easyui-combobox" data-options="editable: false, readonly:true, panelHeight: 50, width: 60, height: 30" name="teacher.t_sex"><option value="男">男</option><option value="女">女</option></select></td>
	    		</tr>
	    		<tr>
	    			<td>电话:</td>
	    			<td><input id="edit_phone" style="width: 200px; height: 30px;" class="easyui-textbox" type="text" name="teacher.t_phone" data-options="readonly: true" /></td>
	    		</tr>
	    		<tr>
	    			<td>QQ:</td>
	    			<td colspan="4"><input id="edit_qq" style="width: 200px; height: 30px;" class="easyui-textbox" type="text" name="teacher.t_qq"  data-options="readonly: true" /></td>
	    		</tr>
	    	</table>
	    </form>
	</div> 
	
	
</body>
</html>