<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="UTF-8">
	<title>教师列表</title>
	<link rel="stylesheet" type="text/css" href="easyui/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="easyui/themes/icon.css">
	<link rel="stylesheet" type="text/css" href="easyui/css/demo.css">
	<script type="text/javascript" src="easyui/jquery.min.js"></script>
	<script type="text/javascript" src="easyui/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="easyui/js/validateExtends.js"></script>
	<script type="text/javascript">
	$(function() {	
		var table;
		
		//datagrid初始化 
	    $('#dataList').datagrid({ 
	        title:'教师列表', 
	        iconCls:'icon-more',//图标 
	        border: true, 
	        collapsible:false,//是否可折叠的 
	        fit: true,//自动大小 
	        method: "post",
	        url:"TeacherServlet?method=TeacherList&t="+new Date().getTime(),
	        idField:'tid', 
	        singleSelect:false,//是否单选 
	        pagination:true,//分页控件 
	        rownumbers:true,//行号 
	        sortName:'tid',
	        sortOrder:'DESC', 
	        remoteSort: true,
	        columns: [[  
 		        {field:'',title:'ID',width:50, sortable: true},    
 		        {field:'tid',title:'ID',width:150, sortable: true},    
 		        {field:'tgonghao',title:'工号',width:150},
 		        {field:'tname',title:'姓名',width:100},
 		        {field:'tsex',title:'性别',width:150},
 		        {field:'tphone',title:'电话',width:150},
 		        {field:'tqq',title:'QQ',width:150, 
 		        },
 		       {field:'courseList',title:'课程',width:500, 
		        }
	 		]], 
	        toolbar: "#toolbar"
	    }); 
	    //设置分页控件 
	    var p = $('#dataList').datagrid('getPager'); 
	    $(p).pagination({ 
	        pageSize: 10,//每页显示的记录条数，默认为10 
	        pageList: [10,20,30,50,100],//可以设置每页记录条数的列表 
	        beforePageText: '第',//页数文本框前显示的汉字 
	        afterPageText: '页    共 {pages} 页', 
	        displayMsg: '当前显示 {from} - {to} 条记录   共 {total} 条记录', 
	    }); 
	    //设置工具类按钮
	    $("#add").click(function(){
	    	$("#addDialog").dialog("open");
	    });
	    //修改
	    $("#edit").click(function(){
	    
	    	var selectRows = $("#dataList").datagrid("getSelections");
        	if(selectRows.length != 1){
            	$.messager.alert("消息提醒", "请选择一条数据进行操作!", "warning");
            } else{
		    	$("#editDialog").dialog("open");
            }
	    });
	    //删除
	    $("#delete").click(function(){
	    	var selectRows = $("#dataList").datagrid("getSelections");
        	var selectLength = selectRows.length;
        	if(selectLength == 0){
            	$.messager.alert("消息提醒", "请选择数据进行删除!", "warning");
            } else{
			            	
            	//var t_id= $("#t_id").val();
            	var obj=document.getElementsByName('checkteahcer'); 
            	var ids = [];
            	for(var i=0;i<obj.length;i++){
            		if(obj[i].checked){
            			ids+=obj[i].value+",";
            		}
            	}
            	$.messager.confirm("消息提醒", "将删除与教师相关的所有数据，确认继续？", function(r){
            		if(r){
            			$.ajax({
							type: "post",
							url: "DeleteTeacher",
							data: {ids:ids},
							success: function(msg){
								if(msg == "success"){
									$.messager.alert("消息提醒","删除成功!","info");

									//刷新表格
									window.location.href = "TeacherServlet";
								} else{
									$.messager.alert("消息提醒","删除失败!","warning");
									return;
								}
							}
						});
            		}
            	});
            }
	    });
	    
	  	//设置添加窗口
	    $("#addDialog").dialog({
	    	title: "添加教师",
	    	width: 850,
	    	height: 550,
	    	iconCls: "icon-add",
	    	modal: true,
	    	collapsible: false,
	    	minimizable: false,
	    	maximizable: false,
	    	draggable: true,
	    	closed: true,
	    	buttons: [
				
	    		{
					text:'添加',
					plain: true,
					iconCls:'icon-user_add',
					handler:function(){
						var validate = $("#addForm").form("validate");
						if(!validate){
							$.messager.alert("消息提醒","请检查你输入的数据!","warning");
							return;
						} else{
						/* 	var t__grade = $(
							"#add_gradeList")
							.combobox("getValue");
					        var t__class = $(
							"#add_clazzList")
							.combobox("getValue"); */
					$
							.ajax({
								type : "post",
								url : "addTeacher",
								data : $("#addForm")
										.serialize(),
								success : function(
										msg) {
									if (msg == "success") {
										$.messager.alert("消息提醒","添加成功!","info");
										//关闭窗口
										$("#addDialog").dialog("close");
										//刷新表格
										window.location.href = "TeacherServlet";
										//清空原表格数据
										$(
												"#add_t_gonghao")
												.textbox(
														'setValue',
														"");
										$(
												"#add_t_name")
												.textbox(
														'setValue',
														"");
										$(
												"#add_t_sex")
												.textbox(
														'setValue',
														"男");
										$(
												"#add_t_phone")
												.textbox(
														'setValue',
														"");
										$(
												"#add_t_qq")
												.textbox(
														'setValue',
														"");
										$("#photo")
												.textbox(
														'setValue',
														"");
									

										//重新刷新页面数据
										$(
												'#dataList')
												.datagrid(
														"options").queryParams = {
											//t_class : t_class
										};
		
										$(
												'#dataList')
												.datagrid(
														"reload");
											
									setTimeout(	
											    function() {
											    $("#add_course")
													.textbox(
															'2',
															"");
											    },
											    
										100);
									/* 	$(
												"#gradeList")
												.combobox(
														'setValue',
														"");
										setTimeout(
												function() {
													$(
															"#clazzList")
															.combobox(
																	'setValue',
																	t_class);
												},
												100); */

									} else {
										$.messager
												.alert(
														"消息提醒",
														"添加失败!",
														"warning");
										return;
									}
								}
							});
				}
			}
		}
							,
				{
					text:'重置',
					plain: true,
					iconCls:'icon-reload',
					handler:function(){
						$("#add_t_gonghao").textbox('setValue', "");
						$("#add_t_name").textbox('setValue', "");
						$("#add_t_phone").textbox('setValue', "");
						$("#add_t_qq").textbox('setValue', "");
						
						//重新加载年级
						$("#add_gradeList").combobox(
								"clear");
						$("#add_gradeList").combobox(
								"reload");
					}
						
					}
				
			],
			
	    });
	  	
	  	//设置课程窗口
	    /* $("#chooseCourseDialog").dialog({
	    	title: "设置课程",
	    	width: 400,
	    	height: 300,
	    	iconCls: "icon-add",
	    	modal: true,
	    	collapsible: false,
	    	minimizable: false,
	    	maximizable: false,
	    	draggable: true,
	    	closed: true,
	    	buttons: [
	    		{
					text:'添加',
					plain: true,
					iconCls:'icon-book-add',
					handler:function(){
			    		//添加之前先判断是否已选择该课程
						var chooseCourse = [];
						$(table).find(".chooseTr").each(function(){
							var gradeid = $(this).find("input[textboxname='gradeid']").attr("gradeId");
							var clazzid = $(this).find("input[textboxname='clazzid']").attr("clazzId");
							var courseid = $(this).find("input[textboxname='courseid']").attr("courseId");
							var course = gradeid+"_"+clazzid+"_"+courseid;
							chooseCourse.push(course);
						});
						//获取新选择的课程
			    		var gradeId = $("#add_gradeList").combobox("getValue");
			    		var clazzId = $("#add_clazzList").combobox("getValue");
			    		var courseId = $("#add_courseList").combobox("getValue");
						var newChoose = gradeId+"_"+clazzId+"_"+courseId;
						for(var i = 0;i < chooseCourse.length;i++){
							if(newChoose == chooseCourse[i]){
								$.messager.alert("消息提醒","已选择该门课程!","info");
								return;
							}
						}
						
						//添加到表格显示
						var tr = $("<tr class='chooseTr'><td>课程:</td></tr>");
						
			    		var gradeName = $("#add_gradeList").combobox("getText");
			    		var gradeTd = $("<td></td>");
			    		var gradeInput = $("<input style='width: 200px; height: 30px;' data-options='readonly: true' class='easyui-textbox' name='gradeid' />").val(gradeName).attr("gradeId", gradeId);
			    		$(gradeInput).appendTo(gradeTd);
			    		$(gradeTd).appendTo(tr);
			    		
			    		var clazzName = $("#add_clazzList").combobox("getText");
			    		var clazzTd = $("<td></td>");
			    		var clazzInput = $("<input style='width: 200px; height: 30px;' data-options='readonly: true' class='easyui-textbox' name='clazzid' />").val(clazzName).attr("clazzId", clazzId);
			    		$(clazzInput).appendTo(clazzTd);
			    		$(clazzTd).appendTo(tr);
			    		
			    		var courseName = $("#add_courseList").combobox("getText");
			    		var courseTd = $("<td></td>");
			    		var courseInput = $("<input style='width: 200px; height: 30px;' data-options='readonly: true' class='easyui-textbox' name='courseid' />").val(courseName).attr("courseId", courseId);
			    		$(courseInput).appendTo(courseTd);
			    		$(courseTd).appendTo(tr);
			    		
			    		var removeTd = $("<td></td>");
			    		var removeA = $("<a href='javascript:;' class='easyui-linkbutton removeBtn'></a>").attr("data-options", "iconCls:'icon-remove'");
			    		$(removeA).appendTo(removeTd);
			    		$(removeTd).appendTo(tr);
			    		
			    		$(tr).appendTo(table);
			    		
			    		//解析
			    		$.parser.parse($(table).find(".chooseTr :last"));
			    		//关闭窗口
			    		$("#chooseCourseDialog").dialog("close");
					}
				}
			]
	    }); */
	  	
	  //下拉框通用属性
	  	$("#add_gradeList, #edit_gradeList, #add_clazzList, #edit_clazzLis，#add_courseList，#edit_courseList").combobox({
	  		width: "200",
	  		height: "30",
	  		valueField: "id",
	  		textField: "name",
	  		multiple: false, //不可多选
	  		editable: false, //不可编辑
	  		method: "post",
	  	});
	  	
	  	/* $("#add_gradeList").combobox({
	  		url: "GradeServlet?method=GradeList&t="+new Date().getTime(),
	  		onChange: function(newValue, oldValue){
	  			//加载该年级下的班级
	  			$("#add_clazzList").combobox("clear");
	  			$("#add_clazzList").combobox("options").queryParams = {gradeid: newValue};
	  			$("#add_clazzList").combobox("reload");
	  			
	  			//加载该年级下的课程
	  			$("#add_courseList").combobox("clear");
	  			$("#add_courseList").combobox("options").queryParams = {gradeid: newValue};
	  			$("#add_courseList").combobox("reload");
	  		},
			onLoadSuccess: function(){
				//默认选择第一条数据
				var data = $(this).combobox("getData");
				$(this).combobox("setValue", data[0].id);
	  		}
	  	});
	  	
	  	$("#add_clazzList").combobox({
	  		url: "ClazzServlet?method=ClazzList&t="+new Date().getTime(),
	  		onLoadSuccess: function(){
				//默认选择第一条数据
				var data = $(this).combobox("getData");
				$(this).combobox("setValue", data[0].id);
	  		}
	  	});
	  	
	  	$("#add_courseList").combobox({
	  		url: "CourseServlet?method=CourseList&t="+new Date().getTime(),
	  		onLoadSuccess: function(){
		  		//默认选择第一条数据
				var data = $(this).combobox("getData");;
				$(this).combobox("setValue", data[0].id);
	  		}
	  	}); */
	  	
	  	//编辑教师信息
	  	$("#editDialog").dialog({
	  		title: "修改教师信息",
	    	width: 850,
	    	height: 550,
	    	iconCls: "icon-edit",
	    	modal: true,
	    	collapsible: false,
	    	minimizable: false,
	    	maximizable: false,
	    	draggable: true,
	    	closed: true,
	    	buttons: [
			
	    		{
					text:'提交',
					plain: true,
					iconCls:'icon-user_add',
					handler:function(){
						var validate = $("#editForm").form("validate");
						if(!validate){
							$.messager.alert("消息提醒","请检查你输入的数据!","warning");
							return;
						} else{
							
	
							
							$.ajax({
								type: "post",
								url: "updateteacher2",
								data: data,
								success: function(msg){
									if(msg == "success"){
										$.messager.alert("消息提醒","修改成功!","info");
										//关闭窗口
										$("#editDialog").dialog("close");
										//清空原表格数据
										$("#edit_number").textbox('setValue', "");
										$("#edit_name").textbox('setValue', "");
										$("#edit_sex").textbox('setValue', "男");
										$("#edit_phone").textbox('setValue', "");
										$("#edit_qq").textbox('setValue', "");
										$(table).find(".chooseTr").remove();
										
										//重新刷新页面数据
							  			$('#dataList').datagrid("reload");
							  			$('#dataList').datagrid("uncheckAll");
										
									} else{
										$.messager.alert("消息提醒","修改失败!","warning");
										return;
									}
								}
							});
						}
					}
				},
				{
					text:'重置',
					plain: true,
					iconCls:'icon-reload',
					handler:function(){
						$("#edit_name").textbox('setValue', "");
						$("#edit_phone").textbox('setValue', "");
						$("#edit_qq").textbox('setValue', "");
						
						$(table).find(".chooseTr").remove();
						
					}
				},
			],
			onBeforeOpen: function(){
				var selectRow = $("#dataList").datagrid("getSelected");
				//设置值
				$("#edit_number").textbox('setValue', selectRow.tgonghao);
				$("#edit_name").textbox('setValue', selectRow.tname);
				$("#edit_sex").textbox('setValue', selectRow.tsex);
				$("#edit_phone").textbox('setValue', selectRow.tphone);
				$("#edit_qq").textbox('setValue', selectRow.tqq);
				$("#edit_photo").attr("src", "PhotoServlet?method=GetPhoto&type=3&number="+selectRow.number);
				
				
			},
			onClose: function(){
				$("#edit_name").textbox('setValue', "");
				$("#edit_phone").textbox('setValue', "");
				$("#edit_qq").textbox('setValue', "");
				
				$(table).find(".chooseTr").remove();
			}
	    });
	   	
	  	// 一行选择课程
	  	$(".removeBtn").live("click", function(){
	  		$(this).parents(".chooseTr").remove();
	  	});
	    
	});
	</script>
</head>
<body>
	<!-- 数据列表 -->
	<table  id="dataList" cellspacing="0" cellpadding="0"> 
	    <c:forEach items="${requestScope.list}" var="list">
	       <tr>
	           <td><input name="checkteahcer" value="${list.t_id}" type="checkbox"/></td>
	           <td><input id="t_id" type="hidden" value="${list.t_id }"/>${list.t_id}</td>
	           <td>${list.t_gonghao}</td>
	           <td>${list.t_name}</td>
	           <td>${list.t_sex}</td>
	           <td>${list.t_phone}</td>
	           <td>${list.t_qq}</td>
	           <td>${list.g_name}
	           ${list.c_name}
	         ${list.s_name}</td>
	       </tr>
	    </c:forEach>
	</table> 
	<!-- 工具栏 -->
	<div id="toolbar">
		<div style="float: left;"><a id="add" href="javascript:;" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true">添加</a></div>
			<div style="float: left;" class="datagrid-btn-separator"></div>
		
			<div style="float: left;" class="datagrid-btn-separator"></div>
		<div><a id="delete" href="javascript:;" class="easyui-linkbutton" data-options="iconCls:'icon-some-delete',plain:true">删除</a></div>
	</div>
	
	<!-- 添加窗口 -->
	<div id="addDialog" style="padding: 10px;">  
   		<div style=" position: absolute; margin-left: 560px; width: 250px; height: 300px; border: 1px solid #EEF4FF" id="photo">
    		<img alt="照片" style="max-width: 250px; max-height: 300px;" title="照片" src="photo/teacher.jpg" />
	    </div> 
   		<form id="addForm" method="post">
	    	<table  cellpadding="6" >
	    		<tr>
	    			<td style="width:40px">工号:</td>
	    			<td colspan="3">
	    				<input id="add_t_gonghao"  class="easyui-textbox" style="width: 200px; height: 30px;" type="text" name="t_gonghao" data-options="required:true, validType:'repeat', missingMessage:'请输入工号'" />
	    			</td>
	    			<td style="width:80px"></td>
	    		</tr>
	    		<tr>
	    			<td>姓名:</td>
	    			<td colspan="4"><input id="add_t_name" style="width: 200px; height: 30px;" class="easyui-textbox" type="text" name="t_name" data-options="required:true, missingMessage:'请填写姓名'" /></td>
	    		</tr>
	    		<tr>
	    			<td>性别:</td>
	    			<td colspan="4"><select id="add_t_sex" class="easyui-combobox" data-options="editable: false, panelHeight: 50, width: 60, height: 30" name="t_sex"><option value="男">男</option><option value="女">女</option></select></td>
	    		</tr>
	    		<tr>
	    			<td>电话:</td>
	    			<td colspan="4"><input id="add_t_phone" style="width: 200px; height: 30px;" class="easyui-textbox" type="text" name="t_phone" validType="mobile" /></td>
	    		</tr>
	    		<tr>
	    			<td>QQ:</td>
	    			<td colspan="4"><input id="add_t_qq" style="width: 200px; height: 30px;" class="easyui-textbox" type="text" name="t_qq" validType="number" /></td>
	    		</tr>
	    		<tr>
	    		   <td><input type="hidden" name="t_grade" id="add_grade"/></td>
	    		</tr>
	    		<tr>
	    		<tr>
	    		   <td><input type="hidden" name="t_class" id="add_class"/></td>
	    		</tr>
	    		<tr>
	    		<tr>
	    		   <td><input type="hidden" name="t_grade" id="add_course"/></td>
	    		</tr>
	    		<%-- <tr>
					<td>年级:</td>
					<td><select name="t_grade" id="add_gradeList"
						class="easyui-textbox"
						data-options="editable: false, panelHeight: 200, width: 200, height: 30">
							<option value="">--请选择--</option>
							<c:forEach items="${requestScope.list1 }" var="list1">
								<option value="${list1.g_id }">${list1.g_name }</option>
							</c:forEach>
					</select></td>
				</tr>
				<tr>
					<td>班级:</td>
					<td><select name="t_class" id="add_clazzList"
						class="easyui-textbox"
						data-options="editable: false, panelHeight: 100, width: 200, height: 30">
							<option value="">--请选择--</option>
							<c:forEach items="${requestScope.list2 }" var="list2">
								<option value="${list2.c_id }">${list2.c_name }</option>
							</c:forEach>
					</select></td>
				</tr>
				<tr>
					<td>课程:</td>
					<td><select name="t_course" id="add_courseList"
						class="easyui-textbox"
						data-options="editable: false, panelHeight: 100, width: 200, height: 30">
							<option value="">--请选择--</option>
							<c:forEach items="${requestScope.list3 }" var="list3">
								<option value="${list3.s_id }">${list3.s_name }</option>
							</c:forEach>
					</select></td>
				</tr> --%>
	    	</table>
	    </form>
	</div>
	
	
	
	<!-- 修改窗口 -->
	<div id="editDialog" style="padding: 10px">
		
    	<form id="editForm" method="post">
	    	<table id="editTable" border=0 style="width:800px; table-layout:fixed;" cellpadding="6" >
	    		<tr>
    			<td><input type="hidden" name="t_id" value="${teachergrade.t_id}"></td>
    		</tr>
	    		<tr>
	    			<td style="width:40px">工号:</td>
	    			<td colspan="3"><input id="edit_number" data-options="readonly: true" class="easyui-textbox" style="width: 200px; height: 30px;" type="text" name="gonghao" data-options="required:true, validType:'repeat', missingMessage:'请输入工号'" value="${teachergrade.t_gonghao }"/></td>
	    			<td style="width:80px"></td>
	    		</tr>
	    		<tr>
	    			<td>姓名:</td>
	    			<td><input id="edit_name" value="${teachergrade.t_gonghao }" style="width: 200px; height: 30px;" class="easyui-textbox" type="text" name="t_name" data-options="required:true, missingMessage:'请填写姓名'" /></td>
	    		</tr>
	    		<tr>
	    			<td>性别:</td>
	    			<td><select id="edit_sex" value="${teachergrade.t_name }" class="easyui-combobox" data-options="editable: false, panelHeight: 50, width: 60, height: 30" name="t_sex"><option value="男">男</option><option value="女">女</option></select></td>
	    		</tr>
	    		<tr>
	    			<td>电话:</td>
	    			<td><input id="edit_phone" value="${teachergrade.t_phone }" style="width: 200px; height: 30px;" class="easyui-textbox" type="text" name="t_phone" validType="mobile" /></td>
	    		</tr>
	    		<tr>
	    			<td>QQ:</td>
	    			<td colspan="4"><input id="edit_qq" value="${teachergrade.t_qq }" style="width: 200px; height: 30px;" class="easyui-textbox" type="text" name="t_qq" validType="number" /></td>
	    		</tr>

	    	</table>
	    </form>
	</div>
	
	
</body>
</html>