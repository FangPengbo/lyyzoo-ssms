package com.baidu.entity;

import java.util.ArrayList;
import java.util.List;

public class AdminerExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public AdminerExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andA_idIsNull() {
            addCriterion("A_id is null");
            return (Criteria) this;
        }

        public Criteria andA_idIsNotNull() {
            addCriterion("A_id is not null");
            return (Criteria) this;
        }

        public Criteria andA_idEqualTo(Integer value) {
            addCriterion("A_id =", value, "a_id");
            return (Criteria) this;
        }

        public Criteria andA_idNotEqualTo(Integer value) {
            addCriterion("A_id <>", value, "a_id");
            return (Criteria) this;
        }

        public Criteria andA_idGreaterThan(Integer value) {
            addCriterion("A_id >", value, "a_id");
            return (Criteria) this;
        }

        public Criteria andA_idGreaterThanOrEqualTo(Integer value) {
            addCriterion("A_id >=", value, "a_id");
            return (Criteria) this;
        }

        public Criteria andA_idLessThan(Integer value) {
            addCriterion("A_id <", value, "a_id");
            return (Criteria) this;
        }

        public Criteria andA_idLessThanOrEqualTo(Integer value) {
            addCriterion("A_id <=", value, "a_id");
            return (Criteria) this;
        }

        public Criteria andA_idIn(List<Integer> values) {
            addCriterion("A_id in", values, "a_id");
            return (Criteria) this;
        }

        public Criteria andA_idNotIn(List<Integer> values) {
            addCriterion("A_id not in", values, "a_id");
            return (Criteria) this;
        }

        public Criteria andA_idBetween(Integer value1, Integer value2) {
            addCriterion("A_id between", value1, value2, "a_id");
            return (Criteria) this;
        }

        public Criteria andA_idNotBetween(Integer value1, Integer value2) {
            addCriterion("A_id not between", value1, value2, "a_id");
            return (Criteria) this;
        }

        public Criteria andA_unameIsNull() {
            addCriterion("A_uname is null");
            return (Criteria) this;
        }

        public Criteria andA_unameIsNotNull() {
            addCriterion("A_uname is not null");
            return (Criteria) this;
        }

        public Criteria andA_unameEqualTo(String value) {
            addCriterion("A_uname =", value, "a_uname");
            return (Criteria) this;
        }

        public Criteria andA_unameNotEqualTo(String value) {
            addCriterion("A_uname <>", value, "a_uname");
            return (Criteria) this;
        }

        public Criteria andA_unameGreaterThan(String value) {
            addCriterion("A_uname >", value, "a_uname");
            return (Criteria) this;
        }

        public Criteria andA_unameGreaterThanOrEqualTo(String value) {
            addCriterion("A_uname >=", value, "a_uname");
            return (Criteria) this;
        }

        public Criteria andA_unameLessThan(String value) {
            addCriterion("A_uname <", value, "a_uname");
            return (Criteria) this;
        }

        public Criteria andA_unameLessThanOrEqualTo(String value) {
            addCriterion("A_uname <=", value, "a_uname");
            return (Criteria) this;
        }

        public Criteria andA_unameLike(String value) {
            addCriterion("A_uname like", value, "a_uname");
            return (Criteria) this;
        }

        public Criteria andA_unameNotLike(String value) {
            addCriterion("A_uname not like", value, "a_uname");
            return (Criteria) this;
        }

        public Criteria andA_unameIn(List<String> values) {
            addCriterion("A_uname in", values, "a_uname");
            return (Criteria) this;
        }

        public Criteria andA_unameNotIn(List<String> values) {
            addCriterion("A_uname not in", values, "a_uname");
            return (Criteria) this;
        }

        public Criteria andA_unameBetween(String value1, String value2) {
            addCriterion("A_uname between", value1, value2, "a_uname");
            return (Criteria) this;
        }

        public Criteria andA_unameNotBetween(String value1, String value2) {
            addCriterion("A_uname not between", value1, value2, "a_uname");
            return (Criteria) this;
        }

        public Criteria andA_pwdIsNull() {
            addCriterion("A_pwd is null");
            return (Criteria) this;
        }

        public Criteria andA_pwdIsNotNull() {
            addCriterion("A_pwd is not null");
            return (Criteria) this;
        }

        public Criteria andA_pwdEqualTo(String value) {
            addCriterion("A_pwd =", value, "a_pwd");
            return (Criteria) this;
        }

        public Criteria andA_pwdNotEqualTo(String value) {
            addCriterion("A_pwd <>", value, "a_pwd");
            return (Criteria) this;
        }

        public Criteria andA_pwdGreaterThan(String value) {
            addCriterion("A_pwd >", value, "a_pwd");
            return (Criteria) this;
        }

        public Criteria andA_pwdGreaterThanOrEqualTo(String value) {
            addCriterion("A_pwd >=", value, "a_pwd");
            return (Criteria) this;
        }

        public Criteria andA_pwdLessThan(String value) {
            addCriterion("A_pwd <", value, "a_pwd");
            return (Criteria) this;
        }

        public Criteria andA_pwdLessThanOrEqualTo(String value) {
            addCriterion("A_pwd <=", value, "a_pwd");
            return (Criteria) this;
        }

        public Criteria andA_pwdLike(String value) {
            addCriterion("A_pwd like", value, "a_pwd");
            return (Criteria) this;
        }

        public Criteria andA_pwdNotLike(String value) {
            addCriterion("A_pwd not like", value, "a_pwd");
            return (Criteria) this;
        }

        public Criteria andA_pwdIn(List<String> values) {
            addCriterion("A_pwd in", values, "a_pwd");
            return (Criteria) this;
        }

        public Criteria andA_pwdNotIn(List<String> values) {
            addCriterion("A_pwd not in", values, "a_pwd");
            return (Criteria) this;
        }

        public Criteria andA_pwdBetween(String value1, String value2) {
            addCriterion("A_pwd between", value1, value2, "a_pwd");
            return (Criteria) this;
        }

        public Criteria andA_pwdNotBetween(String value1, String value2) {
            addCriterion("A_pwd not between", value1, value2, "a_pwd");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}