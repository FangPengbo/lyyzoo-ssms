package com.baidu.entity;

public class TeacherGradeClassCourse {
	private int t_id;
	
	private String t_gonghao;
	
	private String t_name;

    private String t_sex;

    private String t_phone;

    private String t_qq;
    
    private String g_name;
    
    private String c_name;
    
    private String s_name;
    
    private int c_id;
    
    private int g_id;
    
    private int s_id;

	public int getT_id() {
		return t_id;
	}

	public void setT_id(int t_id) {
		this.t_id = t_id;
	}

	public String getT_gonghao() {
		return t_gonghao;
	}

	public void setT_gonghao(String t_gonghao) {
		this.t_gonghao = t_gonghao;
	}

	public String getT_name() {
		return t_name;
	}

	public void setT_name(String t_name) {
		this.t_name = t_name;
	}

	public String getT_sex() {
		return t_sex;
	}

	public void setT_sex(String t_sex) {
		this.t_sex = t_sex;
	}

	public String getT_phone() {
		return t_phone;
	}

	public void setT_phone(String t_phone) {
		this.t_phone = t_phone;
	}

	public String getT_qq() {
		return t_qq;
	}

	public void setT_qq(String t_qq) {
		this.t_qq = t_qq;
	}

	public String getG_name() {
		return g_name;
	}

	public void setG_name(String g_name) {
		this.g_name = g_name;
	}

	public String getC_name() {
		return c_name;
	}

	public void setC_name(String c_name) {
		this.c_name = c_name;
	}

	public String getS_name() {
		return s_name;
	}

	public void setS_name(String s_name) {
		this.s_name = s_name;
	}
	
	

	public int getC_id() {
		return c_id;
	}

	public void setC_id(int c_id) {
		this.c_id = c_id;
	}

	public int getG_id() {
		return g_id;
	}

	public void setG_id(int g_id) {
		this.g_id = g_id;
	}

	public int getS_id() {
		return s_id;
	}

	public void setS_id(int s_id) {
		this.s_id = s_id;
	}

	@Override
	public String toString() {
		return "TeacherGradeClassCourse [t_id=" + t_id + ", t_gonghao=" + t_gonghao + ", t_name=" + t_name + ", t_sex="
				+ t_sex + ", t_phone=" + t_phone + ", t_qq=" + t_qq + ", g_name=" + g_name + ", c_name=" + c_name
				+ ", s_name=" + s_name + "]";
	}
    

}
