package com.baidu.entity;
/**
 * AchievementAndExaminationAndCourse
 * @author FangPengbo
 *
 */
public class AchiAExamACourse {
	private Integer a_id;

    private Integer a_sno;

    private String a_name;
    
    private Integer a_score;

    private Integer e_id;

    private String e_name;
    
    private String s_name;
    


	public Integer getA_id() {
		return a_id;
	}

	public void setA_id(Integer a_id) {
		this.a_id = a_id;
	}

	public Integer getA_sno() {
		return a_sno;
	}

	public void setA_sno(Integer a_sno) {
		this.a_sno = a_sno;
	}

	public String getA_name() {
		return a_name;
	}

	public void setA_name(String a_name) {
		this.a_name = a_name;
	}

	public Integer getA_score() {
		return a_score;
	}

	public void setA_score(Integer a_score) {
		this.a_score = a_score;
	}

	public Integer getE_id() {
		return e_id;
	}

	public void setE_id(Integer e_id) {
		this.e_id = e_id;
	}

	public String getE_name() {
		return e_name;
	}

	public void setE_name(String e_name) {
		this.e_name = e_name;
	}

	public String getS_name() {
		return s_name;
	}

	public void setS_name(String s_name) {
		this.s_name = s_name;
	}

	
	
	
}
