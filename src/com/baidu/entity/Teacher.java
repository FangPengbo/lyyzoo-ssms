package com.baidu.entity;

public class Teacher {
    private Integer t_id;

    private String t_gonghao;

    private String t_name;

    private String t_sex;

    private String t_phone;

    private String t_qq;

    private Integer t_class;

    private Integer t_grade;

    private Integer t_course;

    private String t_photo;

    public Integer getT_id() {
        return t_id;
    }

    public void setT_id(Integer t_id) {
        this.t_id = t_id;
    }

    public String getT_gonghao() {
        return t_gonghao;
    }

    public void setT_gonghao(String t_gonghao) {
        this.t_gonghao = t_gonghao == null ? null : t_gonghao.trim();
    }

    public String getT_name() {
        return t_name;
    }

    public void setT_name(String t_name) {
        this.t_name = t_name == null ? null : t_name.trim();
    }

    public String getT_sex() {
        return t_sex;
    }

    public void setT_sex(String t_sex) {
        this.t_sex = t_sex == null ? null : t_sex.trim();
    }

    public String getT_phone() {
        return t_phone;
    }

    public void setT_phone(String t_phone) {
        this.t_phone = t_phone == null ? null : t_phone.trim();
    }

    public String getT_qq() {
        return t_qq;
    }

    public void setT_qq(String t_qq) {
        this.t_qq = t_qq == null ? null : t_qq.trim();
    }

    public Integer getT_class() {
        return t_class;
    }

    public void setT_class(Integer t_class) {
        this.t_class = t_class;
    }

    public Integer getT_grade() {
        return t_grade;
    }

    public void setT_grade(Integer t_grade) {
        this.t_grade = t_grade;
    }

    public Integer getT_course() {
        return t_course;
    }

    public void setT_course(Integer t_course) {
        this.t_course = t_course;
    }

    public String getT_photo() {
        return t_photo;
    }

    public void setT_photo(String t_photo) {
        this.t_photo = t_photo == null ? null : t_photo.trim();
    }
}