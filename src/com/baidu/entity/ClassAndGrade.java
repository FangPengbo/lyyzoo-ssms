package com.baidu.entity;

public class ClassAndGrade {
		private int c_id;
		private String c_name;
		private String g_name;
		public int getC_id() {
			return c_id;
		}
		public void setC_id(int c_id) {
			this.c_id = c_id;
		}
		public String getC_name() {
			return c_name;
		}
		public void setC_name(String c_name) {
			this.c_name = c_name;
		}
		public String getG_name() {
			return g_name;
		}
		public void setG_name(String g_name) {
			this.g_name = g_name;
		}
		
		
}
