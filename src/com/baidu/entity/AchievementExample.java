package com.baidu.entity;

import java.util.ArrayList;
import java.util.List;

public class AchievementExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public AchievementExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andA_idIsNull() {
            addCriterion("A_id is null");
            return (Criteria) this;
        }

        public Criteria andA_idIsNotNull() {
            addCriterion("A_id is not null");
            return (Criteria) this;
        }

        public Criteria andA_idEqualTo(Integer value) {
            addCriterion("A_id =", value, "a_id");
            return (Criteria) this;
        }

        public Criteria andA_idNotEqualTo(Integer value) {
            addCriterion("A_id <>", value, "a_id");
            return (Criteria) this;
        }

        public Criteria andA_idGreaterThan(Integer value) {
            addCriterion("A_id >", value, "a_id");
            return (Criteria) this;
        }

        public Criteria andA_idGreaterThanOrEqualTo(Integer value) {
            addCriterion("A_id >=", value, "a_id");
            return (Criteria) this;
        }

        public Criteria andA_idLessThan(Integer value) {
            addCriterion("A_id <", value, "a_id");
            return (Criteria) this;
        }

        public Criteria andA_idLessThanOrEqualTo(Integer value) {
            addCriterion("A_id <=", value, "a_id");
            return (Criteria) this;
        }

        public Criteria andA_idIn(List<Integer> values) {
            addCriterion("A_id in", values, "a_id");
            return (Criteria) this;
        }

        public Criteria andA_idNotIn(List<Integer> values) {
            addCriterion("A_id not in", values, "a_id");
            return (Criteria) this;
        }

        public Criteria andA_idBetween(Integer value1, Integer value2) {
            addCriterion("A_id between", value1, value2, "a_id");
            return (Criteria) this;
        }

        public Criteria andA_idNotBetween(Integer value1, Integer value2) {
            addCriterion("A_id not between", value1, value2, "a_id");
            return (Criteria) this;
        }

        public Criteria andA_snoIsNull() {
            addCriterion("A_sno is null");
            return (Criteria) this;
        }

        public Criteria andA_snoIsNotNull() {
            addCriterion("A_sno is not null");
            return (Criteria) this;
        }

        public Criteria andA_snoEqualTo(Integer value) {
            addCriterion("A_sno =", value, "a_sno");
            return (Criteria) this;
        }

        public Criteria andA_snoNotEqualTo(Integer value) {
            addCriterion("A_sno <>", value, "a_sno");
            return (Criteria) this;
        }

        public Criteria andA_snoGreaterThan(Integer value) {
            addCriterion("A_sno >", value, "a_sno");
            return (Criteria) this;
        }

        public Criteria andA_snoGreaterThanOrEqualTo(Integer value) {
            addCriterion("A_sno >=", value, "a_sno");
            return (Criteria) this;
        }

        public Criteria andA_snoLessThan(Integer value) {
            addCriterion("A_sno <", value, "a_sno");
            return (Criteria) this;
        }

        public Criteria andA_snoLessThanOrEqualTo(Integer value) {
            addCriterion("A_sno <=", value, "a_sno");
            return (Criteria) this;
        }

        public Criteria andA_snoIn(List<Integer> values) {
            addCriterion("A_sno in", values, "a_sno");
            return (Criteria) this;
        }

        public Criteria andA_snoNotIn(List<Integer> values) {
            addCriterion("A_sno not in", values, "a_sno");
            return (Criteria) this;
        }

        public Criteria andA_snoBetween(Integer value1, Integer value2) {
            addCriterion("A_sno between", value1, value2, "a_sno");
            return (Criteria) this;
        }

        public Criteria andA_snoNotBetween(Integer value1, Integer value2) {
            addCriterion("A_sno not between", value1, value2, "a_sno");
            return (Criteria) this;
        }

        public Criteria andA_nameIsNull() {
            addCriterion("A_name is null");
            return (Criteria) this;
        }

        public Criteria andA_nameIsNotNull() {
            addCriterion("A_name is not null");
            return (Criteria) this;
        }

        public Criteria andA_nameEqualTo(String value) {
            addCriterion("A_name =", value, "a_name");
            return (Criteria) this;
        }

        public Criteria andA_nameNotEqualTo(String value) {
            addCriterion("A_name <>", value, "a_name");
            return (Criteria) this;
        }

        public Criteria andA_nameGreaterThan(String value) {
            addCriterion("A_name >", value, "a_name");
            return (Criteria) this;
        }

        public Criteria andA_nameGreaterThanOrEqualTo(String value) {
            addCriterion("A_name >=", value, "a_name");
            return (Criteria) this;
        }

        public Criteria andA_nameLessThan(String value) {
            addCriterion("A_name <", value, "a_name");
            return (Criteria) this;
        }

        public Criteria andA_nameLessThanOrEqualTo(String value) {
            addCriterion("A_name <=", value, "a_name");
            return (Criteria) this;
        }

        public Criteria andA_nameLike(String value) {
            addCriterion("A_name like", value, "a_name");
            return (Criteria) this;
        }

        public Criteria andA_nameNotLike(String value) {
            addCriterion("A_name not like", value, "a_name");
            return (Criteria) this;
        }

        public Criteria andA_nameIn(List<String> values) {
            addCriterion("A_name in", values, "a_name");
            return (Criteria) this;
        }

        public Criteria andA_nameNotIn(List<String> values) {
            addCriterion("A_name not in", values, "a_name");
            return (Criteria) this;
        }

        public Criteria andA_nameBetween(String value1, String value2) {
            addCriterion("A_name between", value1, value2, "a_name");
            return (Criteria) this;
        }

        public Criteria andA_nameNotBetween(String value1, String value2) {
            addCriterion("A_name not between", value1, value2, "a_name");
            return (Criteria) this;
        }

        public Criteria andA_courseIsNull() {
            addCriterion("A_course is null");
            return (Criteria) this;
        }

        public Criteria andA_courseIsNotNull() {
            addCriterion("A_course is not null");
            return (Criteria) this;
        }

        public Criteria andA_courseEqualTo(Integer value) {
            addCriterion("A_course =", value, "a_course");
            return (Criteria) this;
        }

        public Criteria andA_courseNotEqualTo(Integer value) {
            addCriterion("A_course <>", value, "a_course");
            return (Criteria) this;
        }

        public Criteria andA_courseGreaterThan(Integer value) {
            addCriterion("A_course >", value, "a_course");
            return (Criteria) this;
        }

        public Criteria andA_courseGreaterThanOrEqualTo(Integer value) {
            addCriterion("A_course >=", value, "a_course");
            return (Criteria) this;
        }

        public Criteria andA_courseLessThan(Integer value) {
            addCriterion("A_course <", value, "a_course");
            return (Criteria) this;
        }

        public Criteria andA_courseLessThanOrEqualTo(Integer value) {
            addCriterion("A_course <=", value, "a_course");
            return (Criteria) this;
        }

        public Criteria andA_courseIn(List<Integer> values) {
            addCriterion("A_course in", values, "a_course");
            return (Criteria) this;
        }

        public Criteria andA_courseNotIn(List<Integer> values) {
            addCriterion("A_course not in", values, "a_course");
            return (Criteria) this;
        }

        public Criteria andA_courseBetween(Integer value1, Integer value2) {
            addCriterion("A_course between", value1, value2, "a_course");
            return (Criteria) this;
        }

        public Criteria andA_courseNotBetween(Integer value1, Integer value2) {
            addCriterion("A_course not between", value1, value2, "a_course");
            return (Criteria) this;
        }

        public Criteria andA_examinationIsNull() {
            addCriterion("A_examination is null");
            return (Criteria) this;
        }

        public Criteria andA_examinationIsNotNull() {
            addCriterion("A_examination is not null");
            return (Criteria) this;
        }

        public Criteria andA_examinationEqualTo(Integer value) {
            addCriterion("A_examination =", value, "a_examination");
            return (Criteria) this;
        }

        public Criteria andA_examinationNotEqualTo(Integer value) {
            addCriterion("A_examination <>", value, "a_examination");
            return (Criteria) this;
        }

        public Criteria andA_examinationGreaterThan(Integer value) {
            addCriterion("A_examination >", value, "a_examination");
            return (Criteria) this;
        }

        public Criteria andA_examinationGreaterThanOrEqualTo(Integer value) {
            addCriterion("A_examination >=", value, "a_examination");
            return (Criteria) this;
        }

        public Criteria andA_examinationLessThan(Integer value) {
            addCriterion("A_examination <", value, "a_examination");
            return (Criteria) this;
        }

        public Criteria andA_examinationLessThanOrEqualTo(Integer value) {
            addCriterion("A_examination <=", value, "a_examination");
            return (Criteria) this;
        }

        public Criteria andA_examinationIn(List<Integer> values) {
            addCriterion("A_examination in", values, "a_examination");
            return (Criteria) this;
        }

        public Criteria andA_examinationNotIn(List<Integer> values) {
            addCriterion("A_examination not in", values, "a_examination");
            return (Criteria) this;
        }

        public Criteria andA_examinationBetween(Integer value1, Integer value2) {
            addCriterion("A_examination between", value1, value2, "a_examination");
            return (Criteria) this;
        }

        public Criteria andA_examinationNotBetween(Integer value1, Integer value2) {
            addCriterion("A_examination not between", value1, value2, "a_examination");
            return (Criteria) this;
        }

        public Criteria andA_scoreIsNull() {
            addCriterion("A_score is null");
            return (Criteria) this;
        }

        public Criteria andA_scoreIsNotNull() {
            addCriterion("A_score is not null");
            return (Criteria) this;
        }

        public Criteria andA_scoreEqualTo(Integer value) {
            addCriterion("A_score =", value, "a_score");
            return (Criteria) this;
        }

        public Criteria andA_scoreNotEqualTo(Integer value) {
            addCriterion("A_score <>", value, "a_score");
            return (Criteria) this;
        }

        public Criteria andA_scoreGreaterThan(Integer value) {
            addCriterion("A_score >", value, "a_score");
            return (Criteria) this;
        }

        public Criteria andA_scoreGreaterThanOrEqualTo(Integer value) {
            addCriterion("A_score >=", value, "a_score");
            return (Criteria) this;
        }

        public Criteria andA_scoreLessThan(Integer value) {
            addCriterion("A_score <", value, "a_score");
            return (Criteria) this;
        }

        public Criteria andA_scoreLessThanOrEqualTo(Integer value) {
            addCriterion("A_score <=", value, "a_score");
            return (Criteria) this;
        }

        public Criteria andA_scoreIn(List<Integer> values) {
            addCriterion("A_score in", values, "a_score");
            return (Criteria) this;
        }

        public Criteria andA_scoreNotIn(List<Integer> values) {
            addCriterion("A_score not in", values, "a_score");
            return (Criteria) this;
        }

        public Criteria andA_scoreBetween(Integer value1, Integer value2) {
            addCriterion("A_score between", value1, value2, "a_score");
            return (Criteria) this;
        }

        public Criteria andA_scoreNotBetween(Integer value1, Integer value2) {
            addCriterion("A_score not between", value1, value2, "a_score");
            return (Criteria) this;
        }

        public Criteria andA_gradeIsNull() {
            addCriterion("A_grade is null");
            return (Criteria) this;
        }

        public Criteria andA_gradeIsNotNull() {
            addCriterion("A_grade is not null");
            return (Criteria) this;
        }

        public Criteria andA_gradeEqualTo(Integer value) {
            addCriterion("A_grade =", value, "a_grade");
            return (Criteria) this;
        }

        public Criteria andA_gradeNotEqualTo(Integer value) {
            addCriterion("A_grade <>", value, "a_grade");
            return (Criteria) this;
        }

        public Criteria andA_gradeGreaterThan(Integer value) {
            addCriterion("A_grade >", value, "a_grade");
            return (Criteria) this;
        }

        public Criteria andA_gradeGreaterThanOrEqualTo(Integer value) {
            addCriterion("A_grade >=", value, "a_grade");
            return (Criteria) this;
        }

        public Criteria andA_gradeLessThan(Integer value) {
            addCriterion("A_grade <", value, "a_grade");
            return (Criteria) this;
        }

        public Criteria andA_gradeLessThanOrEqualTo(Integer value) {
            addCriterion("A_grade <=", value, "a_grade");
            return (Criteria) this;
        }

        public Criteria andA_gradeIn(List<Integer> values) {
            addCriterion("A_grade in", values, "a_grade");
            return (Criteria) this;
        }

        public Criteria andA_gradeNotIn(List<Integer> values) {
            addCriterion("A_grade not in", values, "a_grade");
            return (Criteria) this;
        }

        public Criteria andA_gradeBetween(Integer value1, Integer value2) {
            addCriterion("A_grade between", value1, value2, "a_grade");
            return (Criteria) this;
        }

        public Criteria andA_gradeNotBetween(Integer value1, Integer value2) {
            addCriterion("A_grade not between", value1, value2, "a_grade");
            return (Criteria) this;
        }

        public Criteria andA_classIsNull() {
            addCriterion("A_class is null");
            return (Criteria) this;
        }

        public Criteria andA_classIsNotNull() {
            addCriterion("A_class is not null");
            return (Criteria) this;
        }

        public Criteria andA_classEqualTo(Integer value) {
            addCriterion("A_class =", value, "a_class");
            return (Criteria) this;
        }

        public Criteria andA_classNotEqualTo(Integer value) {
            addCriterion("A_class <>", value, "a_class");
            return (Criteria) this;
        }

        public Criteria andA_classGreaterThan(Integer value) {
            addCriterion("A_class >", value, "a_class");
            return (Criteria) this;
        }

        public Criteria andA_classGreaterThanOrEqualTo(Integer value) {
            addCriterion("A_class >=", value, "a_class");
            return (Criteria) this;
        }

        public Criteria andA_classLessThan(Integer value) {
            addCriterion("A_class <", value, "a_class");
            return (Criteria) this;
        }

        public Criteria andA_classLessThanOrEqualTo(Integer value) {
            addCriterion("A_class <=", value, "a_class");
            return (Criteria) this;
        }

        public Criteria andA_classIn(List<Integer> values) {
            addCriterion("A_class in", values, "a_class");
            return (Criteria) this;
        }

        public Criteria andA_classNotIn(List<Integer> values) {
            addCriterion("A_class not in", values, "a_class");
            return (Criteria) this;
        }

        public Criteria andA_classBetween(Integer value1, Integer value2) {
            addCriterion("A_class between", value1, value2, "a_class");
            return (Criteria) this;
        }

        public Criteria andA_classNotBetween(Integer value1, Integer value2) {
            addCriterion("A_class not between", value1, value2, "a_class");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}