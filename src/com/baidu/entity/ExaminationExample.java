package com.baidu.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class ExaminationExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public ExaminationExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andE_idIsNull() {
            addCriterion("E_id is null");
            return (Criteria) this;
        }

        public Criteria andE_idIsNotNull() {
            addCriterion("E_id is not null");
            return (Criteria) this;
        }

        public Criteria andE_idEqualTo(Integer value) {
            addCriterion("E_id =", value, "e_id");
            return (Criteria) this;
        }

        public Criteria andE_idNotEqualTo(Integer value) {
            addCriterion("E_id <>", value, "e_id");
            return (Criteria) this;
        }

        public Criteria andE_idGreaterThan(Integer value) {
            addCriterion("E_id >", value, "e_id");
            return (Criteria) this;
        }

        public Criteria andE_idGreaterThanOrEqualTo(Integer value) {
            addCriterion("E_id >=", value, "e_id");
            return (Criteria) this;
        }

        public Criteria andE_idLessThan(Integer value) {
            addCriterion("E_id <", value, "e_id");
            return (Criteria) this;
        }

        public Criteria andE_idLessThanOrEqualTo(Integer value) {
            addCriterion("E_id <=", value, "e_id");
            return (Criteria) this;
        }

        public Criteria andE_idIn(List<Integer> values) {
            addCriterion("E_id in", values, "e_id");
            return (Criteria) this;
        }

        public Criteria andE_idNotIn(List<Integer> values) {
            addCriterion("E_id not in", values, "e_id");
            return (Criteria) this;
        }

        public Criteria andE_idBetween(Integer value1, Integer value2) {
            addCriterion("E_id between", value1, value2, "e_id");
            return (Criteria) this;
        }

        public Criteria andE_idNotBetween(Integer value1, Integer value2) {
            addCriterion("E_id not between", value1, value2, "e_id");
            return (Criteria) this;
        }

        public Criteria andE_nameIsNull() {
            addCriterion("E_name is null");
            return (Criteria) this;
        }

        public Criteria andE_nameIsNotNull() {
            addCriterion("E_name is not null");
            return (Criteria) this;
        }

        public Criteria andE_nameEqualTo(String value) {
            addCriterion("E_name =", value, "e_name");
            return (Criteria) this;
        }

        public Criteria andE_nameNotEqualTo(String value) {
            addCriterion("E_name <>", value, "e_name");
            return (Criteria) this;
        }

        public Criteria andE_nameGreaterThan(String value) {
            addCriterion("E_name >", value, "e_name");
            return (Criteria) this;
        }

        public Criteria andE_nameGreaterThanOrEqualTo(String value) {
            addCriterion("E_name >=", value, "e_name");
            return (Criteria) this;
        }

        public Criteria andE_nameLessThan(String value) {
            addCriterion("E_name <", value, "e_name");
            return (Criteria) this;
        }

        public Criteria andE_nameLessThanOrEqualTo(String value) {
            addCriterion("E_name <=", value, "e_name");
            return (Criteria) this;
        }

        public Criteria andE_nameLike(String value) {
            addCriterion("E_name like", value, "e_name");
            return (Criteria) this;
        }

        public Criteria andE_nameNotLike(String value) {
            addCriterion("E_name not like", value, "e_name");
            return (Criteria) this;
        }

        public Criteria andE_nameIn(List<String> values) {
            addCriterion("E_name in", values, "e_name");
            return (Criteria) this;
        }

        public Criteria andE_nameNotIn(List<String> values) {
            addCriterion("E_name not in", values, "e_name");
            return (Criteria) this;
        }

        public Criteria andE_nameBetween(String value1, String value2) {
            addCriterion("E_name between", value1, value2, "e_name");
            return (Criteria) this;
        }

        public Criteria andE_nameNotBetween(String value1, String value2) {
            addCriterion("E_name not between", value1, value2, "e_name");
            return (Criteria) this;
        }

        public Criteria andE_timeIsNull() {
            addCriterion("E_time is null");
            return (Criteria) this;
        }

        public Criteria andE_timeIsNotNull() {
            addCriterion("E_time is not null");
            return (Criteria) this;
        }

        public Criteria andE_timeEqualTo(Date value) {
            addCriterionForJDBCDate("E_time =", value, "e_time");
            return (Criteria) this;
        }

        public Criteria andE_timeNotEqualTo(Date value) {
            addCriterionForJDBCDate("E_time <>", value, "e_time");
            return (Criteria) this;
        }

        public Criteria andE_timeGreaterThan(Date value) {
            addCriterionForJDBCDate("E_time >", value, "e_time");
            return (Criteria) this;
        }

        public Criteria andE_timeGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("E_time >=", value, "e_time");
            return (Criteria) this;
        }

        public Criteria andE_timeLessThan(Date value) {
            addCriterionForJDBCDate("E_time <", value, "e_time");
            return (Criteria) this;
        }

        public Criteria andE_timeLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("E_time <=", value, "e_time");
            return (Criteria) this;
        }

        public Criteria andE_timeIn(List<Date> values) {
            addCriterionForJDBCDate("E_time in", values, "e_time");
            return (Criteria) this;
        }

        public Criteria andE_timeNotIn(List<Date> values) {
            addCriterionForJDBCDate("E_time not in", values, "e_time");
            return (Criteria) this;
        }

        public Criteria andE_timeBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("E_time between", value1, value2, "e_time");
            return (Criteria) this;
        }

        public Criteria andE_timeNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("E_time not between", value1, value2, "e_time");
            return (Criteria) this;
        }

        public Criteria andE_typeIsNull() {
            addCriterion("E_type is null");
            return (Criteria) this;
        }

        public Criteria andE_typeIsNotNull() {
            addCriterion("E_type is not null");
            return (Criteria) this;
        }

        public Criteria andE_typeEqualTo(String value) {
            addCriterion("E_type =", value, "e_type");
            return (Criteria) this;
        }

        public Criteria andE_typeNotEqualTo(String value) {
            addCriterion("E_type <>", value, "e_type");
            return (Criteria) this;
        }

        public Criteria andE_typeGreaterThan(String value) {
            addCriterion("E_type >", value, "e_type");
            return (Criteria) this;
        }

        public Criteria andE_typeGreaterThanOrEqualTo(String value) {
            addCriterion("E_type >=", value, "e_type");
            return (Criteria) this;
        }

        public Criteria andE_typeLessThan(String value) {
            addCriterion("E_type <", value, "e_type");
            return (Criteria) this;
        }

        public Criteria andE_typeLessThanOrEqualTo(String value) {
            addCriterion("E_type <=", value, "e_type");
            return (Criteria) this;
        }

        public Criteria andE_typeLike(String value) {
            addCriterion("E_type like", value, "e_type");
            return (Criteria) this;
        }

        public Criteria andE_typeNotLike(String value) {
            addCriterion("E_type not like", value, "e_type");
            return (Criteria) this;
        }

        public Criteria andE_typeIn(List<String> values) {
            addCriterion("E_type in", values, "e_type");
            return (Criteria) this;
        }

        public Criteria andE_typeNotIn(List<String> values) {
            addCriterion("E_type not in", values, "e_type");
            return (Criteria) this;
        }

        public Criteria andE_typeBetween(String value1, String value2) {
            addCriterion("E_type between", value1, value2, "e_type");
            return (Criteria) this;
        }

        public Criteria andE_typeNotBetween(String value1, String value2) {
            addCriterion("E_type not between", value1, value2, "e_type");
            return (Criteria) this;
        }

        public Criteria andE_gradeIsNull() {
            addCriterion("E_grade is null");
            return (Criteria) this;
        }

        public Criteria andE_gradeIsNotNull() {
            addCriterion("E_grade is not null");
            return (Criteria) this;
        }

        public Criteria andE_gradeEqualTo(Integer value) {
            addCriterion("E_grade =", value, "e_grade");
            return (Criteria) this;
        }

        public Criteria andE_gradeNotEqualTo(Integer value) {
            addCriterion("E_grade <>", value, "e_grade");
            return (Criteria) this;
        }

        public Criteria andE_gradeGreaterThan(Integer value) {
            addCriterion("E_grade >", value, "e_grade");
            return (Criteria) this;
        }

        public Criteria andE_gradeGreaterThanOrEqualTo(Integer value) {
            addCriterion("E_grade >=", value, "e_grade");
            return (Criteria) this;
        }

        public Criteria andE_gradeLessThan(Integer value) {
            addCriterion("E_grade <", value, "e_grade");
            return (Criteria) this;
        }

        public Criteria andE_gradeLessThanOrEqualTo(Integer value) {
            addCriterion("E_grade <=", value, "e_grade");
            return (Criteria) this;
        }

        public Criteria andE_gradeIn(List<Integer> values) {
            addCriterion("E_grade in", values, "e_grade");
            return (Criteria) this;
        }

        public Criteria andE_gradeNotIn(List<Integer> values) {
            addCriterion("E_grade not in", values, "e_grade");
            return (Criteria) this;
        }

        public Criteria andE_gradeBetween(Integer value1, Integer value2) {
            addCriterion("E_grade between", value1, value2, "e_grade");
            return (Criteria) this;
        }

        public Criteria andE_gradeNotBetween(Integer value1, Integer value2) {
            addCriterion("E_grade not between", value1, value2, "e_grade");
            return (Criteria) this;
        }

        public Criteria andE_classIsNull() {
            addCriterion("E_class is null");
            return (Criteria) this;
        }

        public Criteria andE_classIsNotNull() {
            addCriterion("E_class is not null");
            return (Criteria) this;
        }

        public Criteria andE_classEqualTo(Integer value) {
            addCriterion("E_class =", value, "e_class");
            return (Criteria) this;
        }

        public Criteria andE_classNotEqualTo(Integer value) {
            addCriterion("E_class <>", value, "e_class");
            return (Criteria) this;
        }

        public Criteria andE_classGreaterThan(Integer value) {
            addCriterion("E_class >", value, "e_class");
            return (Criteria) this;
        }

        public Criteria andE_classGreaterThanOrEqualTo(Integer value) {
            addCriterion("E_class >=", value, "e_class");
            return (Criteria) this;
        }

        public Criteria andE_classLessThan(Integer value) {
            addCriterion("E_class <", value, "e_class");
            return (Criteria) this;
        }

        public Criteria andE_classLessThanOrEqualTo(Integer value) {
            addCriterion("E_class <=", value, "e_class");
            return (Criteria) this;
        }

        public Criteria andE_classIn(List<Integer> values) {
            addCriterion("E_class in", values, "e_class");
            return (Criteria) this;
        }

        public Criteria andE_classNotIn(List<Integer> values) {
            addCriterion("E_class not in", values, "e_class");
            return (Criteria) this;
        }

        public Criteria andE_classBetween(Integer value1, Integer value2) {
            addCriterion("E_class between", value1, value2, "e_class");
            return (Criteria) this;
        }

        public Criteria andE_classNotBetween(Integer value1, Integer value2) {
            addCriterion("E_class not between", value1, value2, "e_class");
            return (Criteria) this;
        }

        public Criteria andE_beizhuIsNull() {
            addCriterion("E_beizhu is null");
            return (Criteria) this;
        }

        public Criteria andE_beizhuIsNotNull() {
            addCriterion("E_beizhu is not null");
            return (Criteria) this;
        }

        public Criteria andE_beizhuEqualTo(String value) {
            addCriterion("E_beizhu =", value, "e_beizhu");
            return (Criteria) this;
        }

        public Criteria andE_beizhuNotEqualTo(String value) {
            addCriterion("E_beizhu <>", value, "e_beizhu");
            return (Criteria) this;
        }

        public Criteria andE_beizhuGreaterThan(String value) {
            addCriterion("E_beizhu >", value, "e_beizhu");
            return (Criteria) this;
        }

        public Criteria andE_beizhuGreaterThanOrEqualTo(String value) {
            addCriterion("E_beizhu >=", value, "e_beizhu");
            return (Criteria) this;
        }

        public Criteria andE_beizhuLessThan(String value) {
            addCriterion("E_beizhu <", value, "e_beizhu");
            return (Criteria) this;
        }

        public Criteria andE_beizhuLessThanOrEqualTo(String value) {
            addCriterion("E_beizhu <=", value, "e_beizhu");
            return (Criteria) this;
        }

        public Criteria andE_beizhuLike(String value) {
            addCriterion("E_beizhu like", value, "e_beizhu");
            return (Criteria) this;
        }

        public Criteria andE_beizhuNotLike(String value) {
            addCriterion("E_beizhu not like", value, "e_beizhu");
            return (Criteria) this;
        }

        public Criteria andE_beizhuIn(List<String> values) {
            addCriterion("E_beizhu in", values, "e_beizhu");
            return (Criteria) this;
        }

        public Criteria andE_beizhuNotIn(List<String> values) {
            addCriterion("E_beizhu not in", values, "e_beizhu");
            return (Criteria) this;
        }

        public Criteria andE_beizhuBetween(String value1, String value2) {
            addCriterion("E_beizhu between", value1, value2, "e_beizhu");
            return (Criteria) this;
        }

        public Criteria andE_beizhuNotBetween(String value1, String value2) {
            addCriterion("E_beizhu not between", value1, value2, "e_beizhu");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}