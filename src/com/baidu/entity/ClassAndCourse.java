package com.baidu.entity;

public class ClassAndCourse {

	private int c1_id;
	private int c2_id;
	private String c1_name;
	private String c2_name;
	public int getC1_id() {
		return c1_id;
	}
	public void setC1_id(int c1_id) {
		this.c1_id = c1_id;
	}
	public int getC2_id() {
		return c2_id;
	}
	public void setC2_id(int c2_id) {
		this.c2_id = c2_id;
	}
	public String getC1_name() {
		return c1_name;
	}
	public void setC1_name(String c1_name) {
		this.c1_name = c1_name;
	}
	public String getC2_name() {
		return c2_name;
	}
	public void setC2_name(String c2_name) {
		this.c2_name = c2_name;
	}
	
	
}
