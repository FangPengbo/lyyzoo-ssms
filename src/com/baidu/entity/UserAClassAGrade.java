package com.baidu.entity;
/**
 * User And Class And Grade
 * @author FangPengbo
 *
 */
public class UserAClassAGrade {

	private Integer u_Id;

    private Integer u_Sno;

    private String u_Name;

    private String u_Sex;

    private String u_Phone;

    private String u_Qq;

    private Integer u_Class;

    private Integer u_Grade;
	
    private Integer c_Id;

    private String c_Name;

    private Integer g_Id;

    private String g_Name;

    private String g_Course;

    private String u_photo;
    
    
	public String getU_photo() {
		return u_photo;
	}

	public void setU_photo(String u_photo) {
		this.u_photo = u_photo;
	}


	public Integer getU_Id() {
		return u_Id;
	}

	public void setU_Id(Integer u_Id) {
		this.u_Id = u_Id;
	}

	public Integer getU_Sno() {
		return u_Sno;
	}

	public void setU_Sno(Integer u_Sno) {
		this.u_Sno = u_Sno;
	}

	public String getU_Name() {
		return u_Name;
	}

	public void setU_Name(String u_Name) {
		this.u_Name = u_Name;
	}

	public String getU_Sex() {
		return u_Sex;
	}

	public void setU_Sex(String u_Sex) {
		this.u_Sex = u_Sex;
	}

	public String getU_Phone() {
		return u_Phone;
	}

	public void setU_Phone(String u_Phone) {
		this.u_Phone = u_Phone;
	}

	public String getU_Qq() {
		return u_Qq;
	}

	public void setU_Qq(String u_Qq) {
		this.u_Qq = u_Qq;
	}

	public Integer getU_Class() {
		return u_Class;
	}

	public void setU_Class(Integer u_Class) {
		this.u_Class = u_Class;
	}

	public Integer getU_Grade() {
		return u_Grade;
	}

	public void setU_Grade(Integer u_Grade) {
		this.u_Grade = u_Grade;
	}

	public Integer getC_Id() {
		return c_Id;
	}

	public void setC_Id(Integer c_Id) {
		this.c_Id = c_Id;
	}

	public String getC_Name() {
		return c_Name;
	}

	public void setC_Name(String c_Name) {
		this.c_Name = c_Name;
	}

	public Integer getG_Id() {
		return g_Id;
	}

	public void setG_Id(Integer g_Id) {
		this.g_Id = g_Id;
	}

	public String getG_Name() {
		return g_Name;
	}

	public void setG_Name(String g_Name) {
		this.g_Name = g_Name;
	}

	public String getG_Course() {
		return g_Course;
	}

	public void setG_Course(String g_Course) {
		this.g_Course = g_Course;
	}
    
	
	
}
