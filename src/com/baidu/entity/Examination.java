package com.baidu.entity;

import java.util.Date;

public class Examination {
    private Integer e_id;

    private String e_name;

    private Date e_time;

    private String e_type;

    private Integer e_grade;

    private Integer e_class;

    private String e_beizhu;

    public Integer getE_id() {
        return e_id;
    }

    public void setE_id(Integer e_id) {
        this.e_id = e_id;
    }

    public String getE_name() {
        return e_name;
    }

    public void setE_name(String e_name) {
        this.e_name = e_name == null ? null : e_name.trim();
    }

    public Date getE_time() {
        return e_time;
    }

    public void setE_time(Date e_time) {
        this.e_time = e_time;
    }

    public String getE_type() {
        return e_type;
    }

    public void setE_type(String e_type) {
        this.e_type = e_type == null ? null : e_type.trim();
    }

    public Integer getE_grade() {
        return e_grade;
    }

    public void setE_grade(Integer e_grade) {
        this.e_grade = e_grade;
    }

    public Integer getE_class() {
        return e_class;
    }

    public void setE_class(Integer e_class) {
        this.e_class = e_class;
    }

    public String getE_beizhu() {
        return e_beizhu;
    }

    public void setE_beizhu(String e_beizhu) {
        this.e_beizhu = e_beizhu == null ? null : e_beizhu.trim();
    }
}