package com.baidu.entity;

import java.util.ArrayList;
import java.util.List;

public class GradeExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public GradeExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andG_idIsNull() {
            addCriterion("G_id is null");
            return (Criteria) this;
        }

        public Criteria andG_idIsNotNull() {
            addCriterion("G_id is not null");
            return (Criteria) this;
        }

        public Criteria andG_idEqualTo(Integer value) {
            addCriterion("G_id =", value, "g_id");
            return (Criteria) this;
        }

        public Criteria andG_idNotEqualTo(Integer value) {
            addCriterion("G_id <>", value, "g_id");
            return (Criteria) this;
        }

        public Criteria andG_idGreaterThan(Integer value) {
            addCriterion("G_id >", value, "g_id");
            return (Criteria) this;
        }

        public Criteria andG_idGreaterThanOrEqualTo(Integer value) {
            addCriterion("G_id >=", value, "g_id");
            return (Criteria) this;
        }

        public Criteria andG_idLessThan(Integer value) {
            addCriterion("G_id <", value, "g_id");
            return (Criteria) this;
        }

        public Criteria andG_idLessThanOrEqualTo(Integer value) {
            addCriterion("G_id <=", value, "g_id");
            return (Criteria) this;
        }

        public Criteria andG_idIn(List<Integer> values) {
            addCriterion("G_id in", values, "g_id");
            return (Criteria) this;
        }

        public Criteria andG_idNotIn(List<Integer> values) {
            addCriterion("G_id not in", values, "g_id");
            return (Criteria) this;
        }

        public Criteria andG_idBetween(Integer value1, Integer value2) {
            addCriterion("G_id between", value1, value2, "g_id");
            return (Criteria) this;
        }

        public Criteria andG_idNotBetween(Integer value1, Integer value2) {
            addCriterion("G_id not between", value1, value2, "g_id");
            return (Criteria) this;
        }

        public Criteria andG_nameIsNull() {
            addCriterion("G_name is null");
            return (Criteria) this;
        }

        public Criteria andG_nameIsNotNull() {
            addCriterion("G_name is not null");
            return (Criteria) this;
        }

        public Criteria andG_nameEqualTo(String value) {
            addCriterion("G_name =", value, "g_name");
            return (Criteria) this;
        }

        public Criteria andG_nameNotEqualTo(String value) {
            addCriterion("G_name <>", value, "g_name");
            return (Criteria) this;
        }

        public Criteria andG_nameGreaterThan(String value) {
            addCriterion("G_name >", value, "g_name");
            return (Criteria) this;
        }

        public Criteria andG_nameGreaterThanOrEqualTo(String value) {
            addCriterion("G_name >=", value, "g_name");
            return (Criteria) this;
        }

        public Criteria andG_nameLessThan(String value) {
            addCriterion("G_name <", value, "g_name");
            return (Criteria) this;
        }

        public Criteria andG_nameLessThanOrEqualTo(String value) {
            addCriterion("G_name <=", value, "g_name");
            return (Criteria) this;
        }

        public Criteria andG_nameLike(String value) {
            addCriterion("G_name like", value, "g_name");
            return (Criteria) this;
        }

        public Criteria andG_nameNotLike(String value) {
            addCriterion("G_name not like", value, "g_name");
            return (Criteria) this;
        }

        public Criteria andG_nameIn(List<String> values) {
            addCriterion("G_name in", values, "g_name");
            return (Criteria) this;
        }

        public Criteria andG_nameNotIn(List<String> values) {
            addCriterion("G_name not in", values, "g_name");
            return (Criteria) this;
        }

        public Criteria andG_nameBetween(String value1, String value2) {
            addCriterion("G_name between", value1, value2, "g_name");
            return (Criteria) this;
        }

        public Criteria andG_nameNotBetween(String value1, String value2) {
            addCriterion("G_name not between", value1, value2, "g_name");
            return (Criteria) this;
        }

        public Criteria andG_courseIsNull() {
            addCriterion("G_course is null");
            return (Criteria) this;
        }

        public Criteria andG_courseIsNotNull() {
            addCriterion("G_course is not null");
            return (Criteria) this;
        }

        public Criteria andG_courseEqualTo(String value) {
            addCriterion("G_course =", value, "g_course");
            return (Criteria) this;
        }

        public Criteria andG_courseNotEqualTo(String value) {
            addCriterion("G_course <>", value, "g_course");
            return (Criteria) this;
        }

        public Criteria andG_courseGreaterThan(String value) {
            addCriterion("G_course >", value, "g_course");
            return (Criteria) this;
        }

        public Criteria andG_courseGreaterThanOrEqualTo(String value) {
            addCriterion("G_course >=", value, "g_course");
            return (Criteria) this;
        }

        public Criteria andG_courseLessThan(String value) {
            addCriterion("G_course <", value, "g_course");
            return (Criteria) this;
        }

        public Criteria andG_courseLessThanOrEqualTo(String value) {
            addCriterion("G_course <=", value, "g_course");
            return (Criteria) this;
        }

        public Criteria andG_courseLike(String value) {
            addCriterion("G_course like", value, "g_course");
            return (Criteria) this;
        }

        public Criteria andG_courseNotLike(String value) {
            addCriterion("G_course not like", value, "g_course");
            return (Criteria) this;
        }

        public Criteria andG_courseIn(List<String> values) {
            addCriterion("G_course in", values, "g_course");
            return (Criteria) this;
        }

        public Criteria andG_courseNotIn(List<String> values) {
            addCriterion("G_course not in", values, "g_course");
            return (Criteria) this;
        }

        public Criteria andG_courseBetween(String value1, String value2) {
            addCriterion("G_course between", value1, value2, "g_course");
            return (Criteria) this;
        }

        public Criteria andG_courseNotBetween(String value1, String value2) {
            addCriterion("G_course not between", value1, value2, "g_course");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}