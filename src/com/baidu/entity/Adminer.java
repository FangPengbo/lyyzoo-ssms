package com.baidu.entity;

public class Adminer {
    private Integer a_id;

    private String a_uname;

    private String a_pwd;

    public Integer getA_id() {
        return a_id;
    }

    public void setA_id(Integer a_id) {
        this.a_id = a_id;
    }

    public String getA_uname() {
        return a_uname;
    }

    public void setA_uname(String a_uname) {
        this.a_uname = a_uname == null ? null : a_uname.trim();
    }

    public String getA_pwd() {
        return a_pwd;
    }

    public void setA_pwd(String a_pwd) {
        this.a_pwd = a_pwd == null ? null : a_pwd.trim();
    }
}