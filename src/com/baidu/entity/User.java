package com.baidu.entity;

public class User {
    private Integer u_id;

    private Integer u_sno;

    private String u_name;

    private String u_sex;

    private String u_phone;

    private String u_qq;

    private Integer u_class;

    private Integer u_grade;

    private String u_photo;

    public Integer getU_id() {
        return u_id;
    }

    public void setU_id(Integer u_id) {
        this.u_id = u_id;
    }

    public Integer getU_sno() {
        return u_sno;
    }

    public void setU_sno(Integer u_sno) {
        this.u_sno = u_sno;
    }

    public String getU_name() {
        return u_name;
    }

    public void setU_name(String u_name) {
        this.u_name = u_name == null ? null : u_name.trim();
    }

    public String getU_sex() {
        return u_sex;
    }

    public void setU_sex(String u_sex) {
        this.u_sex = u_sex == null ? null : u_sex.trim();
    }

    public String getU_phone() {
        return u_phone;
    }

    public void setU_phone(String u_phone) {
        this.u_phone = u_phone == null ? null : u_phone.trim();
    }

    public String getU_qq() {
        return u_qq;
    }

    public void setU_qq(String u_qq) {
        this.u_qq = u_qq == null ? null : u_qq.trim();
    }

    public Integer getU_class() {
        return u_class;
    }

    public void setU_class(Integer u_class) {
        this.u_class = u_class;
    }

    public Integer getU_grade() {
        return u_grade;
    }

    public void setU_grade(Integer u_grade) {
        this.u_grade = u_grade;
    }

    public String getU_photo() {
        return u_photo;
    }

    public void setU_photo(String u_photo) {
        this.u_photo = u_photo == null ? null : u_photo.trim();
    }
}