package com.baidu.entity;

public class Userlist {

	private Integer u_id;

	private Integer u_sno;

	private String u_name;

	private String u_sex;

	private String u_phone;

	private String u_qq;

	private Integer u_class;

	private Integer u_grade;

	private String u_photo;

	private Integer g_id;

	private String g_name;

	private String g_course;

	private Integer c_id;

	private String c_name;

	public Integer getU_id() {
		return u_id;
	}

	public void setU_id(Integer u_id) {
		this.u_id = u_id;
	}

	public Integer getU_sno() {
		return u_sno;
	}

	public void setU_sno(Integer u_sno) {
		this.u_sno = u_sno;
	}

	public String getU_name() {
		return u_name;
	}

	public void setU_name(String u_name) {
		this.u_name = u_name;
	}

	public String getU_sex() {
		return u_sex;
	}

	public void setU_sex(String u_sex) {
		this.u_sex = u_sex;
	}

	public String getU_phone() {
		return u_phone;
	}

	public void setU_phone(String u_phone) {
		this.u_phone = u_phone;
	}

	public String getU_qq() {
		return u_qq;
	}

	public void setU_qq(String u_qq) {
		this.u_qq = u_qq;
	}

	public Integer getU_class() {
		return u_class;
	}

	public void setU_class(Integer u_class) {
		this.u_class = u_class;
	}

	public Integer getU_grade() {
		return u_grade;
	}

	public void setU_grade(Integer u_grade) {
		this.u_grade = u_grade;
	}

	public String getU_photo() {
		return u_photo;
	}

	public void setU_photo(String u_photo) {
		this.u_photo = u_photo;
	}

	public Integer getG_id() {
		return g_id;
	}

	public void setG_id(Integer g_id) {
		this.g_id = g_id;
	}

	public String getG_name() {
		return g_name;
	}

	public void setG_name(String g_name) {
		this.g_name = g_name;
	}

	public String getG_course() {
		return g_course;
	}

	public void setG_course(String g_course) {
		this.g_course = g_course;
	}

	public Integer getC_id() {
		return c_id;
	}

	public void setC_id(Integer c_id) {
		this.c_id = c_id;
	}

	public String getC_name() {
		return c_name;
	}

	public void setC_name(String c_name) {
		this.c_name = c_name;
	}

}
