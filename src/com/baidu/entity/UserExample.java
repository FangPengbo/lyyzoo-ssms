package com.baidu.entity;

import java.util.ArrayList;
import java.util.List;

public class UserExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public UserExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andU_idIsNull() {
            addCriterion("U_id is null");
            return (Criteria) this;
        }

        public Criteria andU_idIsNotNull() {
            addCriterion("U_id is not null");
            return (Criteria) this;
        }

        public Criteria andU_idEqualTo(Integer value) {
            addCriterion("U_id =", value, "u_id");
            return (Criteria) this;
        }

        public Criteria andU_idNotEqualTo(Integer value) {
            addCriterion("U_id <>", value, "u_id");
            return (Criteria) this;
        }

        public Criteria andU_idGreaterThan(Integer value) {
            addCriterion("U_id >", value, "u_id");
            return (Criteria) this;
        }

        public Criteria andU_idGreaterThanOrEqualTo(Integer value) {
            addCriterion("U_id >=", value, "u_id");
            return (Criteria) this;
        }

        public Criteria andU_idLessThan(Integer value) {
            addCriterion("U_id <", value, "u_id");
            return (Criteria) this;
        }

        public Criteria andU_idLessThanOrEqualTo(Integer value) {
            addCriterion("U_id <=", value, "u_id");
            return (Criteria) this;
        }

        public Criteria andU_idIn(List<Integer> values) {
            addCriterion("U_id in", values, "u_id");
            return (Criteria) this;
        }

        public Criteria andU_idNotIn(List<Integer> values) {
            addCriterion("U_id not in", values, "u_id");
            return (Criteria) this;
        }

        public Criteria andU_idBetween(Integer value1, Integer value2) {
            addCriterion("U_id between", value1, value2, "u_id");
            return (Criteria) this;
        }

        public Criteria andU_idNotBetween(Integer value1, Integer value2) {
            addCriterion("U_id not between", value1, value2, "u_id");
            return (Criteria) this;
        }

        public Criteria andU_snoIsNull() {
            addCriterion("U_sno is null");
            return (Criteria) this;
        }

        public Criteria andU_snoIsNotNull() {
            addCriterion("U_sno is not null");
            return (Criteria) this;
        }

        public Criteria andU_snoEqualTo(Integer value) {
            addCriterion("U_sno =", value, "u_sno");
            return (Criteria) this;
        }

        public Criteria andU_snoNotEqualTo(Integer value) {
            addCriterion("U_sno <>", value, "u_sno");
            return (Criteria) this;
        }

        public Criteria andU_snoGreaterThan(Integer value) {
            addCriterion("U_sno >", value, "u_sno");
            return (Criteria) this;
        }

        public Criteria andU_snoGreaterThanOrEqualTo(Integer value) {
            addCriterion("U_sno >=", value, "u_sno");
            return (Criteria) this;
        }

        public Criteria andU_snoLessThan(Integer value) {
            addCriterion("U_sno <", value, "u_sno");
            return (Criteria) this;
        }

        public Criteria andU_snoLessThanOrEqualTo(Integer value) {
            addCriterion("U_sno <=", value, "u_sno");
            return (Criteria) this;
        }

        public Criteria andU_snoIn(List<Integer> values) {
            addCriterion("U_sno in", values, "u_sno");
            return (Criteria) this;
        }

        public Criteria andU_snoNotIn(List<Integer> values) {
            addCriterion("U_sno not in", values, "u_sno");
            return (Criteria) this;
        }

        public Criteria andU_snoBetween(Integer value1, Integer value2) {
            addCriterion("U_sno between", value1, value2, "u_sno");
            return (Criteria) this;
        }

        public Criteria andU_snoNotBetween(Integer value1, Integer value2) {
            addCriterion("U_sno not between", value1, value2, "u_sno");
            return (Criteria) this;
        }

        public Criteria andU_nameIsNull() {
            addCriterion("U_name is null");
            return (Criteria) this;
        }

        public Criteria andU_nameIsNotNull() {
            addCriterion("U_name is not null");
            return (Criteria) this;
        }

        public Criteria andU_nameEqualTo(String value) {
            addCriterion("U_name =", value, "u_name");
            return (Criteria) this;
        }

        public Criteria andU_nameNotEqualTo(String value) {
            addCriterion("U_name <>", value, "u_name");
            return (Criteria) this;
        }

        public Criteria andU_nameGreaterThan(String value) {
            addCriterion("U_name >", value, "u_name");
            return (Criteria) this;
        }

        public Criteria andU_nameGreaterThanOrEqualTo(String value) {
            addCriterion("U_name >=", value, "u_name");
            return (Criteria) this;
        }

        public Criteria andU_nameLessThan(String value) {
            addCriterion("U_name <", value, "u_name");
            return (Criteria) this;
        }

        public Criteria andU_nameLessThanOrEqualTo(String value) {
            addCriterion("U_name <=", value, "u_name");
            return (Criteria) this;
        }

        public Criteria andU_nameLike(String value) {
            addCriterion("U_name like", value, "u_name");
            return (Criteria) this;
        }

        public Criteria andU_nameNotLike(String value) {
            addCriterion("U_name not like", value, "u_name");
            return (Criteria) this;
        }

        public Criteria andU_nameIn(List<String> values) {
            addCriterion("U_name in", values, "u_name");
            return (Criteria) this;
        }

        public Criteria andU_nameNotIn(List<String> values) {
            addCriterion("U_name not in", values, "u_name");
            return (Criteria) this;
        }

        public Criteria andU_nameBetween(String value1, String value2) {
            addCriterion("U_name between", value1, value2, "u_name");
            return (Criteria) this;
        }

        public Criteria andU_nameNotBetween(String value1, String value2) {
            addCriterion("U_name not between", value1, value2, "u_name");
            return (Criteria) this;
        }

        public Criteria andU_sexIsNull() {
            addCriterion("U_sex is null");
            return (Criteria) this;
        }

        public Criteria andU_sexIsNotNull() {
            addCriterion("U_sex is not null");
            return (Criteria) this;
        }

        public Criteria andU_sexEqualTo(String value) {
            addCriterion("U_sex =", value, "u_sex");
            return (Criteria) this;
        }

        public Criteria andU_sexNotEqualTo(String value) {
            addCriterion("U_sex <>", value, "u_sex");
            return (Criteria) this;
        }

        public Criteria andU_sexGreaterThan(String value) {
            addCriterion("U_sex >", value, "u_sex");
            return (Criteria) this;
        }

        public Criteria andU_sexGreaterThanOrEqualTo(String value) {
            addCriterion("U_sex >=", value, "u_sex");
            return (Criteria) this;
        }

        public Criteria andU_sexLessThan(String value) {
            addCriterion("U_sex <", value, "u_sex");
            return (Criteria) this;
        }

        public Criteria andU_sexLessThanOrEqualTo(String value) {
            addCriterion("U_sex <=", value, "u_sex");
            return (Criteria) this;
        }

        public Criteria andU_sexLike(String value) {
            addCriterion("U_sex like", value, "u_sex");
            return (Criteria) this;
        }

        public Criteria andU_sexNotLike(String value) {
            addCriterion("U_sex not like", value, "u_sex");
            return (Criteria) this;
        }

        public Criteria andU_sexIn(List<String> values) {
            addCriterion("U_sex in", values, "u_sex");
            return (Criteria) this;
        }

        public Criteria andU_sexNotIn(List<String> values) {
            addCriterion("U_sex not in", values, "u_sex");
            return (Criteria) this;
        }

        public Criteria andU_sexBetween(String value1, String value2) {
            addCriterion("U_sex between", value1, value2, "u_sex");
            return (Criteria) this;
        }

        public Criteria andU_sexNotBetween(String value1, String value2) {
            addCriterion("U_sex not between", value1, value2, "u_sex");
            return (Criteria) this;
        }

        public Criteria andU_phoneIsNull() {
            addCriterion("U_phone is null");
            return (Criteria) this;
        }

        public Criteria andU_phoneIsNotNull() {
            addCriterion("U_phone is not null");
            return (Criteria) this;
        }

        public Criteria andU_phoneEqualTo(String value) {
            addCriterion("U_phone =", value, "u_phone");
            return (Criteria) this;
        }

        public Criteria andU_phoneNotEqualTo(String value) {
            addCriterion("U_phone <>", value, "u_phone");
            return (Criteria) this;
        }

        public Criteria andU_phoneGreaterThan(String value) {
            addCriterion("U_phone >", value, "u_phone");
            return (Criteria) this;
        }

        public Criteria andU_phoneGreaterThanOrEqualTo(String value) {
            addCriterion("U_phone >=", value, "u_phone");
            return (Criteria) this;
        }

        public Criteria andU_phoneLessThan(String value) {
            addCriterion("U_phone <", value, "u_phone");
            return (Criteria) this;
        }

        public Criteria andU_phoneLessThanOrEqualTo(String value) {
            addCriterion("U_phone <=", value, "u_phone");
            return (Criteria) this;
        }

        public Criteria andU_phoneLike(String value) {
            addCriterion("U_phone like", value, "u_phone");
            return (Criteria) this;
        }

        public Criteria andU_phoneNotLike(String value) {
            addCriterion("U_phone not like", value, "u_phone");
            return (Criteria) this;
        }

        public Criteria andU_phoneIn(List<String> values) {
            addCriterion("U_phone in", values, "u_phone");
            return (Criteria) this;
        }

        public Criteria andU_phoneNotIn(List<String> values) {
            addCriterion("U_phone not in", values, "u_phone");
            return (Criteria) this;
        }

        public Criteria andU_phoneBetween(String value1, String value2) {
            addCriterion("U_phone between", value1, value2, "u_phone");
            return (Criteria) this;
        }

        public Criteria andU_phoneNotBetween(String value1, String value2) {
            addCriterion("U_phone not between", value1, value2, "u_phone");
            return (Criteria) this;
        }

        public Criteria andU_qqIsNull() {
            addCriterion("U_qq is null");
            return (Criteria) this;
        }

        public Criteria andU_qqIsNotNull() {
            addCriterion("U_qq is not null");
            return (Criteria) this;
        }

        public Criteria andU_qqEqualTo(String value) {
            addCriterion("U_qq =", value, "u_qq");
            return (Criteria) this;
        }

        public Criteria andU_qqNotEqualTo(String value) {
            addCriterion("U_qq <>", value, "u_qq");
            return (Criteria) this;
        }

        public Criteria andU_qqGreaterThan(String value) {
            addCriterion("U_qq >", value, "u_qq");
            return (Criteria) this;
        }

        public Criteria andU_qqGreaterThanOrEqualTo(String value) {
            addCriterion("U_qq >=", value, "u_qq");
            return (Criteria) this;
        }

        public Criteria andU_qqLessThan(String value) {
            addCriterion("U_qq <", value, "u_qq");
            return (Criteria) this;
        }

        public Criteria andU_qqLessThanOrEqualTo(String value) {
            addCriterion("U_qq <=", value, "u_qq");
            return (Criteria) this;
        }

        public Criteria andU_qqLike(String value) {
            addCriterion("U_qq like", value, "u_qq");
            return (Criteria) this;
        }

        public Criteria andU_qqNotLike(String value) {
            addCriterion("U_qq not like", value, "u_qq");
            return (Criteria) this;
        }

        public Criteria andU_qqIn(List<String> values) {
            addCriterion("U_qq in", values, "u_qq");
            return (Criteria) this;
        }

        public Criteria andU_qqNotIn(List<String> values) {
            addCriterion("U_qq not in", values, "u_qq");
            return (Criteria) this;
        }

        public Criteria andU_qqBetween(String value1, String value2) {
            addCriterion("U_qq between", value1, value2, "u_qq");
            return (Criteria) this;
        }

        public Criteria andU_qqNotBetween(String value1, String value2) {
            addCriterion("U_qq not between", value1, value2, "u_qq");
            return (Criteria) this;
        }

        public Criteria andU_classIsNull() {
            addCriterion("U_class is null");
            return (Criteria) this;
        }

        public Criteria andU_classIsNotNull() {
            addCriterion("U_class is not null");
            return (Criteria) this;
        }

        public Criteria andU_classEqualTo(Integer value) {
            addCriterion("U_class =", value, "u_class");
            return (Criteria) this;
        }

        public Criteria andU_classNotEqualTo(Integer value) {
            addCriterion("U_class <>", value, "u_class");
            return (Criteria) this;
        }

        public Criteria andU_classGreaterThan(Integer value) {
            addCriterion("U_class >", value, "u_class");
            return (Criteria) this;
        }

        public Criteria andU_classGreaterThanOrEqualTo(Integer value) {
            addCriterion("U_class >=", value, "u_class");
            return (Criteria) this;
        }

        public Criteria andU_classLessThan(Integer value) {
            addCriterion("U_class <", value, "u_class");
            return (Criteria) this;
        }

        public Criteria andU_classLessThanOrEqualTo(Integer value) {
            addCriterion("U_class <=", value, "u_class");
            return (Criteria) this;
        }

        public Criteria andU_classIn(List<Integer> values) {
            addCriterion("U_class in", values, "u_class");
            return (Criteria) this;
        }

        public Criteria andU_classNotIn(List<Integer> values) {
            addCriterion("U_class not in", values, "u_class");
            return (Criteria) this;
        }

        public Criteria andU_classBetween(Integer value1, Integer value2) {
            addCriterion("U_class between", value1, value2, "u_class");
            return (Criteria) this;
        }

        public Criteria andU_classNotBetween(Integer value1, Integer value2) {
            addCriterion("U_class not between", value1, value2, "u_class");
            return (Criteria) this;
        }

        public Criteria andU_gradeIsNull() {
            addCriterion("U_grade is null");
            return (Criteria) this;
        }

        public Criteria andU_gradeIsNotNull() {
            addCriterion("U_grade is not null");
            return (Criteria) this;
        }

        public Criteria andU_gradeEqualTo(Integer value) {
            addCriterion("U_grade =", value, "u_grade");
            return (Criteria) this;
        }

        public Criteria andU_gradeNotEqualTo(Integer value) {
            addCriterion("U_grade <>", value, "u_grade");
            return (Criteria) this;
        }

        public Criteria andU_gradeGreaterThan(Integer value) {
            addCriterion("U_grade >", value, "u_grade");
            return (Criteria) this;
        }

        public Criteria andU_gradeGreaterThanOrEqualTo(Integer value) {
            addCriterion("U_grade >=", value, "u_grade");
            return (Criteria) this;
        }

        public Criteria andU_gradeLessThan(Integer value) {
            addCriterion("U_grade <", value, "u_grade");
            return (Criteria) this;
        }

        public Criteria andU_gradeLessThanOrEqualTo(Integer value) {
            addCriterion("U_grade <=", value, "u_grade");
            return (Criteria) this;
        }

        public Criteria andU_gradeIn(List<Integer> values) {
            addCriterion("U_grade in", values, "u_grade");
            return (Criteria) this;
        }

        public Criteria andU_gradeNotIn(List<Integer> values) {
            addCriterion("U_grade not in", values, "u_grade");
            return (Criteria) this;
        }

        public Criteria andU_gradeBetween(Integer value1, Integer value2) {
            addCriterion("U_grade between", value1, value2, "u_grade");
            return (Criteria) this;
        }

        public Criteria andU_gradeNotBetween(Integer value1, Integer value2) {
            addCriterion("U_grade not between", value1, value2, "u_grade");
            return (Criteria) this;
        }

        public Criteria andU_photoIsNull() {
            addCriterion("U_photo is null");
            return (Criteria) this;
        }

        public Criteria andU_photoIsNotNull() {
            addCriterion("U_photo is not null");
            return (Criteria) this;
        }

        public Criteria andU_photoEqualTo(String value) {
            addCriterion("U_photo =", value, "u_photo");
            return (Criteria) this;
        }

        public Criteria andU_photoNotEqualTo(String value) {
            addCriterion("U_photo <>", value, "u_photo");
            return (Criteria) this;
        }

        public Criteria andU_photoGreaterThan(String value) {
            addCriterion("U_photo >", value, "u_photo");
            return (Criteria) this;
        }

        public Criteria andU_photoGreaterThanOrEqualTo(String value) {
            addCriterion("U_photo >=", value, "u_photo");
            return (Criteria) this;
        }

        public Criteria andU_photoLessThan(String value) {
            addCriterion("U_photo <", value, "u_photo");
            return (Criteria) this;
        }

        public Criteria andU_photoLessThanOrEqualTo(String value) {
            addCriterion("U_photo <=", value, "u_photo");
            return (Criteria) this;
        }

        public Criteria andU_photoLike(String value) {
            addCriterion("U_photo like", value, "u_photo");
            return (Criteria) this;
        }

        public Criteria andU_photoNotLike(String value) {
            addCriterion("U_photo not like", value, "u_photo");
            return (Criteria) this;
        }

        public Criteria andU_photoIn(List<String> values) {
            addCriterion("U_photo in", values, "u_photo");
            return (Criteria) this;
        }

        public Criteria andU_photoNotIn(List<String> values) {
            addCriterion("U_photo not in", values, "u_photo");
            return (Criteria) this;
        }

        public Criteria andU_photoBetween(String value1, String value2) {
            addCriterion("U_photo between", value1, value2, "u_photo");
            return (Criteria) this;
        }

        public Criteria andU_photoNotBetween(String value1, String value2) {
            addCriterion("U_photo not between", value1, value2, "u_photo");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}