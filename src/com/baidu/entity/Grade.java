package com.baidu.entity;

public class Grade {
    private Integer g_id;

    private String g_name;

    private String g_course;

    public Integer getG_id() {
        return g_id;
    }

    public void setG_id(Integer g_id) {
        this.g_id = g_id;
    }

    public String getG_name() {
        return g_name;
    }

    public void setG_name(String g_name) {
        this.g_name = g_name == null ? null : g_name.trim();
    }

    public String getG_course() {
        return g_course;
    }

    public void setG_course(String g_course) {
        this.g_course = g_course == null ? null : g_course.trim();
    }
}