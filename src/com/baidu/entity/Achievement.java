package com.baidu.entity;

public class Achievement {
    private Integer a_id;

    private Integer a_sno;

    private String a_name;

    private Integer a_course;

    private Integer a_examination;

    private Integer a_score;

    private Integer a_grade;

    private Integer a_class;

    public Integer getA_id() {
        return a_id;
    }

    public void setA_id(Integer a_id) {
        this.a_id = a_id;
    }

    public Integer getA_sno() {
        return a_sno;
    }

    public void setA_sno(Integer a_sno) {
        this.a_sno = a_sno;
    }

    public String getA_name() {
        return a_name;
    }

    public void setA_name(String a_name) {
        this.a_name = a_name == null ? null : a_name.trim();
    }

    public Integer getA_course() {
        return a_course;
    }

    public void setA_course(Integer a_course) {
        this.a_course = a_course;
    }

    public Integer getA_examination() {
        return a_examination;
    }

    public void setA_examination(Integer a_examination) {
        this.a_examination = a_examination;
    }

    public Integer getA_score() {
        return a_score;
    }

    public void setA_score(Integer a_score) {
        this.a_score = a_score;
    }

    public Integer getA_grade() {
        return a_grade;
    }

    public void setA_grade(Integer a_grade) {
        this.a_grade = a_grade;
    }

    public Integer getA_class() {
        return a_class;
    }

    public void setA_class(Integer a_class) {
        this.a_class = a_class;
    }
}