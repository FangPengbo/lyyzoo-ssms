package com.baidu.entity;

import java.util.ArrayList;
import java.util.List;

public class TeacherExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public TeacherExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andT_idIsNull() {
            addCriterion("T_id is null");
            return (Criteria) this;
        }

        public Criteria andT_idIsNotNull() {
            addCriterion("T_id is not null");
            return (Criteria) this;
        }

        public Criteria andT_idEqualTo(Integer value) {
            addCriterion("T_id =", value, "t_id");
            return (Criteria) this;
        }

        public Criteria andT_idNotEqualTo(Integer value) {
            addCriterion("T_id <>", value, "t_id");
            return (Criteria) this;
        }

        public Criteria andT_idGreaterThan(Integer value) {
            addCriterion("T_id >", value, "t_id");
            return (Criteria) this;
        }

        public Criteria andT_idGreaterThanOrEqualTo(Integer value) {
            addCriterion("T_id >=", value, "t_id");
            return (Criteria) this;
        }

        public Criteria andT_idLessThan(Integer value) {
            addCriterion("T_id <", value, "t_id");
            return (Criteria) this;
        }

        public Criteria andT_idLessThanOrEqualTo(Integer value) {
            addCriterion("T_id <=", value, "t_id");
            return (Criteria) this;
        }

        public Criteria andT_idIn(List<Integer> values) {
            addCriterion("T_id in", values, "t_id");
            return (Criteria) this;
        }

        public Criteria andT_idNotIn(List<Integer> values) {
            addCriterion("T_id not in", values, "t_id");
            return (Criteria) this;
        }

        public Criteria andT_idBetween(Integer value1, Integer value2) {
            addCriterion("T_id between", value1, value2, "t_id");
            return (Criteria) this;
        }

        public Criteria andT_idNotBetween(Integer value1, Integer value2) {
            addCriterion("T_id not between", value1, value2, "t_id");
            return (Criteria) this;
        }

        public Criteria andT_gonghaoIsNull() {
            addCriterion("T_gonghao is null");
            return (Criteria) this;
        }

        public Criteria andT_gonghaoIsNotNull() {
            addCriterion("T_gonghao is not null");
            return (Criteria) this;
        }

        public Criteria andT_gonghaoEqualTo(String value) {
            addCriterion("T_gonghao =", value, "t_gonghao");
            return (Criteria) this;
        }

        public Criteria andT_gonghaoNotEqualTo(String value) {
            addCriterion("T_gonghao <>", value, "t_gonghao");
            return (Criteria) this;
        }

        public Criteria andT_gonghaoGreaterThan(String value) {
            addCriterion("T_gonghao >", value, "t_gonghao");
            return (Criteria) this;
        }

        public Criteria andT_gonghaoGreaterThanOrEqualTo(String value) {
            addCriterion("T_gonghao >=", value, "t_gonghao");
            return (Criteria) this;
        }

        public Criteria andT_gonghaoLessThan(String value) {
            addCriterion("T_gonghao <", value, "t_gonghao");
            return (Criteria) this;
        }

        public Criteria andT_gonghaoLessThanOrEqualTo(String value) {
            addCriterion("T_gonghao <=", value, "t_gonghao");
            return (Criteria) this;
        }

        public Criteria andT_gonghaoLike(String value) {
            addCriterion("T_gonghao like", value, "t_gonghao");
            return (Criteria) this;
        }

        public Criteria andT_gonghaoNotLike(String value) {
            addCriterion("T_gonghao not like", value, "t_gonghao");
            return (Criteria) this;
        }

        public Criteria andT_gonghaoIn(List<String> values) {
            addCriterion("T_gonghao in", values, "t_gonghao");
            return (Criteria) this;
        }

        public Criteria andT_gonghaoNotIn(List<String> values) {
            addCriterion("T_gonghao not in", values, "t_gonghao");
            return (Criteria) this;
        }

        public Criteria andT_gonghaoBetween(String value1, String value2) {
            addCriterion("T_gonghao between", value1, value2, "t_gonghao");
            return (Criteria) this;
        }

        public Criteria andT_gonghaoNotBetween(String value1, String value2) {
            addCriterion("T_gonghao not between", value1, value2, "t_gonghao");
            return (Criteria) this;
        }

        public Criteria andT_nameIsNull() {
            addCriterion("T_name is null");
            return (Criteria) this;
        }

        public Criteria andT_nameIsNotNull() {
            addCriterion("T_name is not null");
            return (Criteria) this;
        }

        public Criteria andT_nameEqualTo(String value) {
            addCriterion("T_name =", value, "t_name");
            return (Criteria) this;
        }

        public Criteria andT_nameNotEqualTo(String value) {
            addCriterion("T_name <>", value, "t_name");
            return (Criteria) this;
        }

        public Criteria andT_nameGreaterThan(String value) {
            addCriterion("T_name >", value, "t_name");
            return (Criteria) this;
        }

        public Criteria andT_nameGreaterThanOrEqualTo(String value) {
            addCriterion("T_name >=", value, "t_name");
            return (Criteria) this;
        }

        public Criteria andT_nameLessThan(String value) {
            addCriterion("T_name <", value, "t_name");
            return (Criteria) this;
        }

        public Criteria andT_nameLessThanOrEqualTo(String value) {
            addCriterion("T_name <=", value, "t_name");
            return (Criteria) this;
        }

        public Criteria andT_nameLike(String value) {
            addCriterion("T_name like", value, "t_name");
            return (Criteria) this;
        }

        public Criteria andT_nameNotLike(String value) {
            addCriterion("T_name not like", value, "t_name");
            return (Criteria) this;
        }

        public Criteria andT_nameIn(List<String> values) {
            addCriterion("T_name in", values, "t_name");
            return (Criteria) this;
        }

        public Criteria andT_nameNotIn(List<String> values) {
            addCriterion("T_name not in", values, "t_name");
            return (Criteria) this;
        }

        public Criteria andT_nameBetween(String value1, String value2) {
            addCriterion("T_name between", value1, value2, "t_name");
            return (Criteria) this;
        }

        public Criteria andT_nameNotBetween(String value1, String value2) {
            addCriterion("T_name not between", value1, value2, "t_name");
            return (Criteria) this;
        }

        public Criteria andT_sexIsNull() {
            addCriterion("T_sex is null");
            return (Criteria) this;
        }

        public Criteria andT_sexIsNotNull() {
            addCriterion("T_sex is not null");
            return (Criteria) this;
        }

        public Criteria andT_sexEqualTo(String value) {
            addCriterion("T_sex =", value, "t_sex");
            return (Criteria) this;
        }

        public Criteria andT_sexNotEqualTo(String value) {
            addCriterion("T_sex <>", value, "t_sex");
            return (Criteria) this;
        }

        public Criteria andT_sexGreaterThan(String value) {
            addCriterion("T_sex >", value, "t_sex");
            return (Criteria) this;
        }

        public Criteria andT_sexGreaterThanOrEqualTo(String value) {
            addCriterion("T_sex >=", value, "t_sex");
            return (Criteria) this;
        }

        public Criteria andT_sexLessThan(String value) {
            addCriterion("T_sex <", value, "t_sex");
            return (Criteria) this;
        }

        public Criteria andT_sexLessThanOrEqualTo(String value) {
            addCriterion("T_sex <=", value, "t_sex");
            return (Criteria) this;
        }

        public Criteria andT_sexLike(String value) {
            addCriterion("T_sex like", value, "t_sex");
            return (Criteria) this;
        }

        public Criteria andT_sexNotLike(String value) {
            addCriterion("T_sex not like", value, "t_sex");
            return (Criteria) this;
        }

        public Criteria andT_sexIn(List<String> values) {
            addCriterion("T_sex in", values, "t_sex");
            return (Criteria) this;
        }

        public Criteria andT_sexNotIn(List<String> values) {
            addCriterion("T_sex not in", values, "t_sex");
            return (Criteria) this;
        }

        public Criteria andT_sexBetween(String value1, String value2) {
            addCriterion("T_sex between", value1, value2, "t_sex");
            return (Criteria) this;
        }

        public Criteria andT_sexNotBetween(String value1, String value2) {
            addCriterion("T_sex not between", value1, value2, "t_sex");
            return (Criteria) this;
        }

        public Criteria andT_phoneIsNull() {
            addCriterion("T_phone is null");
            return (Criteria) this;
        }

        public Criteria andT_phoneIsNotNull() {
            addCriterion("T_phone is not null");
            return (Criteria) this;
        }

        public Criteria andT_phoneEqualTo(String value) {
            addCriterion("T_phone =", value, "t_phone");
            return (Criteria) this;
        }

        public Criteria andT_phoneNotEqualTo(String value) {
            addCriterion("T_phone <>", value, "t_phone");
            return (Criteria) this;
        }

        public Criteria andT_phoneGreaterThan(String value) {
            addCriterion("T_phone >", value, "t_phone");
            return (Criteria) this;
        }

        public Criteria andT_phoneGreaterThanOrEqualTo(String value) {
            addCriterion("T_phone >=", value, "t_phone");
            return (Criteria) this;
        }

        public Criteria andT_phoneLessThan(String value) {
            addCriterion("T_phone <", value, "t_phone");
            return (Criteria) this;
        }

        public Criteria andT_phoneLessThanOrEqualTo(String value) {
            addCriterion("T_phone <=", value, "t_phone");
            return (Criteria) this;
        }

        public Criteria andT_phoneLike(String value) {
            addCriterion("T_phone like", value, "t_phone");
            return (Criteria) this;
        }

        public Criteria andT_phoneNotLike(String value) {
            addCriterion("T_phone not like", value, "t_phone");
            return (Criteria) this;
        }

        public Criteria andT_phoneIn(List<String> values) {
            addCriterion("T_phone in", values, "t_phone");
            return (Criteria) this;
        }

        public Criteria andT_phoneNotIn(List<String> values) {
            addCriterion("T_phone not in", values, "t_phone");
            return (Criteria) this;
        }

        public Criteria andT_phoneBetween(String value1, String value2) {
            addCriterion("T_phone between", value1, value2, "t_phone");
            return (Criteria) this;
        }

        public Criteria andT_phoneNotBetween(String value1, String value2) {
            addCriterion("T_phone not between", value1, value2, "t_phone");
            return (Criteria) this;
        }

        public Criteria andT_qqIsNull() {
            addCriterion("T_qq is null");
            return (Criteria) this;
        }

        public Criteria andT_qqIsNotNull() {
            addCriterion("T_qq is not null");
            return (Criteria) this;
        }

        public Criteria andT_qqEqualTo(String value) {
            addCriterion("T_qq =", value, "t_qq");
            return (Criteria) this;
        }

        public Criteria andT_qqNotEqualTo(String value) {
            addCriterion("T_qq <>", value, "t_qq");
            return (Criteria) this;
        }

        public Criteria andT_qqGreaterThan(String value) {
            addCriterion("T_qq >", value, "t_qq");
            return (Criteria) this;
        }

        public Criteria andT_qqGreaterThanOrEqualTo(String value) {
            addCriterion("T_qq >=", value, "t_qq");
            return (Criteria) this;
        }

        public Criteria andT_qqLessThan(String value) {
            addCriterion("T_qq <", value, "t_qq");
            return (Criteria) this;
        }

        public Criteria andT_qqLessThanOrEqualTo(String value) {
            addCriterion("T_qq <=", value, "t_qq");
            return (Criteria) this;
        }

        public Criteria andT_qqLike(String value) {
            addCriterion("T_qq like", value, "t_qq");
            return (Criteria) this;
        }

        public Criteria andT_qqNotLike(String value) {
            addCriterion("T_qq not like", value, "t_qq");
            return (Criteria) this;
        }

        public Criteria andT_qqIn(List<String> values) {
            addCriterion("T_qq in", values, "t_qq");
            return (Criteria) this;
        }

        public Criteria andT_qqNotIn(List<String> values) {
            addCriterion("T_qq not in", values, "t_qq");
            return (Criteria) this;
        }

        public Criteria andT_qqBetween(String value1, String value2) {
            addCriterion("T_qq between", value1, value2, "t_qq");
            return (Criteria) this;
        }

        public Criteria andT_qqNotBetween(String value1, String value2) {
            addCriterion("T_qq not between", value1, value2, "t_qq");
            return (Criteria) this;
        }

        public Criteria andT_classIsNull() {
            addCriterion("T_class is null");
            return (Criteria) this;
        }

        public Criteria andT_classIsNotNull() {
            addCriterion("T_class is not null");
            return (Criteria) this;
        }

        public Criteria andT_classEqualTo(Integer value) {
            addCriterion("T_class =", value, "t_class");
            return (Criteria) this;
        }

        public Criteria andT_classNotEqualTo(Integer value) {
            addCriterion("T_class <>", value, "t_class");
            return (Criteria) this;
        }

        public Criteria andT_classGreaterThan(Integer value) {
            addCriterion("T_class >", value, "t_class");
            return (Criteria) this;
        }

        public Criteria andT_classGreaterThanOrEqualTo(Integer value) {
            addCriterion("T_class >=", value, "t_class");
            return (Criteria) this;
        }

        public Criteria andT_classLessThan(Integer value) {
            addCriterion("T_class <", value, "t_class");
            return (Criteria) this;
        }

        public Criteria andT_classLessThanOrEqualTo(Integer value) {
            addCriterion("T_class <=", value, "t_class");
            return (Criteria) this;
        }

        public Criteria andT_classIn(List<Integer> values) {
            addCriterion("T_class in", values, "t_class");
            return (Criteria) this;
        }

        public Criteria andT_classNotIn(List<Integer> values) {
            addCriterion("T_class not in", values, "t_class");
            return (Criteria) this;
        }

        public Criteria andT_classBetween(Integer value1, Integer value2) {
            addCriterion("T_class between", value1, value2, "t_class");
            return (Criteria) this;
        }

        public Criteria andT_classNotBetween(Integer value1, Integer value2) {
            addCriterion("T_class not between", value1, value2, "t_class");
            return (Criteria) this;
        }

        public Criteria andT_gradeIsNull() {
            addCriterion("T_grade is null");
            return (Criteria) this;
        }

        public Criteria andT_gradeIsNotNull() {
            addCriterion("T_grade is not null");
            return (Criteria) this;
        }

        public Criteria andT_gradeEqualTo(Integer value) {
            addCriterion("T_grade =", value, "t_grade");
            return (Criteria) this;
        }

        public Criteria andT_gradeNotEqualTo(Integer value) {
            addCriterion("T_grade <>", value, "t_grade");
            return (Criteria) this;
        }

        public Criteria andT_gradeGreaterThan(Integer value) {
            addCriterion("T_grade >", value, "t_grade");
            return (Criteria) this;
        }

        public Criteria andT_gradeGreaterThanOrEqualTo(Integer value) {
            addCriterion("T_grade >=", value, "t_grade");
            return (Criteria) this;
        }

        public Criteria andT_gradeLessThan(Integer value) {
            addCriterion("T_grade <", value, "t_grade");
            return (Criteria) this;
        }

        public Criteria andT_gradeLessThanOrEqualTo(Integer value) {
            addCriterion("T_grade <=", value, "t_grade");
            return (Criteria) this;
        }

        public Criteria andT_gradeIn(List<Integer> values) {
            addCriterion("T_grade in", values, "t_grade");
            return (Criteria) this;
        }

        public Criteria andT_gradeNotIn(List<Integer> values) {
            addCriterion("T_grade not in", values, "t_grade");
            return (Criteria) this;
        }

        public Criteria andT_gradeBetween(Integer value1, Integer value2) {
            addCriterion("T_grade between", value1, value2, "t_grade");
            return (Criteria) this;
        }

        public Criteria andT_gradeNotBetween(Integer value1, Integer value2) {
            addCriterion("T_grade not between", value1, value2, "t_grade");
            return (Criteria) this;
        }

        public Criteria andT_courseIsNull() {
            addCriterion("T_course is null");
            return (Criteria) this;
        }

        public Criteria andT_courseIsNotNull() {
            addCriterion("T_course is not null");
            return (Criteria) this;
        }

        public Criteria andT_courseEqualTo(Integer value) {
            addCriterion("T_course =", value, "t_course");
            return (Criteria) this;
        }

        public Criteria andT_courseNotEqualTo(Integer value) {
            addCriterion("T_course <>", value, "t_course");
            return (Criteria) this;
        }

        public Criteria andT_courseGreaterThan(Integer value) {
            addCriterion("T_course >", value, "t_course");
            return (Criteria) this;
        }

        public Criteria andT_courseGreaterThanOrEqualTo(Integer value) {
            addCriterion("T_course >=", value, "t_course");
            return (Criteria) this;
        }

        public Criteria andT_courseLessThan(Integer value) {
            addCriterion("T_course <", value, "t_course");
            return (Criteria) this;
        }

        public Criteria andT_courseLessThanOrEqualTo(Integer value) {
            addCriterion("T_course <=", value, "t_course");
            return (Criteria) this;
        }

        public Criteria andT_courseIn(List<Integer> values) {
            addCriterion("T_course in", values, "t_course");
            return (Criteria) this;
        }

        public Criteria andT_courseNotIn(List<Integer> values) {
            addCriterion("T_course not in", values, "t_course");
            return (Criteria) this;
        }

        public Criteria andT_courseBetween(Integer value1, Integer value2) {
            addCriterion("T_course between", value1, value2, "t_course");
            return (Criteria) this;
        }

        public Criteria andT_courseNotBetween(Integer value1, Integer value2) {
            addCriterion("T_course not between", value1, value2, "t_course");
            return (Criteria) this;
        }

        public Criteria andT_photoIsNull() {
            addCriterion("T_photo is null");
            return (Criteria) this;
        }

        public Criteria andT_photoIsNotNull() {
            addCriterion("T_photo is not null");
            return (Criteria) this;
        }

        public Criteria andT_photoEqualTo(String value) {
            addCriterion("T_photo =", value, "t_photo");
            return (Criteria) this;
        }

        public Criteria andT_photoNotEqualTo(String value) {
            addCriterion("T_photo <>", value, "t_photo");
            return (Criteria) this;
        }

        public Criteria andT_photoGreaterThan(String value) {
            addCriterion("T_photo >", value, "t_photo");
            return (Criteria) this;
        }

        public Criteria andT_photoGreaterThanOrEqualTo(String value) {
            addCriterion("T_photo >=", value, "t_photo");
            return (Criteria) this;
        }

        public Criteria andT_photoLessThan(String value) {
            addCriterion("T_photo <", value, "t_photo");
            return (Criteria) this;
        }

        public Criteria andT_photoLessThanOrEqualTo(String value) {
            addCriterion("T_photo <=", value, "t_photo");
            return (Criteria) this;
        }

        public Criteria andT_photoLike(String value) {
            addCriterion("T_photo like", value, "t_photo");
            return (Criteria) this;
        }

        public Criteria andT_photoNotLike(String value) {
            addCriterion("T_photo not like", value, "t_photo");
            return (Criteria) this;
        }

        public Criteria andT_photoIn(List<String> values) {
            addCriterion("T_photo in", values, "t_photo");
            return (Criteria) this;
        }

        public Criteria andT_photoNotIn(List<String> values) {
            addCriterion("T_photo not in", values, "t_photo");
            return (Criteria) this;
        }

        public Criteria andT_photoBetween(String value1, String value2) {
            addCriterion("T_photo between", value1, value2, "t_photo");
            return (Criteria) this;
        }

        public Criteria andT_photoNotBetween(String value1, String value2) {
            addCriterion("T_photo not between", value1, value2, "t_photo");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}