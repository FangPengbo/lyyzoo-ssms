package com.baidu.mapper;

import com.baidu.entity.Adminer;
import com.baidu.entity.AdminerExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface AdminerMapper {
    int countByExample(AdminerExample example);

    int deleteByExample(AdminerExample example);

    int deleteByPrimaryKey(Integer a_id);

    int insert(Adminer record);

    int insertSelective(Adminer record);

    List<Adminer> selectByExample(AdminerExample example);

    Adminer selectByPrimaryKey(Integer a_id);

    int updateByExampleSelective(@Param("record") Adminer record, @Param("example") AdminerExample example);

    int updateByExample(@Param("record") Adminer record, @Param("example") AdminerExample example);

    int updateByPrimaryKeySelective(Adminer record);

    int updateByPrimaryKey(Adminer record);
}