package com.baidu.mapper;

import com.baidu.entity.ExamList;
import com.baidu.entity.Examination;
import com.baidu.entity.ExaminationAndClassGrade;
import com.baidu.entity.ExaminationExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ExaminationMapper {
	List<ExaminationAndClassGrade> selectExaminationAndClassGrade();
	
	List<ExamList> selectExam();
	int countByExample(ExaminationExample example);

    int deleteByExample(ExaminationExample example);

    int deleteByPrimaryKey(Integer e_id);

    int insert(Examination record);

    int insertSelective(Examination record);

    List<Examination> selectByExample(ExaminationExample example);

    Examination selectByPrimaryKey(Integer e_id);

    int updateByExampleSelective(@Param("record") Examination record, @Param("example") ExaminationExample example);

    int updateByExample(@Param("record") Examination record, @Param("example") ExaminationExample example);

    int updateByPrimaryKeySelective(Examination record);

    int updateByPrimaryKey(Examination record);
}