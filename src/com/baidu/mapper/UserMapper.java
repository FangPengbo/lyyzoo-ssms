package com.baidu.mapper;

import com.baidu.entity.AchiAExamACourse;
import com.baidu.entity.ExamAClassAGrade;
import com.baidu.entity.User;
import com.baidu.entity.UserAClassAGrade;
import com.baidu.entity.UserExample;
import com.baidu.entity.Userlist;

import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UserMapper {
	List<Userlist> selectuserListall();
	
	List<Userlist> selectListall();
	
	List<AchiAExamACourse> findScoreStudent(int u_id, int e_id);
	
	List<ExamAClassAGrade> ExamStudentList(int classid,int gradeid);
	
	List<UserAClassAGrade> findAllStudent(int classid,int gradeid);
	
	UserAClassAGrade findOneStudent(int uid);
	
    int countByExample(UserExample example);

    int deleteByExample(UserExample example);

    int deleteByPrimaryKey(List<Integer> tids);

    int insert(User record);

    int insertSelective(User record);

    List<User> selectByExample(UserExample example);

    User selectByPrimaryKey(Integer u_id);

    int updateByExampleSelective(@Param("record") User record, @Param("example") UserExample example);

    int updateByExample(@Param("record") User record, @Param("example") UserExample example);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);
}