package com.baidu.mapper;

import com.baidu.entity.Teacher;
import com.baidu.entity.TeacherExample;
import com.baidu.entity.TeacherGradeAndClassCourse;
import com.baidu.entity.TeacherGradeClassCourse;

import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TeacherMapper {
	//查询教师 信息
	TeacherGradeAndClassCourse selectTeacherAndGradeClassCourse1(int t_id);
	List<TeacherGradeClassCourse> selectteacherlist();
	List<TeacherGradeAndClassCourse> selectTeacher();
	List<TeacherGradeAndClassCourse> selectTeacherAndGradeClassCourse(int t_course);
	
	int countByExample(TeacherExample example);

    int deleteByExample(TeacherExample example);

    int deleteByPrimaryKey(List<Integer> tids);

    int insert(Teacher record);

    int insertSelective(Teacher record);

    List<Teacher> selectByExample(TeacherExample example);

    Teacher selectByPrimaryKey(Integer t_id);

    int updateByExampleSelective(@Param("record") Teacher record, @Param("example") TeacherExample example);

    int updateByExample(@Param("record") Teacher record, @Param("example") TeacherExample example);

    int updateByPrimaryKeySelective(Teacher record);

    int updateByPrimaryKey(Teacher record);
}