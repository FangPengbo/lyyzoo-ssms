package com.baidu.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baidu.entity.Adminer;
import com.baidu.entity.Teacher;
import com.baidu.entity.User;
import com.baidu.service.LoginService;

@Controller
public class LoginController {

	@Autowired
	LoginService lservice;
	
			@RequestMapping("forwardStudent")
			public String forwardStudent() {
				return"/WEB-INF/view/student/student.jsp";
			}
		@RequestMapping("forwardTeacher")
		public String forwardTeacher() {
			return"/WEB-INF/view/teacher/teacher.jsp";
		}
	@RequestMapping("forwardAdmin")
	public String forwardAdmin() {
		return"/WEB-INF/view/admin/admin.jsp";
	}
	
	@RequestMapping(value="/login",method= {RequestMethod.GET,RequestMethod.POST})
	@ResponseBody
	public String login(HttpSession session,String acc,String pwd,int type) {
			if(type==1) {
				List<Adminer> list = lservice.adminlogin(acc, pwd);
				if(list!=null && list.size()>0) {
					Adminer admin = list.get(0);
					session.setAttribute("admin", admin);
					return "admin";
				}else {
					return "loginError";
				}
				
			}else if(type==2) {
				List<User> list = lservice.userlogin(acc, pwd);
				if(list!=null && list.size()>0) {
					User student = list.get(0);
					session.setAttribute("student", student);
					return "student";
				}else {
					return "loginError";
				}
			}else if (type==3) {
				List<Teacher> list = lservice.teacherlogin(acc, pwd);
				if(list!=null && list.size()>0) {
					Teacher teacher = list.get(0);
					session.setAttribute("teacher", teacher);
					return "teacher";
				}else {
					return "loginError";
				}
			}  
			return "loginError";
	}
	
	
	
	
}
