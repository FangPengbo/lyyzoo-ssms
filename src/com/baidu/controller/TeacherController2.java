package com.baidu.controller;


import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baidu.entity.Teacher;
import com.baidu.entity.TeacherGradeClassCourse;
import com.baidu.service.TeacherService;
import com.baidu.service.TeacherService2;
/**
 * 卢鑫
 * @author FangPengbo
 *
 */
@Controller
public class TeacherController2 {
	
	@Autowired
	private TeacherService2 teacherservice;
	//教师列表显示
	@RequestMapping("TeacherServlet")
	public String teacherlist(HttpServletRequest request) {
		List<TeacherGradeClassCourse> list = teacherservice.selectteacherlist();
		request.setAttribute("list",list);
		if(list!=null&&list.size()>0) {
			return "/WEB-INF/view/teacher/teacherList.jsp";
		}
		return "404.jsp";	
	}
	//删除教师
	@RequestMapping("DeleteTeacher")
	@ResponseBody
	public String deleteteacher(String ids) {
		List<Integer> tids=new ArrayList<Integer>();
		//切割字符串
		String[] idss = ids.split(",");
		for (String string : idss) {
			tids.add(Integer.valueOf(string));
		}
		int i = teacherservice.deleteteachers(tids);
		if(i>0) {
			return "success";
		}
		
		return "error";
		
		
	}
	//添加教师
	@RequestMapping("addTeacher")
	@ResponseBody
	public String addTeacher(Teacher teacher) {
		teacher.setT_grade(1);
		teacher.setT_class(1);
		teacher.setT_course(1);
		int i = teacherservice.addteacher(teacher);
		if(i>0) {
		return "success";
		}
		return "error";
	}
	//修改教师内容
	@RequestMapping("updateteacher2")
	@ResponseBody
	public String updateteacher(Teacher teacher) {
		teacher.setT_id(teacher.getT_id());
		int i = teacherservice.updateteacher(teacher);
		if(i>0) {
			return "success";
		}
		return "error";
	}
	

}
