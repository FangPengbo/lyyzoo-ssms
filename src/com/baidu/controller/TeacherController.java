package com.baidu.controller;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.baidu.entity.AchievementAndCourse;
import com.baidu.entity.Class;
import com.baidu.entity.Examination;
import com.baidu.entity.ExaminationAndClassGrade;
import com.baidu.entity.Grade;
import com.baidu.entity.Teacher;
import com.baidu.entity.TeacherGradeAndClassCourse;
import com.baidu.service.AchievementService;
import com.baidu.service.ExaminationService;
import com.baidu.service.TeacherService;

import net.sf.json.JSONArray;
import sun.net.www.content.image.png;
/**
 * 田硕
 */
@Controller
public class TeacherController {
	@Autowired
	private ExaminationService examinationService;

	// 教学管理列表
	@RequestMapping("ExaminationList")
	public String ExaminationList(HttpServletRequest request) {
		List<ExaminationAndClassGrade> list = examinationService.ExaminationList();
		request.setAttribute("list", list);
		if (list.size() > 0 && list != null) {
			return "/WEB-INF/view/teacher/examTeacherList.jsp";
		}
		return "error.jsp";
	}
	// 添加考试列表
	@RequestMapping("AddExam")
	@ResponseBody
	public String AddExam(String name, String etime, String remark, String type,String c_name,String g_name) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date time = sdf.parse(etime);
		Examination examination = new Examination();
		examination.setE_name(name);
		examination.setE_time(time);
		examination.setE_beizhu(remark);
		examination.setE_type(type);
		examination.setE_class(1);
		examination.setE_grade(1);
		int i = examinationService.AddExamination(examination);
		if (i > 0) {
			return "success";
		}
		return "error";
	}

	@Autowired
	private TeacherService teacherService;

	// 查询个人信息
		@RequestMapping("selectGradeAndClass")
		public String selectGradeAndClass(HttpServletRequest request,HttpSession session) {
			Teacher t = (Teacher) session.getAttribute("teacher");
			TeacherGradeAndClassCourse teacher = teacherService.selectGradeAndClass1(t.getT_id());
			request.setAttribute("teacher", teacher);
			if (teacher != null) {
				System.out.println(teacher);
				return "/WEB-INF/view/teacher/teacherPersonal.jsp";
			}
			return "error.jsp";
		}

	// 更新个人信息  
	@RequestMapping("updateTeacher")
	@ResponseBody
	public String updateTeacher(Teacher teacher) {
		int i = teacherService.updateTeacher(teacher);
		if (i > 0) {
			return "success";
		}
		return "error";
	}

	// 查询教师信息
	@RequestMapping("selectTeacher")
	public String selectTeacher(HttpServletRequest request) {
		List<TeacherGradeAndClassCourse> list = teacherService.selectTeacher();
		request.setAttribute("list", list);
		if (list.size() > 0 && list != null) {
			System.out.println(list);
			return "/WEB-INF/view/teacher/teacherNoteList.jsp";
		}
		return "error.jsp";
	}

	// 查看一个教师的信息
	@RequestMapping("selectTeacherOne")
	public String selectTeacherOne(int t_id) {
		Teacher teacher = teacherService.selectTeacherOne(t_id);
		if (teacher != null) {
			return "/WEB-INF/view/teacher/teacherNoteList.jsp";
		}
		return "error.jsp";
	}

	// 修改教师密码
	@RequestMapping("updateTeacherPwd")
	@ResponseBody
	public String updateTeacherPwd(HttpSession session, String password) {
		Teacher t=(Teacher) session.getAttribute("teacher");
		Teacher teacher = new Teacher();
		teacher.setT_id(t.getT_id());
		teacher.setT_phone(password);
		int i = teacherService.updateTeacher(teacher);
		if (i > 0) {
			return "success";
		}
		return "error";
	}

	/**
	 * 查询成绩
	 * 
	 * @return
	 */
	@Autowired
	private AchievementService achievementService;

	@RequestMapping("selectExamByClass")
	public String selectExamByClass(HttpServletRequest request) throws IOException {
		request.setCharacterEncoding("utf-8");
		List<AchievementAndCourse> list2 = achievementService.selectAll();
		request.setAttribute("list2", list2);
		 String string = JSONArray.fromObject(list2).toString();
		if (list2 != null && list2.size() > 0) {
			return string;
		}
		return "error";
	}
	
	
	
}
