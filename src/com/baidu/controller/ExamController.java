package com.baidu.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baidu.entity.ExamList;
import com.baidu.entity.Examination;
import com.baidu.service.ExamService;

@Controller
public class ExamController {
	@Autowired
	private ExamService examService;
	
//成绩列表
	@RequestMapping("Examlist")
	public String Examlist(HttpServletRequest request) {
		List<ExamList> list = examService.getAll();
		request.setAttribute("list", list);
		if(list.size()>0 && list != null) {
			return "/WEB-INF/view/other/examList.jsp";
		}
		return "error.jsp";
	}

//添加考试
	@RequestMapping("addExam")
	@ResponseBody
	public String addExam(String e_name,String e_time,String e_type,String e_beizhu,Integer e_class,Integer e_grade) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date time = sdf.parse(e_time);
		Examination examination = new Examination();
		examination.setE_name(e_name);
		examination.setE_type(e_type);
		examination.setE_beizhu(e_beizhu);
		examination.setE_class(e_class);
		examination.setE_grade(e_grade);
		examination.setE_time(time);
		int i = examService.addExam(examination);
		if(i>0) {
			return "success";
		}
		return "error";
	}
	
	
//删除
	@RequestMapping("deleteExam")
	@ResponseBody
	public String deleteExam(Integer e_id) {
		int i = examService.deleteExam(e_id);
		if(i>0) {
			return "success";
		}
		return "error";
	}
	
	
}
