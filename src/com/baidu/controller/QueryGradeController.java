package com.baidu.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baidu.entity.Grade;
import com.baidu.mapper.GradeMapper;
import com.baidu.service.GradeService;

@Controller
public class QueryGradeController {
	@Autowired
	private GradeService gradeService;
	
	
	//添加年级
	@RequestMapping("addGrade")
	@ResponseBody
	public String addGrade(Grade grade) {
		int i = gradeService.addgrade(grade);
		if (i>0) {
			return "success";
		}
		return "error";
	}
	
	
		//查询全部年级
		@RequestMapping(value="QueryAllGrade")
		public String QueryAllGrade(HttpServletRequest request) {
			List<Grade> list=gradeService.getAll();
			request.setAttribute("list", list);
			if (list.size()>0 && list!=null) {
				return "/WEB-INF/view/other/gradeList.jsp";
			}
			return "error.jsp";
		}
		//删除单个班级
		@RequestMapping(value="DeleteGrade")
		@ResponseBody
		public String deleteGrade(Integer g_id){
			int i = gradeService.deletegrade(g_id);
			if (i>0) {
				return "success"; 
			}
			return "error";
		}
}
