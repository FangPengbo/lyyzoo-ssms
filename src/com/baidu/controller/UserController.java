package com.baidu.controller;


import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.baidu.entity.AchiAExamACourse;
import com.baidu.entity.ExamAClassAGrade;
import com.baidu.entity.Examination;
import com.baidu.entity.User;
import com.baidu.entity.UserAClassAGrade;
import com.baidu.service.UserService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Controller
public class UserController {

	@Autowired
	UserService uservice;
	
	/**
	 * 获取学生各科成绩
	 */
	@RequestMapping(value="findScoreStudent",produces="html/text;charset=UTF-8")
	@ResponseBody
	public String findScoreStudent(HttpSession session,int e_id,HttpServletResponse response) {
		response.setCharacterEncoding("utf-8");
		//获取登录的学生信息
		User student=(User) session.getAttribute("student");
		List<AchiAExamACourse> list = uservice.findScoreStudent(student.getU_id(),e_id);
		 //转换为json数据
        String string = JSONArray.fromObject(list).toString();
		return string;
	}
	
	
	/**
	 * 获取学生考试列表
	 * @throws UnsupportedEncodingException 
	 */
	@RequestMapping(value="ExamStudentList",produces="html/text;charset=UTF-8")
	@ResponseBody
	public String ExamStudentList(HttpSession session,HttpServletRequest response) throws UnsupportedEncodingException {
		response.setCharacterEncoding("utf-8");
		//获取登录的学生信息
		User student=(User) session.getAttribute("student");
		//获取登录的学生信息
		List<ExamAClassAGrade> list = uservice.ExamStudentList(student.getU_class(),student.getU_grade());
        //转换为json数据
        String string = JSONArray.fromObject(list).toString();
		return string;
	}
	
	/**
	 * 转发到考试列表
	 */
	@RequestMapping("forwardExamList")
	public String forwardExamList() {
		return "/WEB-INF/view/student/examStudentList.jsp";
	}
	
	
	/**
	 * 转发学生列表
	 */
	@RequestMapping("forwardStudent1")
	public String forwardStudent() {
		return "/WEB-INF/view/student/studentNoteList.jsp";
	}
	
	/**
	 * 查找所有学生的信息
	 * 
	 */
	@RequestMapping(value="/findAllStudent",method= {RequestMethod.GET,RequestMethod.POST},
			produces="html/text;charset=UTF-8")
	@ResponseBody
	public String findAllStudent(HttpServletRequest request,HttpServletResponse response,HttpSession session)  {
		response.setCharacterEncoding("utf-8");
		//获取登录的学生信息
		User student=(User) session.getAttribute("student");
		List<UserAClassAGrade> list = uservice.findAllStudent(student.getU_class(),student.getU_grade());
		Map<String, Object> jsonMap = new HashMap<String, Object>(); 
		//total键 存放总记录数，必须的
        jsonMap.put("total", list.size());
        //rows键 存放每页记录 list 
        jsonMap.put("rows", list); 
        //转换为json数据
        String string = JSONObject.fromObject(jsonMap).toString();
		return string;
	}
	
	
	/**
	 * 修改学生密码
	 */
	@RequestMapping("UpdateUphone")
	@ResponseBody
	public String UpdateUphone(HttpSession session,String password) {
		//获取登录的学生信息
		User student=(User) session.getAttribute("student");
		User user=new User();
		user.setU_id(student.getU_id());
		user.setU_phone(password);
		int i = uservice.updateByPrimaryKeySelective(user);
		if(i>0) {
			return"success";
		}
		return "error";
	}
	
	/**
	 * 修改学生照片
	 */
	@RequestMapping("updatePhoto")
	public String updatePhoto(HttpSession session) {
		//获取登录的学生信息
		User student=(User) session.getAttribute("student");
		String fileName = session.getAttribute("userphotoName").toString();
		fileName="/photo/"+fileName;
		User user=new User();
		user.setU_id(student.getU_id());
		user.setU_photo(fileName);
		int i = uservice.updateByPrimaryKeySelective(user);
		return "findOneStudent";
	}
	
	
	/**
	 * 上传学生照片
	 * @throws IOException 
	 * @throws IllegalStateException 
	 */
	@RequestMapping("UserPhoto")
	@ResponseBody
	public String UserPhoto(MultipartFile photo,HttpSession session) throws IllegalStateException, IOException {
		if(photo.getSize()>0) {
			//获取登录的学生信息
			User student=(User) session.getAttribute("student");
			//获取文件名作为保存到服务器的文件名称  (采用学号.jpg固定)
			String fileName = photo.getOriginalFilename();
			if(fileName.endsWith("jpg")) {
				fileName=student.getU_sno()+".jpg";
			}else if(fileName.endsWith("png")) {
				fileName=student.getU_sno()+".png";
			}else if(fileName.endsWith("gif")) {
				fileName="student.getU_sno()+.gif";
			}else if(fileName.endsWith("tif")) {
				fileName="student.getU_sno()+.tif";
			}
			//拼接前半部分路径,目录,将WebRoot下的一个名称为photo文件夹转换成绝对路径
			String leftPath=session.getServletContext().getRealPath("/photo");
			//进行路径拼接
			File file=new File(leftPath);
			if(!file.exists()) {
				//如果文件夹不存在则创建
				file.mkdir();
			}
			File file2=new File(leftPath,fileName);
			//保存文件
			photo.transferTo(file2);
			session.setAttribute("userphotoName",fileName);
			return"success";
		}
		return "error";
	}
	
	/**
	 * 更新学生的信息
	 */
	@RequestMapping("UpdateStudent1")
	@ResponseBody
	public String UpdateStudent(UserAClassAGrade user) {
		//把前台的数据转成User类型
		User user1=new User();
		user1.setU_id(user.getU_Id());
		user1.setU_sno(user.getU_Sno());
		user1.setU_name(user.getU_Name());
		user1.setU_sex(user.getU_Sex());
		user1.setU_phone(user.getU_Phone());
		user1.setU_qq(user.getU_Qq());
		//调用逆向工程的通过主键修改实体
		int i = uservice.updateByPrimaryKeySelective(user1);
		if(i>0) {
			//通过ajax返回成功数据
			return "success";
		}
		//通过ajax返回数据
		return "error";
	}
	
	/**
	 * 查找一个学生的信息
	 */
	@RequestMapping(value="/findOneStudent",method= {RequestMethod.GET,RequestMethod.POST})
	public String findOneStudent(HttpServletRequest request,HttpSession session) {
		//获取登录的学生信息
		User student=(User) session.getAttribute("student");
		UserAClassAGrade user = uservice.findOneStudent(student.getU_id());
		if(user!=null) {
			request.setAttribute("userDetail", user);
			return "/WEB-INF/view/student/studentPersonal.jsp";
		}
		return "404.jsp";
	}
	
	
}
