package com.baidu.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baidu.entity.Class;
import com.baidu.entity.ClassAndGrade;
import com.baidu.entity.Grade;
import com.baidu.service.ClassService;

@Controller
public class QueryAllClassController {
		@Autowired
		private ClassService classService;
		
		//��Ӱ༶and �꼶
		@RequestMapping("AddClassAndGrade")
		@ResponseBody
		public String AddClassAndGrade(String cname,String gname) {
			Class c=new Class();
			Grade g=new Grade();
			c.setC_name(cname);
			g.setG_name(gname);
			int i = classService.addclass(c);
			int j = classService.addgrade(g);
			if (i>0 &&j>0) {
				return "success";
			}
			return "error";
		}
		
//		��ѯȫ���༶
		@RequestMapping(value="QueryAllClass")
		public String QueryAllClass(HttpServletRequest request) {
			List<ClassAndGrade> list1=classService.getAll();
			request.setAttribute("list1", list1);
			if (list1.size()>0 && list1!=null) {
				return "/WEB-INF/view/other/clazzList.jsp";
			}
			return "404.jsp";
		}
		//ɾ��
		@RequestMapping(value="DeleteClass")
		@ResponseBody
		public String deleteClass(Integer c_id) {
			int i = classService.deleteclass(c_id);
			if (i>0) {
				return "success";	
			}
			return "error";
		}
}
