package com.baidu.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baidu.entity.Class;
import com.baidu.entity.Grade;
import com.baidu.entity.User;
import com.baidu.entity.Userlist;
import com.baidu.service.Selectlist;
/**
 * 曾新
 * @author FangPengbo
 *
 */
@Controller
public class UserAndAdminController {

	@Autowired
	private Selectlist selectlist;

	/**
	 * 学生信息管理
	 */
	// 学生列表查看
	// 用来查询列表返回到可以查看信息的jsp页面
	@RequestMapping("Userall")
	public String Userall(HttpServletRequest request) {
		/*
		 * UserMapper mapper = (UserMapper) ac.getBean("userMapper"); Grade grade = new
		 * Grade(); UserExample userExample = new UserExample(); List<User> list =
		 * mapper.selectByExample(userExample);
		 */
		List<Userlist> list = selectlist.gradelist();
		request.setAttribute("list", list);
		if (list.size() > 0 && list != null) {
			System.out.println(list);
			return "/WEB-INF/view/student/studentAllList.jsp";
		}
		return "error";
	}

	// 学生列表查看
	// 查询三张表的信息封装到list中返回 到学生基本信息操作的jsp页面
	@RequestMapping("StudentList")
	public String StudentList(HttpServletRequest request) {
		List<Grade> list1 = selectlist.glist();
		request.setAttribute("list1", list1);
		List<Class> list2 = selectlist.clist();
		request.setAttribute("list2", list2);
		List<Userlist> list = selectlist.gradelist();
		request.setAttribute("list", list);
		if (list.size() > 0 && list != null) {
			System.out.println(list);
			return "/WEB-INF/view/student/studentList.jsp";
		}
		return "error";
	}

	// 学生信息的添加: 用来添加学生
	@RequestMapping("Studentadd")
	@ResponseBody
	public String Studentadd(User user) {
		user.setU_photo("\\photo\\student.jpg");
		int i = selectlist.userinsert(user);
		if (i > 0) {
			return "success";
		}
		return "error";
	}

	// 学生信息的删除: 用来删除学生
	@RequestMapping("DeleteUser")
	@ResponseBody
	public String DeleteUser(String ids) {
		List<Integer> tids = new ArrayList<Integer>();
		// 切割字符串
		String[] idss = ids.split(",");
		for (String string : idss) {
			tids.add(Integer.valueOf(string));
		}
		int i = selectlist.userdelete(tids);
		if (i > 0) {
			return "success";
		}
		return "error";

	}

	// 学生信息的修改: 用来修改学生
	@RequestMapping("UpdateStudent")
	@ResponseBody
	public String UpdateStudent(User user) {
		user.setU_id(user.getU_id());
		user.setU_sno(user.getU_sno());
		user.setU_name(user.getU_name());
		user.setU_sex(user.getU_sex());
		user.setU_phone(user.getU_phone());
		user.setU_qq(user.getU_qq());
		int i = selectlist.updateByPrimaryKeySelective(user);
		if (i > 0) {
			return "success";
		}
		return "error";
	}

}
