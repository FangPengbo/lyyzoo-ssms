package com.baidu.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baidu.entity.AchiAExamACourse;
import com.baidu.entity.ExamAClassAGrade;
import com.baidu.entity.Examination;
import com.baidu.entity.ExaminationExample;
import com.baidu.entity.ExaminationExample.Criteria;
import com.baidu.entity.User;
import com.baidu.entity.UserAClassAGrade;
import com.baidu.mapper.ExaminationMapper;
import com.baidu.mapper.UserMapper;
import com.baidu.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserMapper umapper;
	
	//查找一个学生的信息
	@Override
	public UserAClassAGrade findOneStudent(int uid) {
		return umapper.findOneStudent(uid);
	}
	//根据id修改学生信息
	@Override
	public int updateByPrimaryKeySelective(User user) {
		return umapper.updateByPrimaryKeySelective(user);
	}
	//查找所有学生的信息
	@Override
	public List<UserAClassAGrade> findAllStudent(int classid,int gradeid) {
		return umapper.findAllStudent(classid,gradeid);
	}
	//查找学生考试信息
	@Override
	public List<ExamAClassAGrade> ExamStudentList(int classid, int gradeid) {
		return umapper.ExamStudentList(classid,gradeid);
	}
	//获取学生各科成绩
	@Override
	public List<AchiAExamACourse> findScoreStudent(int u_id, int e_id) {
		return umapper.findScoreStudent(u_id,e_id);
	}

}
