package com.baidu.service.impl;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baidu.entity.Class;
import com.baidu.entity.Examination;
import com.baidu.entity.ExaminationAndClassGrade;
import com.baidu.entity.Grade;
import com.baidu.mapper.ClassMapper;
import com.baidu.mapper.ExaminationMapper;
import com.baidu.mapper.GradeMapper;
import com.baidu.service.ExaminationService;

@Service
@Transactional
public class ExaminationServiceImpl implements ExaminationService {
	@Autowired
	private ExaminationMapper examinationMapper;
	@Autowired
	private ClassMapper classmapper;
	@Autowired
	private GradeMapper gradeMapper;
	//列表
	@Override
	public List<ExaminationAndClassGrade> ExaminationList() {
		List<ExaminationAndClassGrade> list = examinationMapper.selectExaminationAndClassGrade();
		return list;
	}
	//添加
	@Override
	public int AddExamination(Examination examination) {
		int i = examinationMapper.insertSelective( examination);
		return i;
	}

}
