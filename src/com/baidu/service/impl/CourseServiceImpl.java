package com.baidu.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baidu.entity.Course;
import com.baidu.mapper.CourseMapper;
import com.baidu.service.CourseService;
@Service
public class CourseServiceImpl implements CourseService {
@Autowired
private CourseMapper courseMapper;
	@Override
	public List<Course> getAll() {
		// TODO Auto-generated method stub
		List<Course> list=courseMapper.selectByExample(null);
		return list;
	}
	@Override
	public int deletecoure(Integer s_id) {
		// TODO Auto-generated method stub
		int i = courseMapper.deleteByPrimaryKey(s_id);
		return i;
	}
	@Override
	public int addcourse(Course course) {
		// TODO Auto-generated method stub
		int i = courseMapper.insert(course);
		return i;
	}

}
