package com.baidu.service.impl;


import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baidu.mapper.AchievementMapper;
import com.baidu.mapper.ClassMapper;
import com.baidu.mapper.GradeMapper;
import com.baidu.mapper.UserMapper;
import com.baidu.service.Selectlist;
import com.baidu.entity.Class;
import com.baidu.entity.Grade;
import com.baidu.entity.User;
import com.baidu.entity.Userlist;

@Service
public class SelectlistImpl implements Selectlist {

	@Autowired
	private UserMapper userMapper;
	@Autowired
	private GradeMapper gradeMapper;
	@Autowired
	private ClassMapper classMapper;
	@Autowired

	//查询三表的信息 : 将三张表的数据封装到一块
	@Override
	public List<Userlist> gradelist() {
		List<Userlist> list = userMapper.selectuserListall();
		return list;
	}

	//添加学生表的信息
	@Override
	public int userinsert(User user) {
		int i = userMapper.insert(user);
		return i;
	}

	//查询年级表的信息
	@Override
	public List<Grade> glist() {
		List<Grade> list1 = gradeMapper.selectByExample(null);
		return list1;
	}

	//查询班级表的信息
	@Override
	public List<Class> clist() {
		List<Class> list2 = classMapper.selectByExample(null);
		return list2;
	}

	
	// 删除学生表的信息
	@Override
	public int userdelete(List<Integer> tids) {
		int i = userMapper.deleteByPrimaryKey(tids);
		return i;
		
	}
	
	//修改学生表的信息
	@Override
	public int updateByPrimaryKeySelective(User user) {
		return userMapper.updateByPrimaryKeySelective(user);
	}


}
