package com.baidu.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baidu.entity.ExamList;
import com.baidu.entity.Examination;
import com.baidu.mapper.ExaminationMapper;
import com.baidu.service.ExamService;
@Service
@Transactional
public class ExamServiceImpl implements ExamService {
	@Autowired
	private ExaminationMapper examinationMapper;
	
	//成绩列表
	@Override
	public List<ExamList> getAll() {
		List<ExamList> list = examinationMapper.selectExam();
		return list;
	}
	
	//添加考试
		@Override
		public int addExam(Examination examination) {
			int i = examinationMapper.insert(examination);	
			return i;
		}
	
	//删除
	@Override
	public int deleteExam(Integer e_id) {
		int i = examinationMapper.deleteByPrimaryKey(e_id);
		return i;
	}
	
	
	
	
	
	
}
