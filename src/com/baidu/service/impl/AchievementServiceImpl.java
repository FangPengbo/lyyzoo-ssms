package com.baidu.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baidu.entity.AchievementAndCourse;
import com.baidu.mapper.AchievementMapper;
import com.baidu.service.AchievementService;
@Service
public class AchievementServiceImpl implements AchievementService{
	@Autowired
	private AchievementMapper achievementMapper;
	@Override
	public List<AchievementAndCourse> selectAll() {
		List<AchievementAndCourse> list = achievementMapper.selectExamByClass();
		return list;
	}
	
}
