package com.baidu.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baidu.entity.Teacher;
import com.baidu.entity.TeacherExample;
import com.baidu.entity.TeacherExample.Criteria;
import com.baidu.entity.TeacherGradeAndClassCourse;
import com.baidu.mapper.TeacherMapper;
import com.baidu.service.TeacherService;
@Service
public class TeacherServiceImpl implements TeacherService {
	@Autowired
	private TeacherMapper teacherMapper;
	//教师个人信息
		@Override
		public TeacherGradeAndClassCourse selectGradeAndClass1(int t_id) {
			TeacherGradeAndClassCourse teacherAndGradeClassCourse = teacherMapper.selectTeacherAndGradeClassCourse1(t_id);
			return teacherAndGradeClassCourse;
		}
	@Override
	public Teacher selectTeacher(int t_id) {
		Teacher teacher = teacherMapper.selectByPrimaryKey(t_id);
		return teacher;
	}
	//教师个人信息
	@Override
	public List<TeacherGradeAndClassCourse> selectGradeAndClass() {
		List<TeacherGradeAndClassCourse> list = teacherMapper.selectTeacherAndGradeClassCourse(1);
		return list;
	}
	//修改教师信息
	@Override
	public int updateTeacher(Teacher teacher) {
		int i = teacherMapper.updateByPrimaryKeySelective(teacher);
		return i;
	}
	@Override
	public List<TeacherGradeAndClassCourse> selectTeacher() {
		List<TeacherGradeAndClassCourse> list = teacherMapper.selectTeacher();
		return list;
	}
	@Override
	public Teacher selectTeacherOne(int t_id) {
		Teacher teacher = teacherMapper.selectByPrimaryKey(t_id);
		return teacher;
	}
	/**
	 * 修改教師照片
	 */
	@Override
	public int updateByPrimaryKeySelective(Teacher teacher1) {
		int i = teacherMapper.updateByPrimaryKeySelective(teacher1);
		return i;
	}

}
