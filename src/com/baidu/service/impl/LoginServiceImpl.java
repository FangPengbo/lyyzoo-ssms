package com.baidu.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baidu.entity.Adminer;
import com.baidu.entity.AdminerExample;
import com.baidu.entity.AdminerExample.Criteria;
import com.baidu.entity.Teacher;
import com.baidu.entity.TeacherExample;
import com.baidu.entity.User;
import com.baidu.entity.UserExample;
import com.baidu.mapper.AdminerMapper;
import com.baidu.mapper.TeacherMapper;
import com.baidu.mapper.UserMapper;
import com.baidu.service.LoginService;
@Service
@Transactional
public class LoginServiceImpl implements LoginService {

	
	//管理员mapper
	@Autowired
	AdminerMapper amapper;
	//老师mapper
	@Autowired
	TeacherMapper tmapper;
	//学生mapper
		@Autowired
		UserMapper umapper;
	//管理员登陆
	@Override
	public List<Adminer> adminlogin(String acc, String pwd) {
		AdminerExample example=new AdminerExample();
		Criteria criteria = example.createCriteria();
		criteria.andA_unameEqualTo(acc);
		criteria.andA_pwdEqualTo(pwd);
		List<Adminer> list = amapper.selectByExample(example);
		return list;
	}
	//教师登陆
	@Override
	public List<Teacher> teacherlogin(String acc, String pwd) {
		TeacherExample example=new TeacherExample();
		com.baidu.entity.TeacherExample.Criteria create = example.createCriteria();
		create.andT_nameEqualTo(acc);
		create.andT_phoneEqualTo(pwd);
		List<Teacher> list = tmapper.selectByExample(example);
		return list;
	}
	//学生登陆
	@Override
	public List<User> userlogin(String acc, String pwd) {
		// TODO Auto-generated method stub
		UserExample example=new UserExample();
		com.baidu.entity.UserExample.Criteria create1 = example.createCriteria();
		create1.andU_nameEqualTo(acc);
		create1.andU_phoneEqualTo(pwd);
		List<User> list = umapper.selectByExample(example);
		return list;
	}
		

}
