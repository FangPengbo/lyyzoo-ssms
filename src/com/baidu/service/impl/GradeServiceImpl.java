package com.baidu.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baidu.entity.Grade;
import com.baidu.mapper.GradeMapper;
import com.baidu.service.GradeService;
@Service
public class GradeServiceImpl implements GradeService{
@Autowired
private GradeMapper gradeMapper;
	@Override
	public List<Grade> getAll() {
		// TODO Auto-generated method stub
		List<Grade> list=gradeMapper.selectByExample(null);
		return list;
	}
	@Override
	public int deletegrade(Integer g_id) {
		// TODO Auto-generated method stub
		int i = gradeMapper.deleteByPrimaryKey(g_id);
		return i;
	}
	@Override
	public int addgrade(Grade grade) {
		// TODO Auto-generated method stub
		int i = gradeMapper.insert(grade);
		return i;
	}

}
