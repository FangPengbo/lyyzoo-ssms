package com.baidu.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baidu.entity.Teacher;
import com.baidu.entity.TeacherGradeClassCourse;
import com.baidu.mapper.TeacherMapper;
import com.baidu.service.TeacherService;
import com.baidu.service.TeacherService2;

@Service
public class TeacherService2Impl implements TeacherService2 {
	@Autowired
	private TeacherMapper teachermapper;

//查詢教师列表
	@Override
	public List<TeacherGradeClassCourse> selectteacherlist() {
		// TODO Auto-generated method stub
		List<TeacherGradeClassCourse> list = teachermapper.selectteacherlist();
		return list;
	}

//添加教师
	@Override
	public int addteacher(Teacher teacher) {
		// TODO Auto-generated method stub
		int i = teachermapper.insert(teacher);
		return i;
	}

//修改老师信息

	@Override
	public int updateteacher(Teacher teacher) {
		// TODO Auto-generated method stub
		return teachermapper.updateByPrimaryKeySelective(teacher);
	}

//删除老师信息
	
	@Override
	public int deleteteachers(List<Integer> tids) {
		// TODO Auto-generated method stub
		
		return teachermapper.deleteByPrimaryKey(tids);
	}

	
}
