package com.baidu.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baidu.entity.Class;
import com.baidu.entity.ClassAndGrade;
import com.baidu.entity.Grade;
import com.baidu.mapper.ClassMapper;
import com.baidu.mapper.GradeMapper;
import com.baidu.service.ClassService;
@Service
public class ClassServiceImpl implements ClassService {
	//�鿴ȫ��
	@Autowired
	private ClassMapper classMapper;
	@Autowired
	private GradeMapper grademapper;
	
	@Override
	public Class selectClass(int cid) {
		// TODO Auto-generated method stub
		Class clas=classMapper.selectByPrimaryKey(1);
				return clas;
	}

	@Override
	public List<ClassAndGrade> getAll() {
		// TODO Auto-generated method stub
		List<ClassAndGrade> list=classMapper.selectClassAndGrade();
		return list;
	}
//ɾ��
	@Override
	public int deleteclass(Integer c_id) {
		// TODO Auto-generated method stub
		int i = classMapper.deleteByPrimaryKey(c_id);
		return i;
	}
//���
	@Override
	public int addclass(Class classs) {
		// TODO Auto-generated method stub
		int i = classMapper.insertSelective(classs);
		return i;
	}

	@Override
	public int addgrade(Grade grade) {
		
		int i = grademapper.insertSelective(grade);
		return i;
	}
}
