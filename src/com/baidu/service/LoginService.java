package com.baidu.service;

import java.util.List;

import com.baidu.entity.Adminer;
import com.baidu.entity.Teacher;
import com.baidu.entity.User;

public interface LoginService {

	
	//管理员登陆
	public List<Adminer> adminlogin(String acc,String pwd);
	//教师登陆
	public List<Teacher> teacherlogin(String acc,String pwd);
	//学生登陆
	public List<User> userlogin(String acc,String pwd);
	
}
