package com.baidu.service;

import java.util.List;

import com.baidu.entity.Teacher;
import com.baidu.entity.TeacherGradeClassCourse;

public interface TeacherService2 {
	//获取老师列表
	List<TeacherGradeClassCourse> selectteacherlist();
	//添加老师
	int addteacher(Teacher teacher);
	//删除老师
	int deleteteachers(List<Integer> tids);
	//修改老师信息
   public int updateteacher(Teacher teacher);
}
