package com.baidu.service;


import java.util.List;

import com.baidu.entity.Teacher;
import com.baidu.entity.TeacherGradeAndClassCourse;

public interface TeacherService {
	//教师个人信息
	public Teacher selectTeacher(int t_id);
	TeacherGradeAndClassCourse selectGradeAndClass1(int t_id);
	List<TeacherGradeAndClassCourse> selectGradeAndClass();
	//修改教师信息
	int updateTeacher(Teacher teacher);
	//教师通讯录
	List<TeacherGradeAndClassCourse> selectTeacher();
	//查询一个教师的信息
	Teacher selectTeacherOne(int t_id);
	int updateByPrimaryKeySelective(Teacher teacher1);
}
