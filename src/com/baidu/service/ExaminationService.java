package com.baidu.service;

import java.util.List;

import com.baidu.entity.Class;
import com.baidu.entity.Examination;
import com.baidu.entity.ExaminationAndClassGrade;
import com.baidu.entity.Grade;

public interface ExaminationService {
	//列表
	List<ExaminationAndClassGrade> ExaminationList();
	//添加
	int AddExamination(Examination examination);
}