package com.baidu.service;

import java.util.List;

import com.baidu.entity.Grade;

public interface GradeService {
	//查询全部信息
		List<Grade> getAll();
		//删除
		int deletegrade(Integer g_id);
		//添加
		int addgrade(Grade grade);
}
