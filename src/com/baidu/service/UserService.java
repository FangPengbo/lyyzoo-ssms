package com.baidu.service;

import java.util.List;

import com.baidu.entity.AchiAExamACourse;
import com.baidu.entity.ExamAClassAGrade;
import com.baidu.entity.Examination;
import com.baidu.entity.User;
import com.baidu.entity.UserAClassAGrade;

/**
 * 学生业务层接口
 * @author FangPengbo
 *
 */

public interface UserService {
	//获取学生各科成绩
	public List<AchiAExamACourse> findScoreStudent(int u_id,int e_id);
	//查找学生考试信息
	public List<ExamAClassAGrade> ExamStudentList(int classid,int gradeid);
	//查找所有学生的信息
	public List<UserAClassAGrade> findAllStudent(int classid,int gradeid);
	//根据id查找学生的信息
	public UserAClassAGrade findOneStudent(int uid);
	//根据id修改学生信息
	public int updateByPrimaryKeySelective(User user);
}
