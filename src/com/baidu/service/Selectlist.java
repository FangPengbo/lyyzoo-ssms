package com.baidu.service;

import java.util.List;

import com.baidu.entity.Class;
import com.baidu.entity.Grade;
import com.baidu.entity.User;
import com.baidu.entity.Userlist;

public interface Selectlist {

	// 查询三表的信息 : 将三张表的数据封装到一块
	List<Userlist> gradelist();

	// 查询年级表的信息
	List<Grade> glist();

	// 查询班级表的信息
	List<Class> clist();

	// 添加学生表的信息
	int userinsert(User user);

	// 修改学生表的信息
	int updateByPrimaryKeySelective(User user);

	// 删除学生表的信息
	int userdelete(List<Integer> tids);
}
