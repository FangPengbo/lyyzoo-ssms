package com.baidu.service;

import java.util.List;

import com.baidu.entity.AchievementAndCourse;

public interface AchievementService {
	List<AchievementAndCourse> selectAll();
}
