package com.baidu.service;

import java.util.List;

import com.baidu.entity.ExamList;
import com.baidu.entity.Examination;

public interface ExamService {
	//成绩列表 两表数据封装在一起
	List<ExamList> getAll();
	
	//添加成绩
	int addExam(Examination examination);
	
	//删除
	int deleteExam(Integer e_id);


}
