/*
Navicat MySQL Data Transfer

Source Server         : mydb
Source Server Version : 50515
Source Host           : localhost:3306
Source Database       : student

Target Server Type    : MYSQL
Target Server Version : 50515
File Encoding         : 65001

Date: 2019-07-05 07:48:11
*/

SET FOREIGN_KEY_CHECKS=0;
drop database if exists student;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`student` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `student`;
-- ----------------------------
-- Table structure for `achievement`
-- ----------------------------
DROP TABLE IF EXISTS `achievement`;
CREATE TABLE `achievement` (
  `A_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '成绩id',
  `A_sno` int(50) DEFAULT NULL COMMENT '学号',
  `A_name` varchar(50) DEFAULT NULL COMMENT '姓名',
  `A_course` int(20) DEFAULT NULL COMMENT '课程名称',
  `A_examination` int(20) DEFAULT NULL COMMENT '考试场次',
  `A_score` int(20) DEFAULT NULL COMMENT '成绩',
  `A_grade` int(20) DEFAULT NULL COMMENT '年级',
  `A_class` int(20) DEFAULT NULL COMMENT '班级名称',
  PRIMARY KEY (`A_id`)
 
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of achievement
-- -----------------------------
-- 13级语文考试
INSERT INTO achievement VALUES ('1', '1', '张三', '1', '1', '98', '1', '1');
INSERT INTO achievement VALUES ('2', '2', '李四', '1', '1', '98', '1', '1');
INSERT INTO achievement VALUES ('3', '3', '王五', '1', '1', '98', '1', '1');
INSERT INTO achievement VALUES ('4', '4', '赵六', '1', '2', '98', '1', '2');
INSERT INTO achievement VALUES ('5', '5', '李八', '1', '2', '98', '1', '2');
INSERT INTO achievement VALUES ('6', '6', '李白', '1', '2', '98', '1', '2');
INSERT INTO achievement VALUES ('7', '7', '杜甫', '1', '3', '98', '1', '3');
INSERT INTO achievement VALUES ('8', '8', '王维', '1', '3', '98', '1', '3');
INSERT INTO achievement VALUES ('9', '9', '马玉', '1', '3', '98', '1', '3');

-- 14级会考
INSERT INTO achievement VALUES ('10', '10', '孙悟空', '1', '4', '98', '2', '1');
INSERT INTO achievement VALUES ('11', '10', '孙悟空', '2', '4', '98', '2', '1');
INSERT INTO achievement VALUES ('12', '10', '孙悟空', '3', '4', '98', '2', '1');
INSERT INTO achievement VALUES ('13', '11', '猪八戒', '1', '4', '98', '2', '1');
INSERT INTO achievement VALUES ('14', '11', '猪八戒', '2', '4', '98', '2', '1');
INSERT INTO achievement VALUES ('15', '11', '猪八戒', '3', '4', '98', '2', '1');
INSERT INTO achievement VALUES ('16', '12', '沙僧', '1', '4', '98', '2', '1');
INSERT INTO achievement VALUES ('17', '12', '沙僧', '2', '4', '98', '2', '1');
INSERT INTO achievement VALUES ('18', '12', '沙僧', '3', '4', '98', '2', '1');

INSERT INTO achievement VALUES ('19', '13', '唐生', '1', '5', '98', '2', '2');
INSERT INTO achievement VALUES ('20', '13', '唐生', '2', '5', '98', '2', '2');
INSERT INTO achievement VALUES ('21', '13', '唐生', '3', '5', '98', '2', '2');
INSERT INTO achievement VALUES ('22', '14', 'jack', '1', '5', '98', '2', '2');
INSERT INTO achievement VALUES ('23', '14', 'jack', '2', '5', '98', '2', '2');
INSERT INTO achievement VALUES ('24', '14', 'jack', '3', '5', '98', '2', '2');
INSERT INTO achievement VALUES ('25', '15', 'marry', '1', '5', '98', '2', '2');
INSERT INTO achievement VALUES ('26', '15', 'marry', '2', '5', '98', '2', '2');
INSERT INTO achievement VALUES ('27', '15', 'marry', '3', '5', '98', '2', '2');

INSERT INTO achievement VALUES ('28', '16', 'bb', '1', '6', '98', '2', '3');
INSERT INTO achievement VALUES ('29', '16', 'bb', '2', '6', '98', '2', '3');
INSERT INTO achievement VALUES ('30', '16', 'bb', '3', '6', '98', '2', '3');
INSERT INTO achievement VALUES ('31', '17', 'admin', '1', '6', '98', '2', '3');
INSERT INTO achievement VALUES ('32', '17', 'admin', '2', '6', '98', '2', '3');
INSERT INTO achievement VALUES ('33', '17', 'admin', '3', '6', '98', '2', '3');
INSERT INTO achievement VALUES ('34', '18', 'dd', '1', '6', '98', '2', '3');
INSERT INTO achievement VALUES ('35', '18', 'dd', '2', '6', '98', '2', '3');
INSERT INTO achievement VALUES ('36', '18', 'dd', '3', '6', '98', '2', '3');

-- 15级期末考试
INSERT INTO achievement VALUES ('37', '19', '小王', '1', '7', '98', '3', '1');
INSERT INTO achievement VALUES ('38', '19', '小王', '2', '7', '98', '3', '1');
INSERT INTO achievement VALUES ('39', '19', '小王', '3', '7', '98', '3', '1');
INSERT INTO achievement VALUES ('40', '19', '小王', '4', '7', '98', '3', '1');
INSERT INTO achievement VALUES ('41', '19', '小王', '5', '7', '98', '3', '1');
INSERT INTO achievement VALUES ('42', '19', '小王', '6', '7', '98', '3', '1');
INSERT INTO achievement VALUES ('43', '20', '阿达', '1', '7', '98', '3', '1');
INSERT INTO achievement VALUES ('44', '20', '阿达', '2', '7', '98', '3', '1');
INSERT INTO achievement VALUES ('45', '20', '阿达', '3', '7', '98', '3', '1');
INSERT INTO achievement VALUES ('46', '20', '阿达', '4', '7', '98', '3', '1');
INSERT INTO achievement VALUES ('47', '20', '阿达', '5', '7', '98', '3', '1');
INSERT INTO achievement VALUES ('48', '20', '阿达', '6', '7', '98', '3', '1');
INSERT INTO achievement VALUES ('49', '21', '返回', '1', '7', '98', '3', '1');
INSERT INTO achievement VALUES ('50', '21', '返回', '2', '7', '98', '3', '1');
INSERT INTO achievement VALUES ('51', '21', '返回', '3', '7', '98', '3', '1');
INSERT INTO achievement VALUES ('52', '21', '返回', '4', '7', '98', '3', '1');
INSERT INTO achievement VALUES ('53', '21', '返回', '5', '7', '98', '3', '1');
INSERT INTO achievement VALUES ('54', '21', '返回', '6', '7', '98', '3', '1');
INSERT INTO achievement VALUES ('55', '22', '阿雅', '1', '8', '98', '3', '2');
INSERT INTO achievement VALUES ('56', '22', '阿雅', '2', '8', '98', '3', '2');
INSERT INTO achievement VALUES ('57', '22', '阿雅', '3', '8', '98', '3', '2');
INSERT INTO achievement VALUES ('58', '22', '阿雅', '4', '8', '98', '3', '2');
INSERT INTO achievement VALUES ('59', '22', '阿雅', '5', '8', '98', '3', '2');
INSERT INTO achievement VALUES ('60', '22', '阿雅', '6', '8', '98', '3', '2');
INSERT INTO achievement VALUES ('61', '23', '射手', '1', '8', '98', '3', '2');
INSERT INTO achievement VALUES ('62', '23', '射手', '2', '8', '98', '3', '2');
INSERT INTO achievement VALUES ('63', '23', '射手', '3', '8', '98', '3', '2');
INSERT INTO achievement VALUES ('64', '23', '射手', '4', '8', '98', '3', '2');
INSERT INTO achievement VALUES ('65', '23', '射手', '5', '8', '98', '3', '2');
INSERT INTO achievement VALUES ('66', '23', '射手', '6', '8', '98', '3', '2');
INSERT INTO achievement VALUES ('67', '24', '小兰', '1', '8', '98', '3', '2');
INSERT INTO achievement VALUES ('68', '24', '小兰', '2', '8', '98', '3', '2');
INSERT INTO achievement VALUES ('69', '24', '小兰', '3', '8', '98', '3', '2');
INSERT INTO achievement VALUES ('70', '24', '小兰', '4', '8', '98', '3', '2');
INSERT INTO achievement VALUES ('71', '24', '小兰', '5', '8', '98', '3', '2');
INSERT INTO achievement VALUES ('72', '24', '小兰', '6', '8', '98', '3', '2');

INSERT INTO achievement VALUES ('73', '25', '小a', '1', '9', '98', '3', '3');
INSERT INTO achievement VALUES ('74', '25', '小a', '2', '9', '98', '3', '3');
INSERT INTO achievement VALUES ('75', '25', '小a', '3', '9', '98', '3', '3');
INSERT INTO achievement VALUES ('76', '25', '小a', '4', '9', '98', '3', '3');
INSERT INTO achievement VALUES ('77', '25', '小a', '5', '9', '98', '3', '3');
INSERT INTO achievement VALUES ('78', '25', '小a', '6', '9', '98', '3', '3');

INSERT INTO achievement VALUES ('79', '26', '小b', '1', '9', '98', '3', '3');
INSERT INTO achievement VALUES ('80', '26', '小b', '2', '9', '98', '3', '3');
INSERT INTO achievement VALUES ('81', '26', '小b', '3', '9', '98', '3', '3');
INSERT INTO achievement VALUES ('82', '26', '小b', '4', '9', '98', '3', '3');
INSERT INTO achievement VALUES ('83', '26', '小b', '5', '9', '98', '3', '3');
INSERT INTO achievement VALUES ('84', '26', '小b', '6', '9', '98', '3', '3');

INSERT INTO achievement VALUES ('85', '27', '小b', '1', '9', '98', '3', '3');
INSERT INTO achievement VALUES ('86', '27', '小b', '2', '9', '98', '3', '3');
INSERT INTO achievement VALUES ('87', '27', '小b', '3', '9', '98', '3', '3');
INSERT INTO achievement VALUES ('88', '27', '小b', '4', '9', '98', '3', '3');
INSERT INTO achievement VALUES ('89', '27', '小b', '5', '9', '98', '3', '3');
INSERT INTO achievement VALUES ('90', '27', '小b', '6', '9', '98', '3', '3');



-- ----------------------------
-- Table structure for `adminer`
-- ----------------------------
DROP TABLE IF EXISTS `adminer`;
CREATE TABLE `adminer` (
  `A_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '管理员id',
  `A_uname` varchar(50) DEFAULT NULL COMMENT '名称',
  `A_pwd` varchar(50) DEFAULT NULL COMMENT '密码',
  PRIMARY KEY (`A_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of adminer
-- ----------------------------
INSERT INTO adminer VALUES ('1', 'admin', 'admin');
INSERT INTO adminer VALUES ('2', 'jack', 'jack');
INSERT INTO adminer VALUES ('3', 'tom', 'tom');

-- ----------------------------
-- Table structure for `class`
-- ----------------------------
DROP TABLE IF EXISTS `class`;
CREATE TABLE `class` (
  `c_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '班级id',
  `c_name` varchar(50) DEFAULT NULL COMMENT '班级名称',
  PRIMARY KEY (`c_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of class
-- ----------------------------
INSERT INTO class VALUES ('1', '一班');
INSERT INTO class VALUES ('2', '二班');
INSERT INTO class VALUES ('3', '三班');


-- ----------------------------
-- Table structure for `course`
-- ----------------------------
DROP TABLE IF EXISTS `course`;
CREATE TABLE `course` (
  `S_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '课程名称',
  `S_name` varchar(50) DEFAULT NULL COMMENT '课程名称',
  PRIMARY KEY (`S_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of course
-- ----------------------------
INSERT INTO course VALUES ('1', '语文');
INSERT INTO course VALUES ('2', '数学');
INSERT INTO course VALUES ('3', '英语');
INSERT INTO course VALUES ('4', '物理');
INSERT INTO course VALUES ('5', '化学');
INSERT INTO course VALUES ('6', '生物');


-- ----------------------------
-- Table structure for `examination`
-- ----------------------------
DROP TABLE IF EXISTS `examination`;
CREATE TABLE `examination` (
  `E_id` int(11) NOT NULL AUTO_INCREMENT,
  `E_name` varchar(50) DEFAULT NULL COMMENT '考试名称',
  `E_time` date DEFAULT NULL COMMENT '时间',
  `E_type` varchar(50) DEFAULT NULL COMMENT '类型',
  `E_grade` int(20) DEFAULT NULL COMMENT '年级',
  `E_class` int(20) DEFAULT NULL COMMENT '班级',
  `E_beizhu` varchar(50) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`E_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of examination
-- ----------------------------
INSERT INTO examination VALUES ('1', '一班语文测试', '2016-01-15', '平时考试', '1', '1', '语文测试');
INSERT INTO examination VALUES ('2', '二班语文测试', '2016-01-15', '平时考试', '1', '2', '语文测试');
INSERT INTO examination VALUES ('3', '三班语文测试', '2016-01-15', '平时考试', '1', '3', '语文测试');
INSERT INTO examination VALUES ('4', '第一次会考', '2016-06-08', '会考', '2', '1', '会考');
INSERT INTO examination VALUES ('5', '第二次会考', '2016-06-08', '会考', '2', '2', '会考');
INSERT INTO examination VALUES ('6', '第三次会考', '2016-06-08', '会考', '2', '3', '会考');
INSERT INTO examination VALUES ('7', '第一次期末考试', '2016-06-08', '期末考试', '3', '1', '期末考试');
INSERT INTO examination VALUES ('8', '第二次期末考试', '2016-06-08', '期末考试', '3', '2', '期末考试');
INSERT INTO examination VALUES ('9', '第三次期末考试', '2016-06-08', '期末考试', '3', '3', '期末考试');

-- ----------------------------
-- Table structure for `grade`
-- ----------------------------
DROP TABLE IF EXISTS `grade`;
CREATE TABLE `grade` (
  `G_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '年假id',
  `G_name` varchar(50) DEFAULT NULL COMMENT '年级名称',
  `G_course` varchar(50) DEFAULT NULL COMMENT '年级课程',
  PRIMARY KEY (`G_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of grade
-- ----------------------------
INSERT INTO grade VALUES ('1', '2013级', '语文 数学 英语 物理 化学');
INSERT INTO grade VALUES ('2', '2014级', '体育 计算机 政治 语文');
INSERT INTO grade VALUES ('3', '2015级', '数学 化学 历史 政治');

-- ----------------------------
-- Table structure for `teacher`
-- ----------------------------
DROP TABLE IF EXISTS `teacher`;
CREATE TABLE `teacher` (
  `T_id` int(11) NOT NULL AUTO_INCREMENT,
  `T_gonghao` varchar(50) DEFAULT NULL COMMENT '工号',
  `T_name` varchar(50) DEFAULT NULL COMMENT '姓名',
  `T_sex` varchar(50) DEFAULT NULL COMMENT '性别',
  `T_phone` varchar(50) DEFAULT NULL COMMENT '电话',
  `T_qq` varchar(50) DEFAULT NULL COMMENT 'qq',
  `T_class` int(11) DEFAULT NULL COMMENT '班级',
  `T_grade` int(11) DEFAULT NULL COMMENT '年级',
  `T_course` int(11) DEFAULT NULL COMMENT '课程',
  `T_photo` varchar(255) DEFAULT NULL COMMENT '图片',
  PRIMARY KEY (`T_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of teacher
-- ----------------------------
INSERT INTO teacher VALUES ('1', '2000', '智晖', '男', '13888888888', '888888888', '1', '1', '1', "\\photo\\teacher.jpg");
INSERT INTO teacher VALUES ('2', '2001', '景天', '女', '13888888888', '888888888', '1', '2', '2', "\\photo\\teacher.jpg");
INSERT INTO teacher VALUES ('3', '2002', '小红', '女', '13888888888', '888888888', '1', '3', '3', "\\photo\\teacher.jpg");
INSERT INTO teacher VALUES ('4', '2003', '小蓝', '男', '13888888888', '888888888', '2', '1', '3', "\\photo\\teacher.jpg");
INSERT INTO teacher VALUES ('5', '2004', '小黄', '男', '13888888888', '888888888', '2', '2', '3', "\\photo\\teacher.jpg");
INSERT INTO teacher VALUES ('6', '2005', '小城', '男', '13888888888', '888888888', '2', '3', '3',"\\photo\\teacher.jpg");
INSERT INTO teacher VALUES ('7', '2006', '小紫', '女', '13888888888', '888888888', '3', '1', '4', "\\photo\\teacher.jpg");
INSERT INTO teacher VALUES ('8', '2007', '啊大', '女', '13888888888', '888888888', '3', '2', '2', "\\photo\\teacher.jpg");
INSERT INTO teacher VALUES ('9', '2008', '旭鹏', '女', '13888888888', '888888888', '3', '3', '4', "\\photo\\teacher.jpg");

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `U_id` int(11) NOT NULL AUTO_INCREMENT,
  `U_sno` int(20) DEFAULT NULL COMMENT '学号',
  `U_name` varchar(50) DEFAULT NULL COMMENT '姓名',
  `U_sex` varchar(50) DEFAULT NULL COMMENT '性别',
  `U_phone` varchar(50) DEFAULT NULL COMMENT '电话',
  `U_qq` varchar(50) DEFAULT NULL COMMENT 'qq',
  `U_class` int(11) DEFAULT NULL,
  `U_grade` int(11) DEFAULT NULL,
  `U_photo` varchar(255) DEFAULT NULL COMMENT '图片',
  PRIMARY KEY (`U_id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO user VALUES ('1', '201301001', '张三', '男', '13888888888', '888888888', '1', '1', '\\photo\\student.jpg');
INSERT INTO user VALUES ('2', '201301002', '李四', '男', '13888888888', '888888888', '1', '1','\\photo\\student.jpg');
INSERT INTO user VALUES ('3', '201301003', '王五', '女', '13888888888', '888888888', '1', '1', '\\photo\\student.jpg');
INSERT INTO user VALUES ('4', '201302001', '赵六', '男', '13888888888', '888888888', '2', '1', '\\photo\\student.jpg');
INSERT INTO user VALUES ('5', '201302002', '李八', '男', '13888888888', '888888888', '2', '1', '\\photo\\student.jpg');
INSERT INTO user VALUES ('6', '201302003', '李白', '女', '13888888888', '888888888', '2', '1', '\\photo\\student.jpg');
INSERT INTO user VALUES ('7', '201303001', '杜甫', '男', '13888888888', '888888888', '3', '1', '\\photo\\student.jpg');
INSERT INTO user VALUES ('8', '201303002', '王维', '女', '13888888888', '888888888', '3', '1', '\\photo\\student.jpg');
INSERT INTO user VALUES ('9', '201303003', '马玉', '男', '13888888888', '888888888', '3', '1', '\\photo\\student.jpg');
INSERT INTO user VALUES ('10', '201401001', '孙悟空', '男', '13888888888', '888888888', '1', '2', '\\photo\\student.jpg');
INSERT INTO user VALUES ('11', '201401002', '猪八戒', '女', '13888888888', '888888888', '1', '2', '\\photo\\student.jpg');
INSERT INTO user VALUES ('12', '201401003', '沙僧', '女', '13888888888', '888888888', '1', '2', '\\photo\\student.jpg');
INSERT INTO user VALUES ('13', '201402001', '唐生', '男', '13888888888', '888888888', '2', '2', '\\photo\\student.jpg');
INSERT INTO user VALUES ('14', '201402002', 'jack', '男', '13888888888', '888888888', '2', '2', '\\photo\\student.jpg');
INSERT INTO user VALUES ('15', '201402003', 'marry', '男', '13888888888', '888888888', '2', '2', '\\photo\\student.jpg');
INSERT INTO user VALUES ('16', '201403001', 'bb', '男', '13888888888', '888888888', '3', '2', '\\photo\\student.jpg');
INSERT INTO user VALUES ('17', '201403002', 'admin', '男', '13888888888', '888888888', '3', '2', '\\photo\\student.jpg');
INSERT INTO user VALUES ('18', '201403003', 'dd', '男', '13888888888', '888888888', '3', '2', '\\photo\\student.jpg');
INSERT INTO user VALUES ('19', '201501001', '小王', '男', '13888888888', '888888888', '1', '3', '\\photo\\student.jpg');
INSERT INTO user VALUES ('20', '201501002', '阿达', '男', '13888888888', '888888888', '1', '3', '\\photo\\student.jpg');
INSERT INTO user VALUES ('21', '201501003', '返回', '男', '13888888888', '888888888', '1', '3', '\\photo\\student.jpg');
INSERT INTO user VALUES ('22', '201502001', '阿雅', '女', '13888888888', '888888888', '2', '3', '\\photo\\student.jpg');
INSERT INTO user VALUES ('23', '201502002', '射手', '男', '13888888888', '888888888', '2', '3', '\\photo\\student.jpg');
INSERT INTO user VALUES ('24', '201502003', '小兰', '女', '13888888888', '888888888', '2', '3', '\\photo\\student.jpg');
INSERT INTO user VALUES ('25', '201503001', '小a', '男', '13888888888', '888888888', '3', '3', '\\photo\\student.jpg');
INSERT INTO user VALUES ('26', '201503002', '小b', '男', '13888888888', '888888888', '3', '3', '\\photo\\student.jpg');
INSERT INTO user VALUES ('27', '201503003', '小c', '男', '13888888888', '888888888', '3', '3', '\\photo\\student.jpg');




